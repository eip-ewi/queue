/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.io.File;
import java.nio.file.Paths;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import nl.tudelft.queue.properties.QueueProperties;

@SpringBootTest(classes = { AdminService.class })
public class AdminServiceTest {

	@Autowired
	private AdminService as;

	@MockBean
	private QueueProperties qp;

	@BeforeEach
   void setup() {
       when(qp.getStaticallyServedPath()).thenReturn("src/test/resources");
   }

	@AfterEach
	void teardown() {
		File directory = Paths.get("src/test/resources/maps").toFile();

		if (directory.exists()) {
			File[] files = directory.listFiles();
			if (files != null) {
				for (File file : files) {
					file.delete();
				}
			}
		}

		directory.delete();
	}

	@Test
	void newMapCorrectlyOverwritesOldMap() {
		MultipartFile m1 = new MockMultipartFile("my-file.png", (byte[]) null);
		MultipartFile m2 = new MockMultipartFile("my-file.png", (byte[]) null);
		assertThatCode(() -> {
			as.uploadRoomMap(m1, "my-file.png");
			as.uploadRoomMap(m2, "my-file.png");
		}).doesNotThrowAnyException();

		assertThat(Paths.get("src/test/resources/maps").toFile().listFiles().length).isEqualTo(1);
	}

}
