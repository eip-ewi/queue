/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import nl.tudelft.labracore.api.dto.PersonSummaryDTO;
import nl.tudelft.queue.cache.PersonCacheManager;
import nl.tudelft.queue.model.LabRequest;
import nl.tudelft.queue.properties.JitsiProperties;

@SpringBootTest(classes = JitsiService.class)
public class JitsiServiceTest {
	@MockBean
	private JitsiProperties properties;

	@MockBean
	private PersonCacheManager pCache;

	@Autowired
	private JitsiService js;

	private LabRequest request;

	@BeforeEach
	void setUp() {
		request = LabRequest.builder()
				.requester(76832L)
				.createdAt(LocalDateTime.now())
				.build();
	}

	@Test
	void createdJitsiRoomNameShouldStartWithUsername() {
		when(pCache.getRequired(request.getRequester()))
				.thenReturn(new PersonSummaryDTO().username("jvdcbaskjhbdsaj"));

		assertThat(js.createJitsiRoomName(request))
				.startsWith("jvdcbaskjhbdsaj");
	}

	@Test
	void getJitsiRoomLinkDependsOnPropertiesUrl() {
		request.setJitsiRoom("jbndasd");
		when(properties.getUrl()).thenReturn("bjsdbkjasbdkjsabj");

		assertThat(js.getJitsiRoomUrl(request))
				.startsWith("bjsdbkjasbdkjsabj");
	}

	@Test
	void getJitsiRoomLinkContainsRoomName() {
		request.setJitsiRoom("jbndaskjbds");
		when(properties.getUrl()).thenReturn("bnjdkjasbdkjsabkjd");

		assertThat(js.getJitsiRoomUrl(request))
				.endsWith("jbndaskjbds");
	}

	@Test
	void getJitsiRoomLinkReturnsNullIfJitsiRoomIsNull() {
		assertThat(js.getJitsiRoomUrl(request))
				.isNull();
	}
}
