/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import nl.tudelft.labracore.lib.security.user.DefaultRole;
import nl.tudelft.labracore.lib.security.user.Person;
import nl.tudelft.queue.model.Profile;
import nl.tudelft.queue.model.enums.Language;
import nl.tudelft.queue.properties.QueueProperties;
import nl.tudelft.queue.repository.ProfileRepository;
import test.test.TestQueueApplication;

@SpringBootTest(classes = TestQueueApplication.class)
public class CodeOfConductServiceTest {
	private CodeOfConductService codeOfConductService;

	@Autowired
	private QueueProperties queueProperties;

	private ProfileRepository profileRepository;

	private MarkdownService markdownService;

	private Person testPerson;

	@BeforeEach
	void setUp() {
		profileRepository = Mockito.mock(ProfileRepository.class);
		markdownService = Mockito.mock(MarkdownService.class);
		codeOfConductService = new CodeOfConductService(queueProperties, profileRepository,
				markdownService);

		testPerson = new Person(1L, "1", "username", "displayname", "email", DefaultRole.STUDENT,
				1, "1");
	}

	@Test
    void updateCocCallsRepository() {
        when(profileRepository.findProfileForPerson(any())).thenReturn(new Profile());

        codeOfConductService.updateLatestCoC(testPerson, LocalDateTime.now());

        Mockito.verify(profileRepository, times(1)).findProfileForPerson(testPerson);
    }

	@Test
	void getCoCForPerson() {
		var now = LocalDateTime.now();
		when(profileRepository.findProfileForPerson(any())).thenReturn(new Profile(1L,
				Language.ANY, now));
		when(markdownService.markdownToHtml(any())).thenReturn("this is a test");

		var coc = codeOfConductService.getCodeOfConduct(testPerson);

		assertThat(coc.getCodeOfConduct()).isEqualTo("this is a test");
		assertThat(coc.getDateAccessed()).isEqualTo(now);

		Mockito.verify(markdownService, Mockito.times(1)).markdownToHtml(any());
		Mockito.verify(profileRepository, times(1)).findProfileForPerson(testPerson);

	}

}
