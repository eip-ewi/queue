/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import nl.tudelft.labracore.api.ProgramControllerApi;
import nl.tudelft.labracore.api.dto.*;
import nl.tudelft.labracore.lib.security.user.Person;
import nl.tudelft.queue.cache.CourseCacheManager;
import nl.tudelft.queue.cache.EditionCacheManager;
import nl.tudelft.queue.cache.PersonCacheManager;
import nl.tudelft.queue.cache.SessionCacheManager;
import nl.tudelft.queue.model.LabRequest;
import nl.tudelft.queue.model.labs.RegularLab;
import nl.tudelft.queue.repository.RequestRepository;
import nl.tudelft.queue.service.mail.MailService;
import reactor.core.publisher.Mono;
import test.test.NoDBTestQueueApplication;

@SpringBootTest(classes = NoDBTestQueueApplication.class)
public class ReportServiceTest {

	@Autowired
	ReportService reportService;

	@MockBean
	EditionCacheManager editionCacheManager;

	@Autowired
	CourseCacheManager courseCacheManager;

	@Autowired
	ProgramControllerApi programControllerApi;

	@MockBean
	RequestRepository requestRepository;

	@MockBean
	PersonCacheManager personCacheManager;

	@MockBean
	SessionCacheManager sessionCacheManager;

	@MockBean
	MailService mailService;

	@Test
	void getReportDetails() {
		Model model = new ConcurrentModel();
		Person reporter = Person.builder().id(1L).displayName("displayName").build();
		LabRequest request = Mockito.mock(LabRequest.class);
		when(request.getAllPersonsInvolved()).thenReturn(List.of(1L));
		when(request.getSession()).thenReturn(new RegularLab().builder().session(1L).build());
		when(personCacheManager.getRequired(any(Long.class), any())).thenReturn(new PersonSummaryDTO());
		when(requestRepository.findById(any(Long.class))).thenReturn(Optional.of(request));
		when(sessionCacheManager.get(any(Long.class)))
				.thenReturn(Optional.of(new SessionDetailsDTO().edition(new EditionSummaryDTO().id(1L))));
		when(editionCacheManager.get(any(Long.class)))
				.thenReturn(Optional.of(new EditionDetailsDTO().course(new CourseSummaryDTO().id(1L))));
		when(courseCacheManager.get(any(Long.class)))
				.thenReturn(Optional.of(new CourseDetailsDTO().program(new ProgramSummaryDTO().id(1L))));
		when(programControllerApi.getProgramById(any())).thenReturn(Mono.just(new ProgramDetailsDTO()
				.academicCounsellor(new FunctionalMailboxSummaryDTO().email("academicCounsellor"))));

		reportService.getReportDetails(1L, reporter, model);

		assertThat(model.getAttribute("academicCounsellorDto")).isNotNull();
		assertThat(model.getAttribute("confidentialAdvisorDto")).isNotNull();
		assertThat(model.getAttribute("usersInvolved")).isNotNull();

	}
}
