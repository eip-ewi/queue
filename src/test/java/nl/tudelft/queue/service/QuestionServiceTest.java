/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import nl.tudelft.queue.properties.ElasticProperties;
import test.test.TestQueueApplication;

@SpringBootTest(classes = TestQueueApplication.class)
public class QuestionServiceTest {

	private QuestionService qs;

	@Autowired
	private ElasticProperties elasticProperties;

	@Mock
	private RestTemplate restTemplate;

	@BeforeEach
	void setUp() {
		qs = new QuestionService(elasticProperties, restTemplate);
	}

	@Test
	void searchForQuestionsCallsApi() {
		qs.searchForQuestions("question", 1L, null, 5);

		verify(restTemplate).postForObject(ArgumentMatchers.<String>argThat(s -> s.contains("size=5")), any(),
				any());
	}
}
