/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Comparator;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import jakarta.transaction.Transactional;
import nl.tudelft.labracore.api.EditionControllerApi;
import nl.tudelft.labracore.api.dto.EditionDetailsDTO;
import nl.tudelft.labracore.api.dto.RoomDetailsDTO;
import nl.tudelft.queue.service.LabService;
import nl.tudelft.queue.service.PermissionService;
import test.TestDatabaseLoader;
import test.labracore.EditionApiMocker;
import test.test.TestQueueApplication;

@Transactional
@AutoConfigureMockMvc
@SpringBootTest(classes = TestQueueApplication.class)
public class AdminControllerTest {

	@Autowired
	private EditionControllerApi eApi;

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private EditionApiMocker eApiMocker;

	@MockBean
	private LabService ls;

	@Autowired
	private TestDatabaseLoader db;

	@Autowired
	private PermissionService ps;

	private EditionDetailsDTO oopNow;

	@BeforeEach
	void setUp() {
		db.mockAll();

		oopNow = db.getOopNow();
	}

	@Test
	@WithUserDetails("admin")
	void getAdminPanel() throws Exception {
		mockMvc.perform(get("/admin/running")).andExpect(status().isOk());
	}

	@Test
	@WithUserDetails("admin")
	void assertRoomOverviewIsSortedCorrectly() throws Exception {
		var request = mockMvc.perform(get("/admin/rooms"))
				.andExpect(status().isOk())
				.andExpect(view().name("admin/view/rooms"))
				.andExpect(model().attributeExists("rooms"))
				.andReturn();

		var sut = (List<RoomDetailsDTO>) request.getModelAndView().getModel().get("rooms");

		assertThat(sut).isSortedAccordingTo(Comparator
				.comparing((RoomDetailsDTO r) -> r.getBuilding().getName())
				.thenComparing(RoomDetailsDTO::getName));

	}

}
