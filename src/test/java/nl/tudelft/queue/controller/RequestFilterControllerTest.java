/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import jakarta.transaction.Transactional;
import nl.tudelft.queue.dto.util.RequestTableFilterDTO;
import nl.tudelft.queue.service.RequestTableService;
import test.test.TestQueueApplication;

@Transactional
@AutoConfigureMockMvc
@SpringBootTest(classes = TestQueueApplication.class)
public class RequestFilterControllerTest {

	@Autowired
	private MockMvc mvc;

	@MockBean
	private RequestTableService service;

	@Test
	@WithUserDetails("teacher1")
	void submitRequestTableFilters() throws Exception {
		RequestTableFilterDTO filter = new RequestTableFilterDTO();
		String path = "/index";
		when(service.checkAndStoreFilterDTO(any(RequestTableFilterDTO.class), any(String.class)))
				.thenReturn(filter);

		mvc.perform(post("/filter").with(csrf())
				.param("filter-submit", "1")
				.param("return-path", path))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/index?page=0"));

		verify(service).checkAndStoreFilterDTO(filter, path);
	}

	@Test
	@WithUserDetails("teacher1")
	void clearRequestTableFilters() throws Exception {
		String path = "/index";
		doNothing().when(service).clearFilter(anyString());

		mvc.perform(post("/filter").with(csrf())
				.param("filter-clear", "1")
				.param("return-path", path))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("/index?page=0"));

		verify(service).clearFilter(path);
	}

	@ParameterizedTest
	@MethodSource(value = "protectedEndpoints")
	void testWithoutUserDetailsIsForbidden(MockHttpServletRequestBuilder request) throws Exception {
		mvc.perform(request.with(csrf()))
				.andExpect(status().is3xxRedirection())
				.andExpect(redirectedUrl("http://localhost/login"));
	}

	private static List<MockHttpServletRequestBuilder> protectedEndpoints() {
		return List.of(
				post("/filter")
						.param("filter-submit", "1")
						.param("return-path", "/path")
						.contentType(MediaType.APPLICATION_FORM_URLENCODED)
						.flashAttr("filter", new RequestTableFilterDTO()),
				post("/filter")
						.param("filter-clear", "1")
						.param("return-path", "path"));
	}

}
