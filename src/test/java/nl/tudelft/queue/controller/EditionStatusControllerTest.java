/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static test.JsonContentMatcher.jsonContent;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import jakarta.transaction.Transactional;
import nl.tudelft.labracore.api.dto.EditionDetailsDTO;
import nl.tudelft.queue.dto.view.statistics.RequestFrequencyViewDto;
import test.TestDatabaseLoader;
import test.test.TestQueueApplication;

@Transactional
@AutoConfigureMockMvc
@SpringBootTest(classes = TestQueueApplication.class)
public class EditionStatusControllerTest {
	@Autowired
	private TestDatabaseLoader db;

	@Autowired
	private MockMvc mvc;

	private EditionDetailsDTO oopNow;

	@BeforeEach
	void setUp() {
		db.mockAll();

		oopNow = db.getOopNow();
	}

	@Test
	@WithUserDetails("student55")
	void getRequestFrequenciesDoesNotAllowStudent() throws Exception {
		mvc.perform(get("/edition/{eId}/status/freq/request", oopNow.getId()))
				.andExpect(status().isForbidden());
	}

	@ParameterizedTest
	@CsvSource("0, 1, 5, 12")
	@WithUserDetails("admin")
	void getRequestFrequenciesBucketsMatches(int nBuckets) throws Exception {
		mvc.perform(get("/edition/{eId}/status/freq/request", oopNow.getId())
				.queryParam("nBuckets", "" + nBuckets))
				.andExpect(status().isOk())
				.andExpect(jsonContent(RequestFrequencyViewDto.class)
						.test(dto -> assertThat(dto.getBuckets()).size().isEqualTo(nBuckets + 1)));
	}
}
