/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.nullValue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import jakarta.transaction.Transactional;
import nl.tudelft.labracore.api.dto.PersonSummaryDTO;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.queue.model.enums.Language;
import nl.tudelft.queue.repository.ProfileRepository;
import nl.tudelft.queue.service.CodeOfConductService;
import test.labracore.PersonApiMocker;
import test.test.TestQueueApplication;

@Transactional
@AutoConfigureMockMvc
@SpringBootTest(classes = TestQueueApplication.class)
public class ProfileControllerTest {

	@Autowired
	private MockMvc mvc;
	@Autowired
	private ProfileRepository profileRepository;
	@Autowired
	private PersonApiMocker pApiMocker;
	@Autowired
	private CodeOfConductService codeOfConductService;
	@Autowired
	private DTOConverter converter;

	private ProfileController profileController;

	@BeforeEach
	void setUp() {
		profileController = new ProfileController(profileRepository, codeOfConductService,
				converter);
	}

	@Test
	@WithUserDetails("teacher1")
	void updateProfile() throws Exception {
		var person = pApiMocker.getByUsername("teacher1").get();
		mvc.perform(post("/profile/update").with(csrf())
				.header("Content-Type", "application/json")
				.content("{\"language\": \"ENGLISH_ONLY\"}"))
				.andExpect(status().isOk());
		assertThat(profileRepository.findById(person.getId()).get().getLanguage())
				.isEqualTo(Language.ENGLISH_ONLY);
	}

	@Test
	@WithUserDetails("teacher1")
	void getCoCDto() throws Exception {
		mvc.perform(get("/profile/code-of-conduct"))
				.andExpect(status().isOk())
				.andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.dateAccessed", nullValue()))
				.andExpect(jsonPath("$.codeOfConduct", CoreMatchers.not(emptyOrNullString())));
	}

	@Test
	@WithUserDetails("teacher1")
	void updateCoc() throws Exception {
		mvc.perform(post("/profile/code-of-conduct/update").with(csrf()))
				.andExpect(status().isOk());
		PersonSummaryDTO person = pApiMocker.getByUsername("teacher1").get();
		assertThat(profileRepository.findById(person.getId()).get().getLastSeenCoc()).isNotNull();
	}

}
