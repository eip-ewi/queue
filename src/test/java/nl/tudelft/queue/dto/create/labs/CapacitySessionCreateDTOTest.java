/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.labs;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.params.provider.Arguments;

import nl.tudelft.queue.dto.create.QueueSessionCreateDTO;
import nl.tudelft.queue.dto.create.QueueSessionCreateDTOTest;
import nl.tudelft.queue.dto.create.embeddables.CapacitySessionConfigCreateDTO;
import nl.tudelft.queue.model.enums.SelectionProcedure;
import nl.tudelft.queue.model.labs.CapacitySession;

@SuppressWarnings("unchecked")
public class CapacitySessionCreateDTOTest extends
		QueueSessionCreateDTOTest<CapacitySession, CapacitySessionCreateDTO, CapacitySessionCreateDTO.CapacitySessionCreateDTOBuilder<CapacitySessionCreateDTO, ?>> {
	@Override
	protected CapacitySessionCreateDTO.CapacitySessionCreateDTOBuilder<CapacitySessionCreateDTO, ?> initialBuilder() {
		return (CapacitySessionCreateDTO.CapacitySessionCreateDTOBuilder<CapacitySessionCreateDTO, ?>) CapacitySessionCreateDTO
				.builder();
	}

	@Override
	protected QueueSessionCreateDTO.QueueSessionCreateDTOBuilder<?, CapacitySessionCreateDTO, ?> buildFilledDTO(
			CapacitySessionCreateDTO.CapacitySessionCreateDTOBuilder<CapacitySessionCreateDTO, ?> builder) {
		return super.buildFilledDTO(builder
				.capacitySessionConfig(CapacitySessionConfigCreateDTO.builder()
						.enrolmentOpensAt(LocalDateTime.now().minusDays(1))
						.enrolmentClosesAt(LocalDateTime.now())
						.selectionAt(LocalDateTime.now().plusMinutes(5))
						.procedure(SelectionProcedure.RANDOM)
						.build()));
	}

	@Override
	public List<Arguments> getters() {
		return Stream.concat(super.getters().stream(), Stream.of(
				of(dto -> dto.getCapacitySessionConfig().apply(getConverter()),
						CapacitySession::getCapacitySessionConfig)))
				.collect(Collectors.toList());
	}
}
