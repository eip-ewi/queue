/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.labs;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.params.provider.Arguments;

import nl.tudelft.queue.dto.create.QueueSessionCreateDTO;
import nl.tudelft.queue.dto.create.embeddables.SlottedLabConfigCreateDTO;
import nl.tudelft.queue.model.labs.AbstractSlottedLab;

@SuppressWarnings("unchecked")
public abstract class AbstractSlottedLabCreateDTOTest<D extends AbstractSlottedLab<?>, DTO extends AbstractSlottedLabCreateDTO<D>, B extends AbstractSlottedLabCreateDTO.AbstractSlottedLabCreateDTOBuilder<?, DTO, ?>>
		extends LabCreateDTOTest<D, DTO, B> {
	@Override
	protected QueueSessionCreateDTO.QueueSessionCreateDTOBuilder<?, DTO, ?> buildFilledDTO(B builder) {
		return super.buildFilledDTO((B) builder
				.slottedLabConfig(SlottedLabConfigCreateDTO.builder()
						.selectionOpensAt(LocalDateTime.of(2020, 9, 8, 20, 9))
						.build()));
	}

	@Override
	public List<Arguments> getters() {
		return Stream.concat(super.getters().stream(), Stream.of(
				of(dto -> dto.getSlottedLabConfig().apply(getConverter()),
						AbstractSlottedLab::getSlottedLabConfig)))
				.collect(Collectors.toList());
	}
}
