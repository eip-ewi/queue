/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.labs;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.params.provider.Arguments;

import nl.tudelft.queue.dto.create.QueueSessionCreateDTO;
import nl.tudelft.queue.dto.create.embeddables.ExamLabConfigCreateDTO;
import nl.tudelft.queue.model.labs.ExamLab;

@SuppressWarnings("unchecked")
public class ExamLabCreateDTOTest extends
		AbstractSlottedLabCreateDTOTest<ExamLab, ExamLabCreateDTO, ExamLabCreateDTO.ExamLabCreateDTOBuilder<ExamLabCreateDTO, ?>> {
	@Override
	protected ExamLabCreateDTO.ExamLabCreateDTOBuilder<ExamLabCreateDTO, ?> initialBuilder() {
		return (ExamLabCreateDTO.ExamLabCreateDTOBuilder<ExamLabCreateDTO, ?>) ExamLabCreateDTO.builder();
	}

	@Override
	protected QueueSessionCreateDTO.QueueSessionCreateDTOBuilder<?, ExamLabCreateDTO, ?> buildFilledDTO(
			ExamLabCreateDTO.ExamLabCreateDTOBuilder<ExamLabCreateDTO, ?> builder) {
		return super.buildFilledDTO(builder
				.examLabConfig(ExamLabConfigCreateDTO.builder()
						.percentage(69)
						.build()));
	}

	@Override
	public List<Arguments> getters() {
		return Stream.concat(super.getters().stream(), Stream.of(
				of(dto -> dto.getExamLabConfig().apply(getConverter()), ExamLab::getExamLabConfig)))
				.collect(Collectors.toList());
	}
}
