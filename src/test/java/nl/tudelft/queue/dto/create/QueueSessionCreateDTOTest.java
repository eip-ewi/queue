/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.params.provider.Arguments;

import nl.tudelft.queue.dto.create.constraints.ClusterConstraintCreateDTO;
import nl.tudelft.queue.dto.create.constraints.ModuleDivisionConstraintCreateDTO;
import nl.tudelft.queue.dto.create.embeddables.LabRequestConstraintsCreateDTO;
import nl.tudelft.queue.model.QueueSession;
import nl.tudelft.queue.model.embeddables.Slot;

public abstract class QueueSessionCreateDTOTest<D extends QueueSession<?>, DTO extends QueueSessionCreateDTO<D>, B extends QueueSessionCreateDTO.QueueSessionCreateDTOBuilder<?, DTO, ?>>
		extends CreateTest<D, DTO> {
	protected abstract B initialBuilder();

	protected QueueSessionCreateDTO.QueueSessionCreateDTOBuilder<?, DTO, ?> buildFilledDTO(B builder) {
		return builder
				.name("This is a cool name")
				.slot(Slot.builder()
						.opensAt(LocalDateTime.of(2020, 9, 9, 20, 9))
						.closesAt(LocalDateTime.of(2020, 10, 9, 20, 9))
						.build())
				.modules(Set.of(23L, 24L, 26L))
				.rooms(Set.of(5L, 6L))
				.constraints(LabRequestConstraintsCreateDTO.builder()
						.clusterConstraint(ClusterConstraintCreateDTO.builder()
								.clusters(Set.of(33L, 44L))
								.build())
						.moduleDivisionConstraint(ModuleDivisionConstraintCreateDTO.builder()
								.divisions(Set.of(22L, 11L))
								.build())
						.build());
	}

	@Override
	protected DTO filledDTO() {
		return buildFilledDTO(initialBuilder()).build();
	}

	@Override
	public List<Arguments> getters() {
		return List.of(
				of(dto -> dto.getConstraints().getClusterConstraint().getClusters(),
						lab -> lab.getConstraints().getClusterConstraint().getClusters()),
				of(dto -> dto.getConstraints().getModuleDivisionConstraint().getDivisions(),
						lab -> lab.getConstraints().getModuleDivisionConstraint().getDivisions()),
				of(QueueSessionCreateDTO::getModules, QueueSession::getModules));
	}
}
