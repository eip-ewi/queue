/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;

import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import dummy.DummyEntity;
import dummy.DummyIdDTO;
import jakarta.transaction.Transactional;
import lombok.Getter;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.librador.dto.create.Create;
import nl.tudelft.librador.dto.id.IdDTO;
import test.test.TestQueueApplication;

@Transactional
@SpringBootTest(classes = TestQueueApplication.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class CreateTest<D, DTO extends Create<D>> {

	@Autowired
	@Getter
	private DTOConverter converter;

	/**
	 * Creates and returns a DTO that will be tested. The DTO is expected to have some value in each of the
	 * fields that is to be tested. If one of the fields of the DTO is {@code null} and the field is tested,
	 * one of the tests will fail.
	 *
	 * @return The filled DTO object.
	 */
	protected abstract DTO filledDTO();

	/**
	 * Creates a list of getter pairs that is to be tested with a couple checks. The first getter in the pair
	 * corresponds to a DTO field, the second corresponds to the data field.
	 *
	 * These pairs can be created with a type-checked call to {@link #of(Function, Function)}. Note: Do not
	 * add any ID DTO fields to this list, those should be added to {@link #idGetters()}.
	 *
	 * @return The list of arguments with which normal getters will be tested.
	 */
	public abstract List<Arguments> getters();

	/**
	 * Creates a list of getter pairs that is to be tested with a couple checks. The first getter in the pair
	 * corresponds to an ID DTO field, the second corresponds to the data field.
	 *
	 * These pairs can be created with a type-checked call to {@link #ofId(Function, Function)}.
	 *
	 * @return The list of arguments with which ID getters will be tested.
	 */
	public List<Arguments> idGetters() {
		return List.of(ofId(dto -> new DummyIdDTO(), data -> -1L));
	}

	/**
	 * Creates a list of getter pairs that is to be tested with a couple checks. The first getter in the pair
	 * corresponds to a List of ID DTOs field, the second corresponds to the data field.
	 *
	 * These pairs can be created with a type-checked call to {@link #ofIds(Function, Function, Function)}.
	 *
	 * @return The list of arguments with which ID List getters will be tested.
	 */
	public List<Arguments> idListGetters() {
		return List.of(
				ofIds(dto -> List.of(new DummyIdDTO()), data -> List.of(new DummyEntity()), obj -> -1L));
	}

	/**
	 * Creates an {@link Arguments} object from the given getters.
	 *
	 * @param  fDto The getter for a field in the DTO.
	 * @param  fD   The getter for a field in the data class.
	 * @param  <T>  The type to get out of both the DTO and the data.
	 * @return      An {@link Arguments} object containing the getters to be tested.
	 */
	public <T> Arguments of(Function<DTO, T> fDto, Function<D, T> fD) {
		return Arguments.of(fDto, fD);
	}

	/**
	 * Creates an {@link Arguments} object from the given getters.
	 *
	 * @param  fDto     The getter for an ID DTO field in the DTO.
	 * @param  fD       The function getting the target ID out of a data object.
	 * @param  <T>      The type to get out of both the DTO and the data.
	 * @param  <ID>     The type of the ID of the ID DTO.
	 * @param  <ID_DTO> The typ of the ID DTO.
	 * @return          An {@link Arguments} object containing the getters to be tested.
	 */
	public <T, ID, ID_DTO extends IdDTO<T, ID>> Arguments ofId(Function<DTO, ID_DTO> fDto,
			Function<D, ID> fD) {
		return Arguments.of(fDto, fD);
	}

	/**
	 * Creates an {@link Arguments} object from the given getters.
	 *
	 * @param  fDto     The getter for an ID DTO field in the DTO.
	 * @param  fD       The getter for getting the target information out of the data object.
	 * @param  fId      The getter for getting the ID of the filled in information.
	 * @param  <T>      The type to get out of both the DTO and the data.
	 * @param  <ID>     The type of the ID of the ID DTO.
	 * @param  <ID_DTO> The typ of the ID DTO.
	 * @return          An {@link Arguments} object containing the getters to be tested.
	 */
	public <T, ID, ID_DTO extends IdDTO<T, ID>> Arguments ofIds(Function<DTO, Collection<ID_DTO>> fDto,
			Function<D, Collection<T>> fD, Function<T, ID> fId) {
		return Arguments.of(fDto, (Function<D, Collection<ID>>) d -> fD.apply(d).stream().map(fId)
				.collect(Collectors.toList()));
	}

	@Test
	public void dtoConversionWorks() {
		assertThatCode(() -> filledDTO().apply(converter)).doesNotThrowAnyException();
	}

	@ParameterizedTest
	@MethodSource("getters")
	public <T> void dtoFieldIsNotNull(Function<DTO, T> fDto, Function<D, T> fD) {
		DTO dto = filledDTO();

		assertThat(fDto.apply(dto)).isNotNull();
	}

	@ParameterizedTest
	@MethodSource("getters")
	public <T> void dataFieldIsNotNull(Function<DTO, T> fDto, Function<D, T> fD) {
		D data = filledDTO().apply(converter);

		assertThat(fD.apply(data)).isNotNull();
	}

	@ParameterizedTest
	@MethodSource("getters")
	public <T> void dtoFieldShouldMapToDataField(Function<DTO, T> fDto, Function<D, T> fD) {
		DTO dto = filledDTO();
		D data = dto.apply(converter);

		assertThat(fDto.apply(dto)).isEqualTo(fD.apply(data));
	}

	@ParameterizedTest
	@MethodSource("idGetters")
	public <T, ID, ID_DTO extends IdDTO<T, ID>> void dtoIdFieldIsNotNull(Function<DTO, ID_DTO> fDto,
			Function<D, ID> fD) {
		DTO dto = filledDTO();

		assertThat(fDto.apply(dto)).isNotNull();
	}

	@ParameterizedTest
	@MethodSource("idGetters")
	public <T, ID, ID_DTO extends IdDTO<T, ID>> void dataIdFieldIsNotNull(Function<DTO, ID_DTO> fDto,
			Function<D, ID> fD) {
		D data = filledDTO().apply(converter);

		assertThat(fD.apply(data)).isNotNull();
	}

	@ParameterizedTest
	@MethodSource("idGetters")
	public <T, ID, ID_DTO extends IdDTO<T, ID>> void dtoIdFieldShouldMapToEntity(Function<DTO, ID_DTO> fDto,
			Function<D, ID> fD) {
		DTO dto = filledDTO();
		D data = dto.apply(converter);

		assertThat(fDto.apply(dto).getId()).isEqualTo(fD.apply(data));
	}

	@ParameterizedTest
	@MethodSource("idListGetters")
	public <T, ID, ID_DTO extends IdDTO<T, ID>> void dtoIdListFieldIsNotNullOrEmpty(
			Function<DTO, Collection<ID_DTO>> fDto,
			Function<D, Collection<ID>> fD) {
		DTO dto = filledDTO();

		assertThat(fDto.apply(dto)).isNotNull().isNotEmpty();
	}

	@ParameterizedTest
	@MethodSource("idListGetters")
	public <T, ID, ID_DTO extends IdDTO<T, ID>> void dataIdListFieldIsNotNullOrEmpty(
			Function<DTO, Collection<ID_DTO>> fDto,
			Function<D, Collection<ID>> fD) {
		DTO dto = filledDTO();
		D data = dto.apply(converter);

		assertThat(fD.apply(data)).isNotNull().isNotEmpty();
	}

	@ParameterizedTest
	@MethodSource("idListGetters")
	public <T, ID, ID_DTO extends IdDTO<T, ID>> void dtoIdListFieldShouldMapToEntity(
			Function<DTO, Collection<ID_DTO>> fDto,
			Function<D, Collection<ID>> fD) {
		DTO dto = filledDTO();
		D data = dto.apply(converter);

		assertThat(fDto.apply(dto).stream().map(IdDTO::getId))
				.containsExactlyInAnyOrderElementsOf(fD.apply(data));
	}

}
