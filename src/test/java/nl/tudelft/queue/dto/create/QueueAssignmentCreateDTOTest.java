/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.params.provider.Arguments;

import nl.tudelft.labracore.api.dto.AssignmentCreateDTO;

public class QueueAssignmentCreateDTOTest extends CreateTest<AssignmentCreateDTO, QueueAssignmentCreateDTO> {
	@Override
	protected QueueAssignmentCreateDTO filledDTO() {
		return QueueAssignmentCreateDTO.builder()
				.name("Oei")
				.moduleId(999L)
				.description("This is a simple yet complicated description")
				.deadline(LocalDateTime.of(2020, 9, 9, 20, 9, 19))
				.build();
	}

	@Override
	public List<Arguments> getters() {
		return List.of(
				of(QueueAssignmentCreateDTO::getName, AssignmentCreateDTO::getName),
				of(QueueAssignmentCreateDTO::getModuleId, acd -> acd.getModule().getId()),
				of(QueueAssignmentCreateDTO::getDeadline, AssignmentCreateDTO::getDeadline),
				of(QueueAssignmentCreateDTO::getDescription, acd -> acd.getDescription().getText()));
	}
}
