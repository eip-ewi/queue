/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.labs;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.params.provider.Arguments;

import nl.tudelft.queue.dto.create.QueueSessionCreateDTO;
import nl.tudelft.queue.dto.create.QueueSessionCreateDTOTest;
import nl.tudelft.queue.model.enums.CommunicationMethod;
import nl.tudelft.queue.model.enums.RequestType;
import nl.tudelft.queue.model.labs.Lab;

@SuppressWarnings("unchecked")
public abstract class LabCreateDTOTest<D extends Lab, DTO extends LabCreateDTO<D>, B extends LabCreateDTO.LabCreateDTOBuilder<?, DTO, ?>>
		extends QueueSessionCreateDTOTest<D, DTO, B> {
	protected abstract B initialBuilder();

	protected QueueSessionCreateDTO.QueueSessionCreateDTOBuilder<?, DTO, ?> buildFilledDTO(B builder) {
		return super.buildFilledDTO((B) builder
				.communicationMethod(CommunicationMethod.STUDENT_VISIT_TA)
				.requestTypes(Map.of(
						1L, Set.of(RequestType.QUESTION),
						2L, Set.of(RequestType.SUBMISSION))));
	}

	@Override
	protected DTO filledDTO() {
		return buildFilledDTO(initialBuilder()).build();
	}

	@Override
	public List<Arguments> getters() {
		return List.of(
				of(LabCreateDTO::getCommunicationMethod, Lab::getCommunicationMethod),
				of(LabCreateDTO::getModules, Lab::getModules));
	}
}
