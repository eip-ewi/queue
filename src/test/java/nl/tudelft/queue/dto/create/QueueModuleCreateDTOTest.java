/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import java.util.List;

import org.junit.jupiter.params.provider.Arguments;

import nl.tudelft.labracore.api.dto.ModuleCreateDTO;

public class QueueModuleCreateDTOTest extends CreateTest<ModuleCreateDTO, QueueModuleCreateDTO> {
	@Override
	protected QueueModuleCreateDTO filledDTO() {
		return QueueModuleCreateDTO.builder()
				.name("This is a name for a module")
				.editionId(78434L)
				.build();
	}

	@Override
	public List<Arguments> getters() {
		return List.of(
				of(QueueModuleCreateDTO::getName, ModuleCreateDTO::getName),
				of(QueueModuleCreateDTO::getEditionId, mcd -> mcd.getEdition().getId()));
	}
}
