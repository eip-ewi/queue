/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.misc;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AnnouncementTest {
	private Announcement announcement;

	@BeforeEach
	void setUp() {
		announcement = Announcement.builder()
				.startTime(LocalDateTime.of(1888, 8, 8, 8, 8, 8))
				.endTime(null)
				.isDismissible(false)
				.message("888888")
				.id(888L)
				.backgroundColour(888888)
				.textColour(14334944)
				.build();
	}

	@Test
	void hexBackgroundColourWorks() {
		assertThat(announcement.hexBackgroundColour())
				.isEqualToIgnoringCase("0D9038");
	}

	@Test
	void hexTextColourWorks() {
		assertThat(announcement.hexTextColour())
				.isEqualToIgnoringCase("DABBE0");
	}
}
