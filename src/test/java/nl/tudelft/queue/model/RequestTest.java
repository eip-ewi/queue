/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;

import org.assertj.core.data.Offset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.tudelft.queue.model.embeddables.RequestEventInfo;

public class RequestTest {

	private LabRequest request;

	@BeforeEach
	void setUp() {
		var now = LocalDateTime.now();

		request = LabRequest.builder()
				.createdAt(now.minusSeconds(60))
				.eventInfo(RequestEventInfo.builder()
						.firstProcessedAt(now.minusSeconds(30))
						.lastEventAt(now.minusSeconds(15))
						.build())
				.build();
	}

	@Test
	void waitingTimeCalculatesDifferenceBetweenCreatedAtAndFirstProcessedTime() {
		assertThat(request.waitingTime()).isPresent()
				.hasValueCloseTo(30.0, Offset.offset(0.005));
	}

	@Test
	void waitingTimeCalculationFailsForNullCreatedAt() {
		request.setCreatedAt(null);

		assertThat(request.waitingTime()).isEmpty();
	}

	@Test
	void waitingTimeCalculationFailsForNullFirstProcessedAt() {
		request.getEventInfo().setFirstProcessedAt(null);

		assertThat(request.waitingTime()).isEmpty();
	}
}
