/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model;

import static nl.tudelft.queue.model.enums.RequestStatus.*;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.assertj.core.data.TemporalUnitWithinOffset;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.tudelft.queue.model.embeddables.RequestEventInfo;
import nl.tudelft.queue.model.events.RequestApprovedEvent;
import nl.tudelft.queue.model.events.RequestForwardedToAnyEvent;
import nl.tudelft.queue.model.events.RequestRejectedEvent;

public class RequestEventInfoTest {
	private RequestEventInfo rei;

	private RequestEvent re1;
	private RequestEvent re2;
	private RequestEvent re3;

	@BeforeEach
	void setUp() {
		rei = RequestEventInfo.builder().build();

		re1 = new RequestRejectedEvent(null, 23L, "reason for TA", "reason for student");
		re2 = new RequestApprovedEvent(null, 23L, "reason for student");
		re3 = new RequestForwardedToAnyEvent(null, 23L, "reason for TA");
	}

	@Test
	void oneRequestEventShouldCauseChange() {
		rei.getEvents().add(re1);
		rei.apply(rei);

		assertThat(rei.getLastEventAt()).isCloseTo(LocalDateTime.now(),
				new TemporalUnitWithinOffset(1L, ChronoUnit.SECONDS));
		assertThat(rei.getStatus()).isEqualTo(REJECTED);
	}

	@Test
	void threeRequestEventsShouldCauseChange() {
		rei.getEvents().add(re1);
		rei.getEvents().add(re2);
		rei.getEvents().add(re3);
		rei.apply(rei);

		assertThat(rei.getLastEventAt()).isCloseTo(LocalDateTime.now(),
				new TemporalUnitWithinOffset(1L, ChronoUnit.SECONDS));
		assertThat(rei.getStatus()).isEqualTo(FORWARDED);
	}

	@Test
	void threeRequestEventsWithInterruption() {
		rei.getEvents().add(re1);
		rei.apply(rei);

		assertThat(rei.getStatus()).isEqualTo(REJECTED);

		rei.getEvents().add(re2);
		rei.apply(rei);

		assertThat(rei.getStatus()).isEqualTo(APPROVED);

		rei.getEvents().add(re3);
		rei.apply(rei);

		assertThat(rei.getLastEventAt()).isCloseTo(LocalDateTime.now(),
				new TemporalUnitWithinOffset(1L, ChronoUnit.SECONDS));
		assertThat(rei.getStatus()).isEqualTo(FORWARDED);
	}
}
