/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.enums;

import static nl.tudelft.queue.model.enums.RequestType.QUESTION;
import static nl.tudelft.queue.model.enums.RequestType.SUBMISSION;
import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

public class RequestTypeTest {

	@Test
	void toSentenceIsNotTheSameForQuestionAndSubmission() {
		assertThat(QUESTION.toSentence())
				.isNotEqualTo(SUBMISSION.toSentence());
	}

	@ParameterizedTest
	@EnumSource(RequestType.class)
	void displayNameIsSameAsName(RequestType type) {
		assertThat(type.displayName()).isEqualToIgnoringCase(type.name());
	}

	@ParameterizedTest
	@EnumSource(RequestType.class)
	void displayNameIsCapitalized(RequestType type) {
		assertThat(type.displayName().charAt(0)).isUpperCase();
	}

}
