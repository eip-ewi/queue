/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.embeddables;

import static java.time.LocalDateTime.now;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SlotTest {

	private Slot slot1;
	private Slot slot2;
	private Slot slot3;

	@BeforeEach
	void setUp() {
		slot1 = Slot.builder()
				.opensAt(now().minusMinutes(3L))
				.closesAt(now().plusHours(4))
				.build();

		slot2 = Slot.builder()
				.opensAt(now().plusDays(1))
				.closesAt(now().plusDays(1).plusHours(4))
				.build();

		slot3 = Slot.builder()
				.opensAt(now().minusDays(1))
				.closesAt(now().minusHours(1))
				.build();
	}

	@Test
	void todaySlotShouldReturnTrueForTodayFunction() {
		assertThat(slot1.today()).isTrue();
	}

	@Test
	void tomorrowSlotShouldReturnFalseForTodayFunction() {
		assertThat(slot2.today()).isFalse();
	}

	@Test
	void currentSlotShouldBeOpen() {
		assertThat(slot1.open()).isTrue();
	}

	@Test
	void tomorrowSlotShouldBeClosed() {
		assertThat(slot2.open()).isFalse();
	}

	@Test
	void yesterdaySlotShouldBeClosed() {
		assertThat(slot3.open()).isFalse();
	}

	@Test
	void sentenceContainsNoOnIfToday() {
		assertThat(slot1.today()).isTrue();
		assertThat(slot1.toSentence()).doesNotContain(" on ");
	}

	@Test
	void sentenceContainsOnIfNotToday() {
		assertThat(slot2.today()).isFalse();
		assertThat(slot2.toSentence()).contains(" on ");
	}

}
