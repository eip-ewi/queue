/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model;

import static org.mockito.Mockito.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.tudelft.queue.dto.view.RequestEventViewDTO;
import nl.tudelft.queue.model.embeddables.RequestEventInfo;
import nl.tudelft.queue.model.enums.RequestStatus;

public class RequestEventConcreteTest {

	private RequestEvent event;

	private RequestEventInfo info;

	@BeforeEach
	void setUp() {
		info = mock(RequestEventInfo.class);

		event = new RequestEvent<LabRequest>() {
			@Override
			public Class<? extends RequestEventViewDTO<? extends RequestEvent<LabRequest>>> viewClass() {
				return null;
			}

			@Override
			public void apply(RequestEventInfo target) {
			}
		};
		event.setTimestamp(LocalDateTime.now());
	}

	@Test
	void applySetsStatus() {
		event.apply(info, RequestStatus.REVOKED);

		verify(info).setStatus(RequestStatus.REVOKED);
	}

	@Test
	void applySetsLastEventAt() {
		event.apply(info, RequestStatus.REVOKED);

		verify(info).setLastEventAt(event.getTimestamp());
	}

	@Test
	void applySetsHandledAtForApprove() {
		event.apply(info, RequestStatus.APPROVED);

		verify(info).setHandledAt(event.getTimestamp());
	}

	@Test
	void applySetsFinishedAtForApprove() {
		event.apply(info, RequestStatus.APPROVED);

		verify(info).setFinishedAt(event.getTimestamp());
	}

	@Test
	void applySetsFinishedAtForRevoked() {
		event.apply(info, RequestStatus.APPROVED);

		verify(info).setFinishedAt(event.getTimestamp());
	}

	@Test
	void applyResetsReasons() {
		event.apply(info, RequestStatus.REVOKED);

		verify(info).setReasonForAssistant(null);
		verify(info).setReasonForStudent(null);
	}

	@Test
	void applyWithAssignedToSetsAssignedTo() {
		event.apply(info, RequestStatus.REVOKED, 7832L);

		verify(info).setAssignedTo(7832L);
	}

	@Test
	void applyWithAssignedToDoesntSetLastAssignedAtWhenRevoked() {
		event.apply(info, RequestStatus.REVOKED, 7832L);

		verify(info, never()).setLastAssignedAt(event.getTimestamp());
	}

	@Test
	void applyWithAssignedToSetsLastAssignedAtWhenProcessing() {
		event.apply(info, RequestStatus.PROCESSING, 452L);

		verify(info).setLastAssignedAt(event.getTimestamp());
	}

	@Test
	void applyWithReasonsSetsReasons() {
		event.apply(info, RequestStatus.REVOKED, "assistant", "student");

		verify(info).setReasonForStudent("student");
		verify(info).setReasonForAssistant("assistant");
	}

	@Test
	void applyWithAssignedToAndReasonsSetsAll() {
		event.apply(info, RequestStatus.REVOKED, 423L, "assistant", "student");

		verify(info).setReasonForAssistant("assistant");
		verify(info).setReasonForStudent("student");

		verify(info).setAssignedTo(423L);
	}
}
