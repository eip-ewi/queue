/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.labracore;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.api.CourseControllerApi;
import nl.tudelft.labracore.api.dto.CourseDetailsDTO;
import nl.tudelft.labracore.api.dto.CourseSummaryDTO;

@AllArgsConstructor
public class CourseApiMocker extends LabracoreApiMocker<CourseDetailsDTO, Long> {
	private final CourseControllerApi cApi;

	@Override
	public void mock() {
		when(cApi.getAllCourses()).thenAnswer(getAll(CourseSummaryDTO.class));
		when(cApi.getAllCoursesById(any())).thenAnswer(getAllByIds);
	}

	@Override
	public Long getId(CourseDetailsDTO dto) {
		return dto.getId();
	}
}
