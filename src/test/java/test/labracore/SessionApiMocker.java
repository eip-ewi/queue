/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.labracore;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.api.SessionControllerApi;
import nl.tudelft.labracore.api.dto.SessionDetailsDTO;
import reactor.core.publisher.Flux;

@AllArgsConstructor
@SuppressWarnings("unchecked")
public class SessionApiMocker extends LabracoreApiMocker<SessionDetailsDTO, Long> {
	private final SessionControllerApi sApi;

	@Override
	public void mock() {
		when(sApi.getSessionsById(any())).thenAnswer(getAllByIds);

		when(sApi.getActiveSessionsInEditionsWithGracePeriod(any(), any())).thenAnswer(invocation -> {
			Set<Long> editionIds = new HashSet<Long>(invocation.getArgument(0, List.class));
			Integer gracePeriod = invocation.getArgument(1);

			var now = LocalDateTime.now();

			return Flux.fromIterable(data.values())
					.filter(s -> s.getStart().isBefore(now) &&
							now.isBefore(s.getEndTime().plusMinutes(gracePeriod)) &&
							s.getEditions().stream()
									.anyMatch(edition -> editionIds.contains(edition.getId())));
		});
	}

	@Override
	public Long getId(SessionDetailsDTO sessionDetailsDTO) {
		return sessionDetailsDTO.getId();
	}
}
