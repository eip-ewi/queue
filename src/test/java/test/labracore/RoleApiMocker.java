/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.labracore;

import static nl.tudelft.labracore.api.dto.RoleDetailsDTO.TypeEnum.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.api.AuthorizationControllerApi;
import nl.tudelft.labracore.api.EditionControllerApi;
import nl.tudelft.labracore.api.PersonControllerApi;
import nl.tudelft.labracore.api.RoleControllerApi;
import nl.tudelft.labracore.api.dto.RoleDetailsDTO;
import nl.tudelft.labracore.api.dto.RoleEditionDetailsDTO;
import nl.tudelft.labracore.api.dto.RoleId;
import nl.tudelft.labracore.api.dto.RolePersonDetailsDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class RoleApiMocker extends LabracoreApiMocker<RoleDetailsDTO, RoleId> {
	private final MultiValueMap<Long, RoleDetailsDTO> rolesByEdition = new LinkedMultiValueMap<>();
	private final MultiValueMap<Long, RoleDetailsDTO> rolesByPerson = new LinkedMultiValueMap<>();
	private final Set<Long> usersWithStaffRole = new HashSet<>();

	private final AuthorizationControllerApi aApi;

	private final PersonControllerApi pApi;

	private final EditionControllerApi eApi;

	private final RoleControllerApi rApi;

	@Override
	@SuppressWarnings("unchecked")
	public void mock() {
		when(eApi.getEditionParticipants(any())).thenAnswer(invocation -> {
			Long id = invocation.getArgument(0);
			return Flux.fromIterable(rolesByEdition.getOrDefault(id, List.of()))
					.map(dto -> mapper.map(dto, RolePersonDetailsDTO.class));
		});

		when(pApi.getRolesForPerson(any())).thenAnswer(invocation -> {
			Long id = invocation.getArgument(0);
			return Flux.fromIterable(rolesByPerson.getOrDefault(id, List.of()))
					.map(dto -> mapper.map(dto, RoleEditionDetailsDTO.class));
		});

		when(rApi.getRolesByEditions(any())).thenAnswer(invocation -> {
			Set<Long> ids = invocation.getArgument(0);
			return Flux.fromIterable(ids)
					.flatMapIterable(id -> rolesByEdition.getOrDefault(id, List.of()))
					.map(dto -> mapper.map(dto, RolePersonDetailsDTO.class));
		});

		when(rApi.getRolesById(any(), any())).thenAnswer(invocation -> {
			HashSet<Long> editionIds = new HashSet<Long>(invocation.getArgument(0, Set.class));
			HashSet<Long> peopleIds = new HashSet<Long>(invocation.getArgument(1, Set.class));

			return Flux.fromIterable(data.values())
					.filter(role -> editionIds.contains(role.getEdition().getId())
							&& peopleIds.contains(role.getPerson().getId()));
		});

		when(aApi.hasStaffRoleInAnyEdition(any())).thenAnswer(invocation -> {
			Long id = invocation.getArgument(0);
			return Mono.just(usersWithStaffRole.contains(id));
		});
	}

	@Override
	public RoleDetailsDTO save(RoleDetailsDTO dto) {
		super.save(dto);

		rolesByEdition.add(Objects.requireNonNull(getId(dto).getEditionId()), dto);
		rolesByPerson.add(Objects.requireNonNull(getId(dto).getPersonId()), dto);

		if (Set.of(TA, HEAD_TA, TEACHER, TEACHER_RO).contains(dto.getType())) {
			usersWithStaffRole.add(dto.getPerson().getId());
		}

		return dto;
	}

	@Override
	public RoleId getId(RoleDetailsDTO roleDetailsDTO) {
		return roleDetailsDTO.getId();
	}
}
