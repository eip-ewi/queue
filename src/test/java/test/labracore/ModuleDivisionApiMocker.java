/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.labracore;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.Objects;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.api.ModuleDivisionControllerApi;
import nl.tudelft.labracore.api.dto.ModuleDivisionDetailsDTO;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class ModuleDivisionApiMocker extends LabracoreApiMocker<ModuleDivisionDetailsDTO, Long> {

	private final ModuleDivisionControllerApi mdApi;

	private final MultiValueMap<Long, ModuleDivisionDetailsDTO> divisionsByModule = new LinkedMultiValueMap<>();

	@Override
	public void mock() {
		when(mdApi.getAllDivisionsInModule(any())).thenAnswer(invocation -> {
			Long id = invocation.getArgument(0);
			return Flux.fromIterable(divisionsByModule.getOrDefault(id, List.of()));
		});

		when(mdApi.getPeopleInDivision(any())).thenAnswer(getOneByMap(data));
	}

	@Override
	public ModuleDivisionDetailsDTO save(ModuleDivisionDetailsDTO dto) {
		super.save(dto);

		divisionsByModule.add(Objects.requireNonNull(dto.getModule().getId()), dto);

		return dto;
	}

	@Override
	public Long getId(ModuleDivisionDetailsDTO dto) {
		return dto.getId();
	}
}
