/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.labracore;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

import java.util.*;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.api.PersonControllerApi;
import nl.tudelft.labracore.api.dto.PersonDetailsDTO;
import nl.tudelft.labracore.api.dto.PersonSummaryDTO;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class PersonApiMocker extends LabracoreApiMocker<PersonSummaryDTO, Long> {
	private transient final Map<String, PersonSummaryDTO> peopleByUsername = new HashMap<>();

	private final PersonControllerApi pApi;

	@Override
	public void mock() {
		when(pApi.getPersonById(any())).thenAnswer(getOneByMap(data, PersonDetailsDTO.class));
		when(pApi.getPeopleById(any())).thenAnswer(getAllByIds);
		when(pApi.getPersonByExternalId(any()))
				.thenAnswer(getOneByMap(peopleByUsername, PersonDetailsDTO.class));
		when(pApi.getPersonByUsername(any()))
				.thenAnswer(getOneByMap(peopleByUsername, PersonDetailsDTO.class));
		when(pApi.searchForPeopleByIdentifier(anyString(), anyInt(), anyInt()))
				.thenAnswer(invocation -> {
					String identifier = invocation.getArgument(0);
					return mockSearchLogic(identifier);
				});
	}

	@Override
	public PersonSummaryDTO save(PersonSummaryDTO dto) {
		super.save(dto);

		peopleByUsername.put(dto.getUsername(), dto);

		return dto;
	}

	@Override
	public Long getId(PersonSummaryDTO dto) {
		return dto.getId();
	}

	public Optional<PersonSummaryDTO> getByUsername(String username) {
		return Optional.ofNullable(peopleByUsername.get(username));
	}

	private Flux<PersonSummaryDTO> mockSearchLogic(String identifier) {
		List<PersonSummaryDTO> result = data.values().stream()
				.filter(person -> person.getDisplayName().contains(identifier) ||
						person.getUsername().contains(identifier) ||
						String.valueOf(person.getNumber()).contains(identifier) ||
						Objects.equals(person.getEmail(), identifier))
				.toList();
		return Flux.fromIterable(result);
	}

}
