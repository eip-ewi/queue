/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.labracore;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.tudelft.labracore.api.StudentGroupControllerApi;
import nl.tudelft.labracore.api.dto.StudentGroupDetailsDTO;
import nl.tudelft.labracore.api.dto.StudentGroupSummaryDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@AllArgsConstructor
public class StudentGroupApiMocker extends LabracoreApiMocker<StudentGroupDetailsDTO, Long> {
	private transient final MultiValueMap<Long, StudentGroupDetailsDTO> groupsByPerson = new LinkedMultiValueMap<>();
	private transient final Map<PersonAndModule, StudentGroupDetailsDTO> groupsByPersonAndModule = new HashMap<>();

	private final StudentGroupControllerApi sgApi;

	@Override
	public void mock() {
		when(sgApi.getStudentGroupsById(any())).thenAnswer(getAllByIds);

		when(sgApi.getGroupForPersonAndModule(any(), any())).thenAnswer(invocation -> {
			Long personId = invocation.getArgument(0, Long.class);
			Long moduleId = invocation.getArgument(1, Long.class);

			return Mono.justOrEmpty(groupsByPersonAndModule.get(new PersonAndModule(personId, moduleId)));
		});

		when(sgApi.getGroupsForPerson(any())).thenAnswer(invocation -> {
			Long id = invocation.getArgument(0);
			return Flux.fromIterable(groupsByPerson.getOrDefault(id, List.of()))
					.map(sg -> mapper.map(sg, StudentGroupSummaryDTO.class));
		});
	}

	@Override
	public StudentGroupDetailsDTO save(StudentGroupDetailsDTO sg) {
		super.save(sg);

		sg.getMembers().forEach(member -> {
			groupsByPersonAndModule
					.put(new PersonAndModule(member.getPerson().getId(), sg.getModule().getId()), sg);
			groupsByPerson.add(member.getPerson().getId(), sg);
		});

		return sg;
	}

	public Optional<StudentGroupDetailsDTO> getByPersonAndModule(Long person, Long module) {
		return Optional.ofNullable(groupsByPersonAndModule.get(new PersonAndModule(person, module)));
	}

	@Override
	public Long getId(StudentGroupDetailsDTO studentGroupDetailsDTO) {
		return studentGroupDetailsDTO.getId();
	}

	@Data
	@AllArgsConstructor
	private static class PersonAndModule {
		private Long personId;
		private Long moduleId;
	}
}
