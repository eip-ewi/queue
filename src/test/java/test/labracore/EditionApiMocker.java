/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.labracore;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.tudelft.labracore.api.EditionControllerApi;
import nl.tudelft.labracore.api.dto.EditionDetailsDTO;

@AllArgsConstructor
public class EditionApiMocker extends LabracoreApiMocker<EditionDetailsDTO, Long> {
	@Getter
	private final EditionControllerApi eApi;

	@Override
	public void mock() {
		when(eApi.getEditionsById(any())).thenAnswer(getAllByIds);
		when(eApi.getEditionById(any())).thenAnswer(getOneByMap(data));
		when(eApi.getAllEditions()).thenAnswer(getAll);
	}

	@Override
	public Long getId(EditionDetailsDTO dto) {
		return dto.getId();
	}
}
