/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.labracore;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.api.RoomControllerApi;
import nl.tudelft.labracore.api.dto.RoomDetailsDTO;
import nl.tudelft.labracore.api.dto.RoomSummaryDTO;
import reactor.core.publisher.Flux;

@AllArgsConstructor
public class RoomApiMocker extends LabracoreApiMocker<RoomDetailsDTO, Long> {
	private final RoomControllerApi rApi;

	@Override
	public void mock() {
		when(rApi.getAllRoomsById(any())).thenAnswer(getAllByIds);

		when(rApi.getAllRooms()).thenAnswer(invocation -> {
			var summaries = data.values().stream()
					.map(dto -> mapper.map(dto, RoomSummaryDTO.class))
					.collect(Collectors.toList());

			return Flux.fromIterable(summaries);
		});
	}

	@Override
	public Long getId(RoomDetailsDTO dto) {
		return dto.getId();
	}
}
