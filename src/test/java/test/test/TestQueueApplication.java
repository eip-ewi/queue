/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.test;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

import nl.tudelft.labracore.lib.LabracoreApiConfig;
import nl.tudelft.queue.QueueApplication;
import test.BaseMockConfig;
import test.TestApiMockers;
import test.TestDatabaseLoader;
import test.labracore.LabracoreApiMocker;

@SpringBootApplication
@PropertySource("classpath:application-test.properties")
@ComponentScan(basePackageClasses = LabracoreApiMocker.class)
@Import({
		LabracoreApiConfig.class,
		BaseMockConfig.class,
		TestDatabaseLoader.class,
		TestUserDetailsService.class,
		TestApiMockers.class
})
public class TestQueueApplication extends QueueApplication {
}
