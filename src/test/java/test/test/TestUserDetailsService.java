/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import nl.tudelft.labracore.api.dto.PersonSummaryDTO;
import nl.tudelft.labracore.lib.security.LabradorUserDetails;
import nl.tudelft.labracore.lib.security.user.DefaultRole;
import nl.tudelft.labracore.lib.security.user.Person;
import test.labracore.PersonApiMocker;

@Primary
@Service
@DependsOn("personApiMocker")
public class TestUserDetailsService implements UserDetailsService {
	@Autowired
	private PersonApiMocker pApiMocker;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		var person = pApiMocker.getByUsername(username)
				.orElseThrow(() -> new UsernameNotFoundException(username));

		return new LabradorUserDetails(Person.builder()
				.id(person.getId())
				.username(person.getUsername())
				.email(person.getEmail())
				.displayName(person.getDisplayName())
				.defaultRole(defaultRoleFromDetails(person))
				.number(person.getNumber())
				.build());
	}

	private DefaultRole defaultRoleFromDetails(PersonSummaryDTO person) {
		switch (person.getDefaultRole()) {
			case STUDENT:
				return DefaultRole.STUDENT;
			case ADMIN:
				return DefaultRole.ADMIN;
			case TEACHER:
				return DefaultRole.TEACHER;
		}
		throw new RuntimeException("Cannot translate default role: " + person.getDefaultRole());
	}
}
