/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Configuration;

import nl.tudelft.labracore.api.*;
import nl.tudelft.queue.cache.CourseCacheManager;

@Configuration
public class BaseMockConfig {

	@MockBean
	private AssignmentControllerApi assignmentControllerApi;

	@MockBean
	private AuthorizationControllerApi authorizationControllerApi;

	@MockBean
	private ClusterControllerApi clusterControllerApi;

	@MockBean
	private CohortControllerApi cohortControllerApi;

	@MockBean
	private CourseControllerApi courseControllerApi;

	@MockBean
	private EditionControllerApi editionControllerApi;

	@MockBean
	private EditionCollectionControllerApi editionCollectionControllerApi;

	@MockBean
	private ModuleControllerApi moduleControllerApi;

	@MockBean
	private SessionControllerApi sessionControllerApi;

	@MockBean
	private PersonControllerApi personControllerApi;

	@MockBean
	private ProgramControllerApi programControllerApi;

	@MockBean
	private RoleControllerApi roleControllerApi;

	@MockBean
	private RoomControllerApi roomControllerApi;

	@MockBean
	private StudentGroupControllerApi studentGroupControllerApi;

	@MockBean
	private CourseCacheManager courseCacheManager;

	@MockBean
	private QuestionControllerApi questionControllerApi;

	@MockBean
	private ModuleDivisionControllerApi moduleDivisionControllerApi;
}
