/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import nl.tudelft.labracore.api.*;
import test.labracore.*;

@Configuration
public class TestApiMockers {
	@Autowired
	private AssignmentControllerApi aApi;

	@Autowired
	private AuthorizationControllerApi authApi;

	@Autowired
	private ClusterControllerApi clApi;

	@Autowired
	private CohortControllerApi coApi;

	@Autowired
	private CourseControllerApi cApi;

	@Autowired
	private EditionControllerApi eApi;

	@Autowired
	private EditionCollectionControllerApi ecApi;

	@Autowired
	private ModuleControllerApi mApi;

	@Autowired
	private ModuleDivisionControllerApi mdApi;

	@Autowired
	private PersonControllerApi pApi;

	@Autowired
	private ProgramControllerApi prApi;

	@Autowired
	private RoleControllerApi rlApi;

	@Autowired
	private RoomControllerApi rApi;

	@Autowired
	private SessionControllerApi sApi;

	@Autowired
	private StudentGroupControllerApi sgApi;

	@Bean
	public AssignmentApiMocker assignmentApiMocker() {
		return new AssignmentApiMocker(aApi);
	}

	@Bean
	public ClusterApiMocker clusterApiMocker() {
		return new ClusterApiMocker(clApi);
	}

	@Bean
	public CohortApiMocker cohortApiMocker() {
		return new CohortApiMocker(coApi);
	}

	@Bean
	public CourseApiMocker courseApiMocker() {
		return new CourseApiMocker(cApi);
	}

	@Bean
	public EditionApiMocker editionApiMocker() {
		return new EditionApiMocker(eApi);
	}

	@Bean
	public EditionCollectionApiMocker editionCollectionApiMocker() {
		return new EditionCollectionApiMocker(ecApi);
	}

	@Bean
	public ModuleApiMocker moduleApiMocker() {
		return new ModuleApiMocker(mApi);
	}

	@Bean
	public ModuleDivisionApiMocker moduleDivisionApiMocker() {
		return new ModuleDivisionApiMocker(mdApi);
	}

	@Bean
	public PersonApiMocker personApiMocker() {
		return new PersonApiMocker(pApi);
	}

	@Bean
	public ProgramApiMocker programApiMocker() {
		return new ProgramApiMocker(prApi);
	}

	@Bean
	public RoleApiMocker roleApiMocker() {
		return new RoleApiMocker(authApi, pApi, eApi, rlApi);
	}

	@Bean
	public RoomApiMocker roomApiMocker() {
		return new RoomApiMocker(rApi);
	}

	@Bean
	public SessionApiMocker sessionApiMocker() {
		return new SessionApiMocker(sApi);
	}

	@Bean
	public StudentGroupApiMocker studentGroupApiMocker() {
		return new StudentGroupApiMocker(sgApi);
	}
}
