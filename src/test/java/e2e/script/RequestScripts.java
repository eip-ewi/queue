/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package e2e.script;

import java.util.regex.Pattern;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;

import e2e.E2E;
import e2e.data.RequestInfo;

public final class RequestScripts {

	public static RequestInfo getNextRequest(E2E e2e, String labName) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/requests"));

		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Get next for " + labName))
				.click();

		page.waitForURL(Pattern.compile(".*/request/\\d+"));
		long requestId = Long.parseLong(page.url().substring(page.url().lastIndexOf('/') + 1));

		return getRequestInfo(e2e, requestId);
	}

	public static RequestInfo getRequestInfo(E2E e2e, long requestId) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/request/" + requestId));
		String requester = page.locator("dt:text('Request by') + *").innerText();
		String course = page.locator("dt:text('Course') + *").innerText();
		String lab = page.locator("dt:text('Lab') + *").innerText();
		String assignment = page.locator("dt:text('Assignment') + *").innerText();
		String room = page.locator("dt:text('Room') + *").innerText();
		String type = page.locator("dt:text('Type') + *").innerText();
		String question = page.locator("dt:text('Question') + *").innerText();
		String assistant = e2e.optional(page.locator("dt:text('Assistant') + *"), 200).map(Locator::innerText)
				.orElse(null);
		String status = page.locator("dt:text('Status') + *").innerText();
		return new RequestInfo(requestId, requester, course, lab, assignment, room, type, question, assistant,
				status);
	}

	public static void approveRequest(E2E e2e, long requestId) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/request/" + requestId));
		page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Approve")).click();
		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Approve")).click();
	}

	public static void rejectRequest(E2E e2e, long requestId, String reason) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/request/" + requestId));
		page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Reject")).click();
		page.locator("[name='reasonForStudent']").fill(reason);
		page.locator("[name='reasonForAssistant']").fill(reason);
		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Reject")).click();
	}

	public static void forwardRequest(E2E e2e, long requestId, String to, String reason) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/request/" + requestId));
		page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Forward")).click();
		if (to != null) {
			page.locator("[name='assistant']").selectOption(to);
		}
		page.locator("[name='reasonForAssistant']").fill(reason);
		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Forward")).click();
	}

	public static void notFoundRequest(E2E e2e, long requestId) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/request/" + requestId));
		page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Not found")).click();
	}

}
