/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package e2e.script;

import java.util.HashSet;
import java.util.Set;

import com.microsoft.playwright.Page;

import e2e.E2E;

public final class UserScripts {

	public static void login(E2E e2e, String user) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/login"));
		page.getByLabel("Username").click();
		page.getByLabel("Username").fill(user);
		page.getByLabel("Username").press("Tab");
		page.getByLabel("Password").fill(user);
		page.getByLabel("Password").press("Enter");
		acceptCoC(e2e, user);
	}

	private static final Set<String> cocAccepted = new HashSet<>();

	public static void acceptCoC(E2E e2e, String user) {
		if (!cocAccepted.contains(user)) {
			e2e.page().locator("#coc-approve").click();
			cocAccepted.add(user);
		}
	}

}
