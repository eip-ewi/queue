/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package e2e.script;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.List;
import java.util.regex.Pattern;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;

import e2e.E2E;

public final class SessionScripts {

	private static final DateTimeFormatter formatter = new DateTimeFormatterBuilder()
			.appendPattern("yyyy-MM-dd'T'HH:mm").toFormatter();

	public static long createRegularSession(E2E e2e, long editionId, String name, List<String> moduleNames,
			List<String> assignmentNames) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/edition/" + editionId + "/labs"));

		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Create session")).click();
		page.locator("#session-type").selectOption("REGULAR");
		page.locator("#create-session-modal")
				.getByRole(AriaRole.BUTTON, new Locator.GetByRoleOptions().setName("Create")).click();

		page.locator("#title-input").fill(name);

		page.locator("label[for='direction-select'] + * .select__button").click();
		page.getByRole(AriaRole.OPTION, new Page.GetByRoleOptions().setName("TA visits student")).click();

		page.locator("#opens-at").fill(LocalDateTime.now().format(formatter));
		page.locator("#closes-at").fill(LocalDateTime.now().plusHours(4).format(formatter));

		page.locator("label[for='building-select'] + * .select__button").click();
		page.getByRole(AriaRole.OPTION, new Page.GetByRoleOptions().setName("EWI")).click();
		page.locator("label[for='building-select'] + * .select__button").click();

		page.locator("label[for='room-select'] + * .select__button").click();
		page.getByRole(AriaRole.OPTION, new Page.GetByRoleOptions().setName("Ampère")).click();
		page.getByRole(AriaRole.OPTION, new Page.GetByRoleOptions().setName("Boole")).click();
		page.locator("label[for='room-select'] + * .select__button").click();

		page.locator("label[for='module-select'] + * .select__button").click();
		moduleNames.forEach(moduleName -> {
			page.getByRole(AriaRole.OPTION, new Page.GetByRoleOptions().setName(moduleName)).click();
		});

		assignmentNames.forEach(assignmentName -> {
			page.click("td:text('" + assignmentName + "')");
			Locator select = page.locator("td:text('" + assignmentName + "') + td .select");
			select.locator(".select__button").click();
			select.getByRole(AriaRole.OPTION, new Locator.GetByRoleOptions().setName("Question")).click();
		});

		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Create new session")).click();

		String url = page
				.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName(Pattern.compile(name + ".*")))
				.getAttribute("href");
		return Long.parseLong(url.substring(url.lastIndexOf('/') + 1));
	}

	public static void enqueueForSession(E2E e2e, String sessionName, String assignmentName, String roomName,
			String question) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/"));

		page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName(sessionName)).click();
		page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Enqueue")).click();

		page.locator("label[for='input-assignment'] + * .select__button").click();
		page.getByRole(AriaRole.OPTION, new Page.GetByRoleOptions().setName(assignmentName)).click();

		page.locator("label[for='input-type'] + * .select__button").click();
		page.getByRole(AriaRole.OPTION, new Page.GetByRoleOptions().setName("Question")).click();

		page.locator("label[for='input-room'] + * .select__button").click();
		page.getByRole(AriaRole.OPTION, new Page.GetByRoleOptions().setName(roomName)).click();

		page.locator("#input-question").fill(question);

		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Enqueue")).click();
	}

}
