/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package e2e.script;

import java.util.regex.Pattern;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;

import e2e.E2E;

public final class ModuleEndToEnd {

	public static long createModule(E2E e2e, long editionId, String name) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/edition/" + editionId + "/modules"));

		page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Create Module")).click();

		Locator nameInput = page.locator("#title-input");
		nameInput.click();
		nameInput.fill(name);

		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Create new module")).click();

		page.waitForURL(Pattern.compile(".*/edition/\\d+/modules"));

		return Long.parseLong(page.locator("#module-select")
				.getByRole(AriaRole.OPTION, new Locator.GetByRoleOptions().setName(name))
				.getAttribute("value"));
	}

}
