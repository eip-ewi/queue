/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package e2e.script;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.regex.Pattern;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.TimeoutError;
import com.microsoft.playwright.options.AriaRole;

import e2e.E2E;

public final class EditionScripts {

	private static final DateTimeFormatter formatter = new DateTimeFormatterBuilder()
			.appendPattern("yyyy-MM-dd'T'HH:mm").toFormatter();

	public static long createEdition(E2E e2e, String name) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/edition/add"));

		// This user has courses
		assertThat(page.locator("#course-select").inputValue()).isNotBlank();

		Locator editionName = page.locator("#edition-name");
		editionName.click();
		editionName.fill(name);

		// There are cohorts
		assertThat(page.locator("#cohort-select").inputValue()).isNotBlank();

		Locator from = page.locator("#slot-from");
		from.click();
		from.fill(LocalDateTime.now().minusDays(1).format(formatter));

		Locator to = page.locator("#slot-to");
		to.click();
		to.fill(LocalDateTime.now().plusDays(1).format(formatter));

		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Create edition")).click();

		page.waitForURL(Pattern.compile(".*/edition/\\d+"));
		String url = page.url();
		return Long.parseLong(url.substring(url.lastIndexOf('/') + 1));
	}

	public static void addParticipantToEdition(E2E e2e, long editionId, String username, String role) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/edition/" + editionId + "/participants"));

		page.getByRole(AriaRole.LINK, new Page.GetByRoleOptions().setName("Add participant")).click();

		page.locator("[for='inputRole'] + div .select__button").click();
		page.getByRole(AriaRole.OPTION,
				new Page.GetByRoleOptions().setName(Pattern.compile("^" + role + ".*")).setExact(true))
				.click();

		page.locator("#inputIdentifier").fill(username);

		page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Add"))
				.click();
	}

	public static void enrolForEdition(E2E e2e, long editionId) {
		Page page = e2e.page();
		page.navigate(e2e.withPath("/edition/" + editionId));

		Locator enrol = page.getByRole(AriaRole.LINK,
				new Page.GetByRoleOptions().setName("Enrol for this edition"));
		try {
			enrol.waitFor(new Locator.WaitForOptions().setTimeout(250));
			enrol.click();
			page.getByRole(AriaRole.BUTTON, new Page.GetByRoleOptions().setName("Enrol")).click();
		} catch (TimeoutError ignored) {
		}
	}

	public static void navigateToEdition(E2E e2e, Page page, long id) {
		page.navigate(e2e.withPath("/edition/" + id));
	}

}
