--
-- Queue - A Queueing system that can be used to handle labs in higher education
-- Copyright (C) 2016-2024  Delft University of Technology
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
--

USE labracore;

INSERT INTO apikey (created_at, enabled, expires_at, uuid, name, secret, created_by_id) VALUES
    ('2024-01-01 00:00', b'1', '2034-01-01 00:00', 'queue-key', 'Queue Key', 'queue-secret', 1);
INSERT INTO apikey_permissions (keys_id, permissions_id) SELECT 2, permissions_id FROM apikey_permissions;

INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
    SELECT b'0', 3, 'CSE Teacher 1', 'cseteacher1@tudelft.nl', 'cseteacher1', 10, b'0', 'cseteacher1', id FROM person ORDER BY id DESC LIMIT 1;
INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
SELECT b'0', 3, 'CSE Teacher 2', 'cseteacher2@tudelft.nl', 'cseteacher2', 11, b'0', 'cseteacher2', id FROM person ORDER BY id DESC LIMIT 1;

INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
SELECT b'0', 0, 'CSE Student 1', 'csestudent1@tudelft.nl', 'csestudent1', 21, b'0', 'csestudent1', id FROM person ORDER BY id DESC LIMIT 1;
INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
SELECT b'0', 0, 'CSE Student 2', 'csestudent2@tudelft.nl', 'csestudent2', 22, b'0', 'csestudent2', id FROM person ORDER BY id DESC LIMIT 1;
INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
SELECT b'0', 0, 'CSE Student 3', 'csestudent3@tudelft.nl', 'csestudent3', 23, b'0', 'csestudent3', id FROM person ORDER BY id DESC LIMIT 1;
INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
SELECT b'0', 0, 'CSE Student 4', 'csestudent4@tudelft.nl', 'csestudent4', 24, b'0', 'csestudent4', id FROM person ORDER BY id DESC LIMIT 1;
INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
SELECT b'0', 0, 'CSE Student 5', 'csestudent4@tudelft.nl', 'csestudent5', 25, b'0', 'csestudent5', id FROM person ORDER BY id DESC LIMIT 1;
INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
SELECT b'0', 0, 'CSE Student 6', 'csestudent6@tudelft.nl', 'csestudent6', 26, b'0', 'csestudent6', id FROM person ORDER BY id DESC LIMIT 1;
INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
SELECT b'0', 0, 'CSE Student 7', 'csestudent7@tudelft.nl', 'csestudent7', 27, b'0', 'csestudent7', id FROM person ORDER BY id DESC LIMIT 1;
INSERT INTO person () VALUES ();
insert into concrete_person (account_disabled, default_role, display_name, email, external_id, number, two_fa_enabled, username, id)
SELECT b'0', 0, 'CSE Student 8', 'csestudent8@tudelft.nl', 'csestudent8', 28, b'0', 'csestudent8', id FROM person ORDER BY id DESC LIMIT 1;

INSERT INTO program (name, director_id, type) VALUES ('Computer Science and Engineering', 1, 0);

INSERT INTO course (code, is_archived, name, program_id) VALUES ('CSE1100', b'0', 'Introduction to Programming', 1);
INSERT INTO course (code, is_archived, name, program_id) VALUES ('CSE1300', b'0', 'Reasoning and Logic', 1);

INSERT INTO course_managers (manages_id, managers_id) SELECT 1, id FROM concrete_person WHERE username = 'cseteacher1';
INSERT INTO course_managers (manages_id, managers_id) SELECT 2, id FROM concrete_person WHERE username = 'cseteacher2';

INSERT INTO cohort (name, program_id) VALUES ('CSE 2024/2025', 1);

INSERT INTO building (name) VALUES ('EWI');
INSERT INTO room (name, building_id) VALUES ('Ampère', 1);
INSERT INTO room (name, building_id) VALUES ('Boole', 1);
INSERT INTO room (name, building_id) VALUES ('Pi', 1);
