# Code of Conduct

Welcome to Queue! We are committed to fostering a positive and collaborative learning environment. To ensure a smooth and respectful experience for all users, we have established the following Code of Conduct for Queue in addition to the [TU Delft Code of Conduct](https://filelist.tudelft.nl/TUDelft/Over_TU_Delft/Strategie/Integriteitsbeleid/COC%20EN.pdf). By using our platform, you agree to adhere to these guidelines:

### **General Guidelines**

1. **Respectful Communication:**
    - Treat everyone with respect and kindness.
    - Use appropriate professional language and tone in all communications.
2. **Queue System Usage:**
    - Utilise the queue system for asking questions and seeking assistance from teaching assistants.
    - Wait your turn and avoid spamming the queue with multiple questions.
3. **Timely Responses:**
    - Teaching assistants are committed to responding to questions in a timely manner.
    - Be patient and wait for your question to be addressed.

### **Inappropriate Behaviour**

1. **Harassment and Discrimination:** Harassment or discrimination based on race, gender, sexual orientation, disability, or any other characteristic is strictly prohibited.
2. **Offensive Content:** Do not post or share any content that is offensive, discriminatory, or inappropriate.
3. **Unauthorised Access:** Do not attempt to access or tamper with any part of the platform that you do not have permission to use.

### **Consequences for Inappropriate Behaviour**

1. **Investigation:** Reports may eventually be forwarded to academic counsellors, who may contact the reporter at a later date.
2. **Consequences:** Violations of the Code of Conduct may result in warnings, temporary 
   suspension, or permanent removal from the platform. The TU Delft may enforce other 
   consequences at their own discretion. 

### Reporting Options/Process

1. **Reporting Incidents:** If you encounter inappropriate behaviour, report it immediately by contacting a teacher, academic counsellor or [confidential counsellor](https://www.tudelft.nl/en/about-tu-delft/strategy/integrity-policy/confidential-advisors/the-team).

### **Compliance with Laws**

1. **Legal Compliance:** Users must comply with all applicable laws and regulations while using the platform.
2. **Intellectual Property:** Respect intellectual property rights. Do not plagiarise or infringe on copyrights.

### **Summary**

Queue is intended to provide positive and collaborative learning environment, which should be safe and welcoming to all. Please make use of the platform accordingly. If at any point you feel this is being violated, please do not hesitate to report this.
