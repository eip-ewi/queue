<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" layout:decorate="~{lab/edit}">
    <head>
        <title>Edit Slotted Session</title>
    </head>

    <!--@thymesVar id="ec" type="nl.tudelft.labracore.api.dto.EditionCollectionDetailsDTO"-->
    <!--@thymesVar id="edition" type="nl.tudelft.labracore.api.dto.EditionDetailsDTO"-->

    <!--@thymesVar id="modules" type="java.util.List<nl.tudelft.labracore.api.dto.ModuleDetailsDTO>"-->
    <!--@thymesVar id="assignments" type="java.util.List<nl.tudelft.labracore.api.dto.AssignmentDetailsDTO>"-->
    <!--@thymesVar id="rooms" type="java.util.List<nl.tudelft.labracore.api.dto.RoomSummaryDTO>"-->

    <!--@thymesVar id="edition" type="nl.tudelft.labracore.api.dto.EditionDetailsDTO"-->
    <!--@thymesVar id="ec" type="nl.tudelft.labracore.api.dto.EditionCollectionDetailsDTO"-->

    <!--@thymesVar id="lSession" type="nl.tudelft.labracore.api.dto.SessionDetailsDTO"-->

    <!--@thymesVar id="lab" type="nl.tudelft.queue.model.labs.AbstractSlottedLab"-->
    <!--@thymesVar id="dto" type="nl.tudelft.queue.dto.patch.labs.AbstractSlottedLabPatchDTO"-->

    <body>
        <section layout:fragment="general-config">
            <th:block th:replace="~{lab/edit/components/lab-general :: lab-general}"></th:block>
        </section>

        <th:block layout:fragment="extra-config">
            <th:block th:replace="~{lab/edit/components/extra-info :: extra-info}"></th:block>
        </th:block>

        <section layout:fragment="advanced-config">
            <th:block th:replace="~{lab/edit/components/advanced-slotted :: advanced}"></th:block>
        </section>

        <th:block th:object="${dto}">
            <section layout:fragment="slot-config">
                <h3 class="font-500 mb-3">Time slots</h3>
                <p th:unless="${dto instanceof T(nl.tudelft.queue.dto.patch.labs.ExamLabPatchDTO)}" class="colour-info mb-3">
                    <span class="fa-info-circle fa-solid"></span>
                    To change the slots, go to 'Edit time slot capacity' on the session page.
                </p>
                <p th:if="${dto instanceof T(nl.tudelft.queue.dto.patch.labs.ExamLabPatchDTO)}" class="colour-warning mb-3">
                    <span class="fa-warning fa-solid"></span>
                    Exam slots cannot be changed after the session is created.
                </p>

                <div class="grid col-2 align-center gap-3" style="--col-1: minmax(0, 29rem)">
                    <label for="slot-open-input">Slot selection opens at:</label>
                    <div>
                        <input
                            th:classappend="${#fields.hasErrors('slottedLabConfig.selectionOpensAt')} ? 'is-invalid'"
                            name="slottedLabConfig.selectionOpensAt"
                            th:value="${#temporals.format(lab.slottedLabConfig.selectionOpensAt, 'yyyy-MM-dd''T''HH:mm')}"
                            type="datetime-local"
                            class="textfield"
                            id="slot-open-input"
                            required />
                        <div
                            class="colour-error"
                            th:if="${#fields.hasErrors('slottedLabConfig.selectionOpensAt')}"
                            th:errors="*{slottedLabConfig.selectionOpensAt}">
                            Slot opensAt error
                        </div>
                    </div>

                    <div style="grid-column: span 2">
                        <input
                            id="slot-disable-late-enrollment-input"
                            type="checkbox"
                            name="slottedLabConfig.disableLateEnrollment"
                            th:checked="${lab.slottedLabConfig.disableLateEnrollment}" />
                        <label for="slot-disable-late-enrollment-input">Disallow enrollment in ongoing slots</label>
                    </div>

                    <div style="grid-column: span 2">
                        <input
                            id="consecutive-filling-checkbox"
                            type="checkbox"
                            class="form-check-input"
                            th:checked="${lab.isConsecutive()}"
                            onchange="updateConsecutiveFillingVisibility()" />
                        <label for="consecutive-filling-checkbox">Consecutive Filling</label>
                    </div>

                    <label id="slot-full-percentage-label" for="slot-full-percentage" th:hidden="not ${lab.isConsecutive()}">
                        The percentage of a slot that has to be filled prior to opening subsequent slots:
                    </label>
                    <div id="slot-full-percentage-div" th:hidden="not ${lab.isConsecutive()}">
                        <input
                            id="slot-full-percentage"
                            type="number"
                            name="slottedLabConfig.consideredFullPercentage"
                            th:value="${lab.slottedLabConfig.consideredFullPercentage}"
                            class="textfield"
                            min="1"
                            max="100"
                            placeholder="50"
                            th:disabled="${!lab.isConsecutive()}"
                            th:required="${lab.isConsecutive()}" />
                    </div>

                    <label id="empty-allowed-threshold-label" for="empty-allowed-threshold" th:hidden="not ${lab.isConsecutive()}">
                        The number of previous slots that are permitted to be below the percentage threshold:
                    </label>
                    <div id="empty-allowed-threshold-div" th:hidden="not ${lab.isConsecutive()}">
                        <input
                            id="empty-allowed-threshold"
                            type="number"
                            name="slottedLabConfig.previousEmptyAllowedThreshold"
                            th:value="${lab.slottedLabConfig.previousEmptyAllowedThreshold}"
                            class="textfield"
                            min="0"
                            placeholder="0"
                            th:disabled="${!lab.isConsecutive()}"
                            th:required="${lab.isConsecutive()}" />
                    </div>
                </div>

                <div class="mt-5 underlined"></div>

                <script th:inline="javascript">
                    function updateConsecutiveFillingVisibility() {
                        const checkbox = document.getElementById("consecutive-filling-checkbox");
                        const formElements = ["slot-full-percentage", "empty-allowed-threshold"].map(id => document.getElementById(id));
                        const toHide = [
                            "slot-full-percentage-label",
                            "empty-allowed-threshold-label",
                            "slot-full-percentage-div",
                            "empty-allowed-threshold-div",
                        ].map(id => document.getElementById(id));

                        if (checkbox.checked) {
                            formElements.forEach(elem => {
                                elem.setAttribute("required", "");
                                elem.removeAttribute("disabled");
                            });
                            toHide.forEach(elem => {
                                elem.removeAttribute("hidden");
                            });
                        } else {
                            formElements.forEach(elem => {
                                elem.removeAttribute("required");
                                elem.setAttribute("disabled", "");
                            });
                            toHide.forEach(elem => {
                                elem.setAttribute("hidden", "");
                            });
                        }
                    }
                </script>
            </section>
        </th:block>
    </body>
</html>
