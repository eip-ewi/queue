<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<html lang="en" xmlns:th="http://www.thymeleaf.org">
    <!--@thymesVar id="#authenticatedP" type="nl.tudelft.labracore.lib.security.user.Person"-->

    <!--@thymesVar id="qSession" type="nl.tudelft.queue.dto.view.LabViewDTO"-->

    <!--@thymesVar id="rooms" type="java.util.List<nl.tudelft.labracore.api.dto.RoomDetailsDTO>"-->
    <!--@thymesVar id="assignments" type="java.util.Map<nl.tudelft.labracore.api.dto
.AssignmentSummaryDTO, java.lang.String>"-->
    <!--@thymesVar id="modules" type="java.util.List<nl.tudelft.labracore.api.dto.ModuleDetailsDTO>"-->

    <!--@thymesVar id="types" type="java.util.Map<java.lang.Long, java.util.Set<java.lang.String>>"-->

    <body>
        <th:block th:fragment="lab-info">
            <div class="surface p-0">
                <h3 class="surface__header">Lab info</h3>

                <div class="surface__content">
                    <dl>
                        <dt class="fw-500">Slot</dt>
                        <dd
                            th:text="|${#temporals.format(qSession.session.start, 'dd MMMM yyyy HH:mm')} - ${#temporals.format(qSession.session.endTime, 'dd MMMM yyyy HH:mm')}|"></dd>

                        <dt class="fw-500 mt-3">Rooms</dt>
                        <dd th:if="${#lists.isEmpty(rooms)}">This lab does not have any rooms</dd>
                        <dd th:unless="${#lists.isEmpty(rooms)}" th:text="${#strings.listJoin(@roomDTOService.names(rooms), ', ')}"></dd>

                        <dt class="fw-500 mt-3">Online Modes</dt>
                        <dd th:if="${#sets.isEmpty(qSession.onlineModes)}">This lab does not have an online alternative</dd>
                        <dd
                            th:unless="${#sets.isEmpty(qSession.onlineModes)}"
                            th:text="${T(nl.tudelft.queue.model.enums.OnlineMode).displayNames(qSession.onlineModes)}"></dd>

                        <dt class="fw-500 mt-3">Modules</dt>
                        <dd th:if="${#lists.isEmpty(modules)}">This lab does not have any modules configured</dd>
                        <dd th:unless="${#lists.isEmpty(modules)}">
                            <ul class="list">
                                <li th:if="${qSession.session.editions.size() == 1}" th:each="m : ${modules}" th:text="${m.name}"></li>
                                <li
                                    th:if="${qSession.session.editions.size() > 1}"
                                    th:each="m : ${modules}"
                                    th:text="|${@editionCacheManager.getRequired(m.edition.id).course.name} - ${m.name}|"></li>
                            </ul>
                        </dd>

                        <dt class="fw-500 mt-3">Allowed Requests</dt>
                        <dd th:if="${qSession.allowedRequests.isEmpty()}" class="colour-error">
                            This lab does not allow any requests at the moment, please contact the course staff about this.
                        </dd>
                        <dd th:unless="${qSession.allowedRequests.isEmpty()}">
                            <ul class="list">
                                <li th:each="entry : ${assignments}" th:text="|${entry.value} (${#strings.setJoin(types.get(entry.key), '/')})|"></li>
                            </ul>
                        </dd>

                        <th:block th:if="${!#lists.isEmpty(qSession.constraints.toList())}">
                            <dt class="fw-500 mt-3">Constraints</dt>
                            <dd>
                                <ul class="list">
                                    <li
                                        th:each="constraint : ${qSession.constraints.toList()}"
                                        th:text="|${@constraintService.constraintDescription(constraint)} ${@labRequestConstraintService.canCreateRequest(constraint, #authenticatedP.id) ? '' : '(you do not satisfy this)'}|"
                                        th:classappend="${!@labRequestConstraintService.canCreateRequest(constraint, #authenticatedP.id)} ? 'text-danger'"></li>
                                </ul>
                            </dd>
                        </th:block>

                        <th:block th:if="${qSession.isBilingual}">
                            <dt class="fw-500 mt-3">Languages</dt>
                            <dd>Requests can be done in English or Dutch.</dd>
                        </th:block>
                    </dl>
                </div>
            </div>

            <div class="surface p-0">
                <h3 class="surface__header">Lab status</h3>

                <div class="surface__content">
                    <dt class="fw-500">Session</dt>
                    <dd>
                        <span
                            th:if="${qSession.isBeforeSession()}"
                            th:text="|Session will start ${#temporals.format(qSession.session.start, 'dd-MM-yyyy HH:mm')}|"></span>
                        <span th:if="${qSession.isActive()}">
                            This is an
                            <strong class="fw-500">active</strong>
                            session
                        </span>
                        <span
                            th:if="${qSession.isAfterSession()}"
                            th:text="|Session ended ${#temporals.format(qSession.session.endTime, 'dd-MM-yyyy HH:mm')}|"></span>
                    </dd>

                    <dt class="fw-500 mt-3">Enqueuing Status</dt>
                    <dd>
                        <span class="chip" data-type="accept" th:if="${qSession.isEnqueueingOpen()}">Open</span>
                        <span class="chip" data-type="error" th:unless="${qSession.isEnqueueingOpen()}" th:text="${qSession.closedReason()}">
                            Closed
                        </span>
                    </dd>

                    <dt class="fw-500 mt-3">Current size of queue</dt>
                    <dd>
                        <span th:text="|${qSession.data.getQueue().size()} student(s)|"></span>
                        <div class="tooltip">
                            <button class="tooltip__control fa-solid fa-question"></button>
                            <p role="tooltip">Number of students currently in the queue for this lab</p>
                        </div>
                    </dd>

                    <dt class="fw-500 mt-3">Waiting time</dt>
                    <dd th:with="avg = ${@labService.averageWaitingTime(qSession.data)}, cur = ${@labService.currentWaitingTime(qSession.data)}">
                        <th:block th:if="${avg.isPresent() || cur.isPresent()}">
                            <th:block th:if="${avg.isPresent()}">
                                <span th:text="|${T(java.lang.Math).round(avg.getAsDouble() / 60)} minutes (average)|"></span>
                                <div class="tooltip">
                                    <button class="tooltip__control fa-solid fa-question"></button>
                                    <p role="tooltip">Computed as the average waiting time of archived requests in the past hour.</p>
                                </div>
                            </th:block>
                            <span th:unless="${avg.isPresent()}">Unknown (average)</span>
                            <span>/</span>
                            <th:block th:if="${cur.isPresent()}">
                                <span th:text="|${T(java.lang.Math).round(cur.getAsDouble() / 60)} minutes (current)|"></span>
                                <div class="tooltip">
                                    <button class="tooltip__control fa-solid fa-question"></button>
                                    <p role="tooltip" style="width: 20rem; white-space: initial">
                                        Computed as the waiting time of the oldest pending request. If there are no pending requests, the waiting time
                                        of the last handled request is used.
                                    </p>
                                </div>
                            </th:block>
                            <span th:unless="${cur.isPresent()}">Unknown (current)</span>
                        </th:block>
                        <th:block th:unless="${avg.isPresent() || cur.isPresent()}">Unknown</th:block>
                    </dd>
                </div>
            </div>
        </th:block>
    </body>
</html>
