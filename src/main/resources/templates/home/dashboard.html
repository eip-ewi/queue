<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" layout:decorate="~{container}">
    <head>
        <script src="/webjars/fullcalendar/index.global.js"></script>
    </head>

    <!--@thymesVar id="user" type="nl.tudelft.labracore.api.dto.PersonDetailsDTO"-->
    <!--@thymesVar id="editions" type="java.util.Map<java.lang.Long, nl.tudelft.labracore.api.dto.EditionDetailsDTO>"-->
    <!--@thymesVar id="runningEditions" type="java.util.List<nl.tudelft.labracore.api.dto.EditionSummaryDTO>"-->

    <!--@thymesVar id="activeRoles" type="java.util.List<nl.tudelft.labracore.api.dto.RoleEditionDetailsDTO>"-->
    <!--@thymesVar id="finishedRoles" type="java.util.List<nl.tudelft.labracore.api.dto.RoleEditionDetailsDTO>"-->
    <!--@thymesVar id="archivedRoles" type="java.util.List<nl.tudelft.labracore.api.dto.RoleEditionDetailsDTO>"-->

    <body>
        <main layout:fragment="content">
            <h1 class="font-800 mb-3">My Courses</h1>

            <th:block th:if="${@permissionService.isAdminOrTeacher()}">
                <div class="flex gap-3 mb-5">
                    <a
                        th:href="@{/edition/add}"
                        class="button"
                        th:classappend="${!@permissionService.canCreateEdition()} ? 'disabled'"
                        th:disabled="${!@permissionService.canCreateEdition()}">
                        Create new edition
                    </a>
                    <div class="flex gap-1 align-center">
                        <button class="button" data-style="outlined" data-dialog="create-shared-edition-dialog">Create shared edition</button>
                        <div class="tooltip">
                            <div class="tooltip__control fa-solid fa-question"></div>
                            <p role="tooltip" style="min-width: 100ch; white-space: initial">
                                A shared edition is a collection of courses that acts in the queue as one course. This can be used for sessions in
                                which multiple courses take place at the same time. This avoids having to create a session for every course, as
                                sessions can be shared.
                            </p>
                        </div>
                    </div>
                </div>
                <div th:replace="~{shared-edition/create/create-shared-edition :: overlay}"></div>
            </th:block>

            <div class="tabs mb-5" id="overview-tabs" role="tablist">
                <button id="overview-tab" role="tab" aria-controls="overview" aria-selected="true">Course overview</button>
                <button id="calendar-tab" role="tab" aria-controls="calendar" aria-selected="false">Calendar</button>
            </div>

            <div id="calendar" role="tabpanel" aria-labelledby="calendar-tab">
                <div id="calendar-view"></div>
                <script th:inline="javascript">
                    document.addEventListener('DOMContentLoaded', function () {
                        let calendarEl = document.getElementById('calendar-view');
                        let calendar = new FullCalendar.Calendar(calendarEl, {
                            // plugins: [listPlugin],
                            headerToolbar: {
                                left: 'timeGridWeek,timeGridDay,listDay',
                                center: 'title',
                                right: 'today prev,next'
                            },
                            initialView: 'timeGridWeek',
                            nowIndicator: true,
                            businessHours: {
                                daysOfWeek: [1, 2, 3, 4, 5],
                                startTime: '08:45',
                                endTime: '17:45'
                            },
                            eventDisplay: 'block',
                        });
                        let sessions = /*[[${sessions}]]*/;
                        sessions.forEach(dto => {
                            const event = {
                                title: dto.session.name,
                                start: dto.session.start,
                                end: dto.session.end,
                                description: dto.session.description,
                                url: "/lab/" + dto.id
                            }
                            switch (dto.type) {
                                case "SLOTTED":
                                    event['backgroundColor'] = "yellow"
                                    event['textColor'] = "black"
                                    calendar.addEvent({
                                        title: dto.session.name + " slot selection",
                                        start: dto.selectionOpensAt,
                                        backgroundColor: "yellow",
                                        textColor: "black"
                                    })
                                    break;
                                case "EXAM":
                                    event['backgroundColor'] = "red"
                                    calendar.addEvent({
                                        title: dto.session.name + " slot selection",
                                        start: dto.selectionOpensAt,
                                        backgroundColor: "red",
                                    })
                                    break;
                            }
                            calendar.addEvent(event)
                        })
                        calendar.render();
                        $(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
                            calendar.updateSize();
                        });

                        document.getElementById("calendar").setAttribute("hidden", true);
                    });
                </script>
            </div>

            <div
                id="overview"
                role="tabpanel"
                aria-labelledby="overview-tab"
                th:with="visibleActive = ${activeRoles.?[#this.type.name() == 'TEACHER' or not (#root.editions.get(#this.edition.id).hidden)]},
                          visibleUpcoming = ${upcomingRoles.?[#this.type.name() == 'TEACHER' or not (#root.editions.get(#this.edition.id).hidden)]},
                          visibleFinished = ${finishedRoles.?[not (#root.editions.get(#this.edition.id).hidden)]},
                          visibleArchived = ${archivedRoles.?[not (#root.editions.get(#this.edition.id).hidden)]}">
                <div class="flex vertical">
                    <div class="surface p-0">
                        <h3 class="surface__header">Active courses you participate in</h3>

                        <div class="surface__content" th:if="${#lists.isEmpty(visibleActive) && #sets.isEmpty(activeSharedEditions)}">
                            <span th:if="${user.defaultRole.name() == 'STUDENT'}">
                                You do not participate in any courses. Why don't you
                                <a class="link" th:href="@{/editions}">enrol for your first course</a>
                                ?
                            </span>
                            <span th:if="${user.defaultRole.name() == 'ADMIN' or user.defaultRole.name() == 'TEACHER'}">
                                You do not have any active courses. You can create a course edition by clicking 'Create new edition' above.
                            </span>
                        </div>

                        <ul
                            class="surface__content list flex vertical gap-3"
                            role="list"
                            th:unless="${#lists.isEmpty(visibleActive) && #sets.isEmpty(activeSharedEditions)}">
                            <li
                                th:each="role : ${visibleActive}"
                                th:with="edition = ${editions.get(role.edition.id)}"
                                th:if="${role.type.name() == 'TEACHER'} or not ${editions[role.edition.id].hidden}">
                                <div class="mb-1">
                                    <a
                                        class="link"
                                        th:href="@{/edition/{id}(id=${edition.id})}"
                                        th:text="|${edition.course.name} (${edition.name})|"></a>
                                    <span th:text="${'(' + @roleDTOService.typeDisplayName(role.type.toString()) + ')'}"></span>
                                    <span class="chip" th:if="${edition.hidden}">Hidden</span>
                                </div>

                                <ul class="list ml-5" role="list" th:unless="${#lists.isEmpty(labs)}">
                                    <th:block th:each="sess : ${edition.sessions}">
                                        <li th:each="lab : ${labs[sess.id]}" th:unless="${lab.isShared}" class="flex align-center gap-3">
                                            <a
                                                class="link"
                                                th:href="@{/lab/{id}(id=${lab.id})}"
                                                th:text="|${lab.name} ${#temporals.format(lab.slot.opensAt, 'dd MMMM yyyy HH:mm')} - ${#temporals.format(lab.slot.closesAt, 'dd MMMM yyyy HH:mm')} ${lab.slotOccupationString}|"></a>
                                            <div class="flex wrap gap-2 p-1">
                                                <span th:if="${lab.isShared}" class="chip">Shared lab</span>
                                                <span class="chip" th:if="${lab.slot.open()}">Active</span>
                                                <span class="chip" th:if="${lab.slot.closed()}">Completed</span>

                                                <span class="chip" th:if="${lab.type == T(nl.tudelft.queue.model.enums.QueueSessionType).EXAM}">
                                                    Exam
                                                </span>
                                                <span class="chip" th:if="${lab.type == T(nl.tudelft.queue.model.enums.QueueSessionType).SLOTTED}">
                                                    Slotted
                                                </span>
                                                <span class="chip" th:if="${lab.type == T(nl.tudelft.queue.model.enums.QueueSessionType).CAPACITY}">
                                                    Limited Capacity
                                                </span>

                                                <span
                                                    class="chip"
                                                    data-type="warning"
                                                    th:if="${!lab.slot.closed() && lab.status.isOpenToEnqueueing()}">
                                                    Open for enqueueing
                                                </span>
                                            </div>
                                        </li>
                                    </th:block>
                                </ul>
                            </li>

                            <li th:each="shared : ${activeSharedEditions}">
                                <div>
                                    <a class="link" th:href="@{/shared-edition/{id}(id=${shared.id})}" th:text="|${shared.getName()}|"></a>
                                    <span class="chip">Shared edition</span>
                                </div>

                                <ul class="list ml-5" role="list" th:unless="${#lists.isEmpty(labs)}">
                                    <li th:each="lab : ${sharedLabs[shared.id]}" class="flex align-center gap-3">
                                        <a
                                            href="#"
                                            class="link"
                                            th:href="@{/lab/{id}(id=${lab.id})}"
                                            th:text="|${lab.name} ${#temporals.format(lab.slot.opensAt, 'dd MMMM yyyy HH:mm')} - ${#temporals.format(lab.slot.closesAt, 'dd MMMM yyyy HH:mm')} ${lab.slotOccupationString}|"></a>

                                        <div>
                                            <span th:if="${lab.isShared}" class="chip">Shared lab</span>
                                            <span class="chip" th:if="${lab.slot.open()}">Active</span>
                                            <span class="chip" th:if="${lab.slot.closed()}">Completed</span>

                                            <span class="chip" th:if="${lab.type == T(nl.tudelft.queue.model.enums.QueueSessionType).EXAM}">
                                                Exam
                                            </span>
                                            <span class="chip" th:if="${lab.type == T(nl.tudelft.queue.model.enums.QueueSessionType).SLOTTED}">
                                                Slotted
                                            </span>
                                            <span class="chip" th:if="${lab.type == T(nl.tudelft.queue.model.enums.QueueSessionType).CAPACITY}">
                                                Limited Capacity
                                            </span>

                                            <span class="chip" data-type="warning" th:if="${!lab.slot.closed() && lab.status.isOpenToEnqueueing()}">
                                                Open for enqueueing
                                            </span>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>

                    <div class="surface p-0" th:if="${!#lists.isEmpty(visibleUpcoming) || !#lists.isEmpty(upcomingSharedEditions)}">
                        <h3 class="surface__header">Upcoming courses</h3>

                        <ul class="surface__content list flex vertical gap-3" role="list">
                            <li
                                class="flex gap-3 align-center"
                                th:each="role : ${visibleUpcoming}"
                                th:with="edition = ${editions.get(role.edition.id)}">
                                <div>
                                    <a
                                        class="link"
                                        th:href="@{/edition/{id}(id=${edition.id})}"
                                        th:text="|${edition.course.name} (${edition.name})|"></a>
                                    <span th:text="${'(' + @roleDTOService.typeDisplayName(role.type.toString()) + ')'}"></span>
                                </div>
                                <div>
                                    <span class="chip" th:if="${edition.hidden}">Hidden</span>
                                </div>
                            </li>

                            <li th:each="shared : ${upcomingSharedEditions}">
                                <div>
                                    <a class="link" th:href="@{/shared-edition/{id}(id=${shared.id})}" th:text="|${shared.getName()}|"></a>
                                    <span class="chip">Shared edition</span>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="surface p-0" th:if="${!#lists.isEmpty(visibleFinished) || !#lists.isEmpty(finishedSharedEditions)}">
                        <h3 class="surface__header">Finished courses</h3>

                        <ul class="surface__content list flex vertical gap-3" role="list">
                            <li th:each="role : ${visibleFinished}" th:with="edition = ${editions.get(role.edition.id)}">
                                <a class="link" th:href="@{/edition/{id}(id=${edition.id})}" th:text="|${edition.course.name} (${edition.name})|"></a>
                                <span th:text="${'(' + @roleDTOService.typeDisplayName(role.type.toString()) + ')'}"></span>
                            </li>

                            <li th:each="shared : ${finishedSharedEditions}">
                                <div>
                                    <a class="link" th:href="@{/shared-edition/{id}(id=${shared.id})}" th:text="|${shared.getName()}|"></a>
                                    <span class="chip">Shared edition</span>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="surface p-0" th:if="${!#lists.isEmpty(visibleArchived) || !#lists.isEmpty(archivedSharedEditions)}">
                        <h3 class="surface__header">Archived courses</h3>

                        <ul class="surface__content list flex vertical gap-3" role="list">
                            <th:block th:each="role : ${visibleArchived}" th:with="edition = ${editions.get(role.edition.id)}">
                                <li>
                                    <a
                                        class="link"
                                        th:href="@{/edition/{id}(id=${edition.id})}"
                                        th:text="|${edition.course.name} (${edition.name})|"></a>
                                    <span th:text="${'(' + @roleDTOService.typeDisplayName(role.type.toString()) + ')'}"></span>
                                </li>
                            </th:block>

                            <li th:each="shared : ${archivedSharedEditions}">
                                <div>
                                    <a class="link" th:href="@{/shared-edition/{id}(id=${shared.id})}" th:text="|${shared.getName()}|"></a>
                                    <span class="chip">Shared edition</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>
