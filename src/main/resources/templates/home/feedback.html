<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" layout:decorate="~{container}">
    <!--@thymesVar id="#authenticatedP" type="nl.tudelft.labracore.lib.security.user.Person"-->

    <!--@thymesVar id="assistant" type="nl.tudelft.labracore.api.dto.PersonDetailsDTO"-->
    <!--@thymesVar id="feedback" type="org.springframework.data.domain.Page<nl.tudelft.queue.dto.view.FeedbackViewDTO>"-->
    <!--@thymesVar id="stars" type="java.util.List<java.lang.Integer>"-->
    <!--@thymesVar id="assistantRating" type="java.util.List<nl.tudelft.queue.dto.view.statistics.AssistantRatingViewDto>"-->

    <head>
        <title th:text="|Feedback for ${assistant.displayName}|"></title>

        <script src="/webjars/chartjs/dist/chart.umd.js"></script>
        <script src="/js/chart_utils.js"></script>
    </head>

    <body>
        <main layout:fragment="content">
            <h1 class="font-800 mb-5" th:text="|Feedback for ${assistant.displayName}|"></h1>

            <div class="flex vertical space-between">
                <section th:if="${#authenticatedP.id != assistant.id}" id="feedback-statistics-section">
                    <div class="flex wrap align-center align-stretch content-wrapper mb-3 gap-2">
                        <div class="surface content-wrapper">
                            <div class="surface__content flex vertical align-center gap-3">
                                <h2 class="font-400">Number of requests processed</h2>
                                <p class="font-300" th:text="${assistantRating.numberOfRequests}"></p>
                            </div>
                        </div>

                        <div class="surface content-wrapper">
                            <div class="surface__content flex vertical align-center gap-3">
                                <h2 class="font-400">Average Rating</h2>
                                <div style="display: inline-flex; justify-content: space-between">
                                    <div class="flex gap-0 font-500" style="color: goldenrod">
                                        <span
                                            class="fa-solid fa-star"
                                            th:each="_ : ${#numbers.sequence(1, assistantRating.getNumFilledStars(), 1)}"></span>

                                        <span th:if="${assistantRating.hasHalfFilledStar()}" class="fa-solid fa-star-half-stroke"></span>

                                        <span
                                            class="fa-regular fa-star"
                                            th:each="_ : ${#numbers.sequence(1, assistantRating.getNumEmptyStars(), 1)}"></span>
                                    </div>
                                    <p class="ml-3" th:text="${assistantRating.avgRating}"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="feedback-stat-chart-section">
                    <div class="surface p-0">
                        <div class="surface__header flex align-center">
                            <h2 class="font-500">Rating breakdown</h2>
                            <div class="tooltip">
                                <button class="tooltip__control fa-question"></button>
                                <p role="tooltip" style="width: 30rem; white-space: initial">
                                    Ratings range from 1 (needs improvement) to 5 (excellent) for request handling performance.
                                </p>
                            </div>
                        </div>
                        <div class="surface__content">
                            <canvas id="feedback-histogram"></canvas>
                        </div>

                        <script th:inline="javascript" type="text/javascript">
                            //<![CDATA[
                            const stars = /*[[${stars}]]*/ [0, 0, 0, 0, 0];

                            const ctx = document.getElementById("feedback-histogram").getContext("2d");
                            new Chart(ctx, {
                                type: "bar",
                                data: {
                                    labels: ["1", "2", "3", "4", "5"],
                                    datasets: [
                                        {
                                            label: "# of Votes",
                                            data: stars,
                                            backgroundColor: [
                                                "rgba(255, 99, 132, 0.2)",
                                                "rgba(54, 162, 235, 0.2)",
                                                "rgba(255, 206, 86, 0.2)",
                                                "rgba(75, 192, 192, 0.2)",
                                                "rgba(153, 102, 255, 0.2)",
                                                "rgba(255, 159, 64, 0.2)",
                                            ],
                                            borderColor: [
                                                "rgba(255, 99, 132, 1)",
                                                "rgba(54, 162, 235, 1)",
                                                "rgba(255, 206, 86, 1)",
                                                "rgba(75, 192, 192, 1)",
                                                "rgba(153, 102, 255, 1)",
                                                "rgba(255, 159, 64, 1)",
                                            ],
                                            borderWidth: 1,
                                        },
                                    ],
                                },
                                options: {
                                    scales: {
                                        y: {
                                            ticks: {
                                                beginAtZero: true,
                                                precision: 0,
                                            },
                                        },
                                    },
                                },
                            });
                            //]]>
                        </script>
                    </div>
                </section>

                <section id="commentary-feedback-section">
                    <div class="flex vertical">
                        <div class="surface p-0">
                            <h2 class="surface__header font-500">Written feedback</h2>
                            <div class="surface__content">
                                <div class="banner" data-type="info" role="banner" th:if="${feedback.isEmpty()}">
                                    <span class="banner__icon fa-solid fa-info-circle"></span>
                                    <p th:if="${#authenticatedP.id == assistant.id}">You have no written feedback.</p>
                                    <p th:if="${#authenticatedP.id != assistant.id}">This person has no written feedback.</p>
                                </div>

                                <ul class="surface divided list pil-5 pbl-3" role="list" th:unless="${feedback.isEmpty()}">
                                    <li
                                        class="flex vertical gap-3 pbl-3"
                                        th:each="fb : ${feedback}"
                                        th:if="${fb.feedback != null && !fb.feedback.isEmpty()}">
                                        <div class="flex space-between">
                                            <div class="flex">
                                                <div
                                                    th:if="${fb.isReported && @permissionService.canViewDeanonimisedFeedback()}"
                                                    class="tooltip"
                                                    style="justify-content: space-evenly; margin: auto">
                                                    <i class="tooltip__control" style="min-height: 1.1rem">!</i>
                                                    <p role="tooltip" style="width: 30rem; white-space: initial">
                                                        This feedback item has been reported and will not be shown anymore to the TA.
                                                    </p>
                                                </div>
                                                <p
                                                    th:text="${fb.feedback}"
                                                    style="justify-content: space-around; margin: auto; vertical-align: middle"></p>
                                            </div>
                                            <span class="chip" data-type="error" th:unless="${fb.deletedAt == null}">Deleted Feedback</span>
                                            <div class="menu-wrapper" th:if="${fb.deletedAt == null}">
                                                <button
                                                    aria-haspopup="menu"
                                                    class="fa-solid fa-ellipsis-vertical button"
                                                    th:aria-controls="|feedback-${fb.id}|"></button>
                                                <ul class="menu" th:id="|feedback-${fb.id}|" role="menu">
                                                    <li>
                                                        <form method="post" th:action="@{/feedback/report}">
                                                            <input hidden th:value="${fb.id.requestId}" name="request" />
                                                            <input hidden th:value="${fb.id.assistantId}" name="assistant" />
                                                            <input hidden th:value="${#uri}" name="callback" />
                                                            <button role="menuitem">Report as inappropriate</button>
                                                        </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>

                                        <p th:if="${fb.rating != null}" class="flex gap-1" style="color: goldenrod">
                                            <span class="fa-solid fa-star" th:each="_ : ${#numbers.sequence(1, fb.rating, 1)}"></span>
                                            <span class="fa-regular fa-star" th:each="_ : ${#numbers.sequence(1, 5 - fb.rating, 1)}"></span>
                                        </p>

                                        <p class="font-200" th:if="${@permissionService.canViewDeanonimisedFeedback()}">
                                            <span>Feedback</span>
                                            <span th:if="${fb.rating != null}" th:text="|of ${fb.rating + '/5'} |"></span>
                                            <th:block th:if="${fb.requestExists}">
                                                <span th:text="|by ${fb.groupName} on Request|"></span>
                                                <a
                                                    class="link"
                                                    th:text="${'#' + fb.id.requestId}"
                                                    th:href="@{/request/{id}(id = ${fb.id.requestId})}"></a>
                                            </th:block>
                                            <th:block th:unless="${fb.requestExists}">
                                                <span>by an unknown requester on a deleted request.</span>
                                            </th:block>
                                            <br />
                                            <span
                                                th:text="|Last updated on ${#temporals.format(fb.lastUpdatedAt, 'dd-MM-yyyy')}, created ${#temporals.format(fb.createdAt, 'dd-MM-yyyy')}|"></span>
                                            <span
                                                th:if="${fb.deletedAt != null}"
                                                th:text="|Deleted on ${#temporals.format(fb.deletedAt, 'dd-MM-yyyy')}|"></span>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <th:block th:replace="~{pagination :: pagination (page=${feedback}, size=3)}"></th:block>
                    </div>
                </section>
            </div>
            <th:block th:if="${param.reportSuccess != null}">
                <th:block th:replace="~{simple-dialog.html :: dialog (text='We have succesfully received your report')}"></th:block>
            </th:block>
        </main>
    </body>
</html>
