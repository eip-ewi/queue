<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<html lang="en" xmlns:th="http://www.thymeleaf.org">
    <head>
        <style th:fragment="style">
            @media (width > 48em) {
                .feedback-tab {
                    max-width: 30cqw;
                }
            }

            .feedback label {
                cursor: pointer;
            }
            .feedback :where(.feedback__control > .fa-regular) {
                display: none;
            }
            .feedback :where(input:checked ~ .feedback__control > .fa-regular) {
                display: initial;
            }
            .feedback :where(input:not(:checked) ~ .feedback__control > .fa-solid) {
                display: initial;
            }
            .feedback :where(input:checked ~ .feedback__control > .fa-solid) {
                display: none;
            }

            .feedback :where(:has(input:invalid) .feedback__control > .fa-regular) {
                display: initial;
            }
            .feedback :where(:has(input:invalid) .feedback__control > .fa-solid) {
                display: none;
            }

            .feedback__control {
                min-width: 36px;
            }

            .feedback:hover :where(.feedback__control > *) {
                display: none;
            }
            .feedback:hover :where(.feedback__control:hover ~ .feedback__control > .fa-regular) {
                display: initial;
            }
            .feedback:hover :where(.feedback__control > .fa-solid) {
                display: initial;
            }
            .feedback:hover :where(.feedback__control:hover ~ .feedback__control > .fa-solid) {
                display: none;
            }
        </style>
    </head>

    <body>
        <th:block th:fragment="feedback" th:with="assistants = ${@feedbackService.assistantsInvolvedInRequest(request.id)}">
            <div class="flex vertical gap-3" th:if="${@permissionService.canGiveFeedback(request.id) && !#lists.isEmpty(assistants)}">
                <h3 class="font-500">Feedback</h3>

                <div>
                    <label for="select-assistant">Select TA:</label>
                    <select id="select-assistant" class="textfield">
                        <option disabled selected>Select an assistant</option>
                        <option th:each="assistant, i : ${assistants}" th:value="${assistant.id}" th:text="${assistant.displayName}"></option>
                    </select>
                </div>

                <div th:each="assistant, i : ${assistants}" th:id="|feedback-${assistant.id}|" class="feedback-tab hidden">
                    <form
                        th:action="@{/request/{id}/feedback/{aId}(id=${request.id}, aId=${assistant.id})}"
                        th:with="feedback = ${@feedbackRepository.findById(request.id, assistant.id)}"
                        method="post"
                        class="flex vertical">
                        <div>
                            <div
                                class="feedback flex gap-0 font-700"
                                style="color: goldenrod"
                                th:with="checked = ${feedback.isPresent() ? feedback.get().rating : 0}">
                                <th:block th:each="i : ${#numbers.sequence(1, 5)}">
                                    <div class="feedback__control">
                                        <label th:for="|star-${assistant.id}-${i}|" class="fa-regular fa-star"></label>
                                        <label th:for="|star-${assistant.id}-${i}|" class="fa-solid fa-star"></label>
                                    </div>
                                    <input
                                        th:id="|star-${assistant.id}-${i}|"
                                        type="radio"
                                        th:name="rating"
                                        th:value="${i}"
                                        autocomplete="off"
                                        required
                                        hidden
                                        th:checked="${checked == i}" />
                                </th:block>
                            </div>
                        </div>
                        <textarea
                            th:id="|input-feedback-${assistant.id}|"
                            aria-label="Feedback"
                            maxlength="250"
                            class="textfield"
                            name="feedback"
                            th:required="${feedback.isPresent() && feedback.get().rating == 1 ? '' : null}"
                            th:minlength="${feedback.isPresent() && feedback.get().rating == 1 ? 10 : null}"
                            th:text="${feedback.isPresent() ? feedback.get().feedback : ''}"
                            th:placeholder="|Leave feedback about ${assistant.displayName}.|"></textarea>
                        <div>
                            <p class="font-200 mb-2" th:hidden="${feedback.isEmpty() || feedback.get().rating > 1}" data-one-star-only>
                                Filling in some textual feedback is required for one-star reviews.
                            </p>
                            <p class="font-200">
                                Your feedback is
                                <strong class="fw-500">anonymous</strong>
                                : The TA will only be able to see the feedback and score you give, not your name. Be respectful and keep your feedback
                                constructive.
                            </p>
                        </div>
                        <div>
                            <button type="submit" class="button">Save feedback</button>
                        </div>
                    </form>

                    <script>
                        $("#select-assistant").on("change", function () {
                            $(`.feedback-tab`).addClass("hidden");
                            $(`#feedback-${$(this).val()}`).removeClass("hidden");
                        });

                        document.addEventListener("DOMContentLoaded", function () {
                            document.querySelectorAll("input[type='radio']").forEach(star =>
                                star.addEventListener("change", function () {
                                    if (!star.checked) {
                                        return;
                                    }
                                    const oneStar = star.value === "1";
                                    const textField = star.closest("form").querySelector("[name='feedback']");
                                    const paragraph = star.closest("form").querySelector("[data-one-star-only]");
                                    if (oneStar) {
                                        textField.setAttribute("required", "");
                                        textField.setAttribute("minlength", "10");
                                        paragraph.removeAttribute("hidden");
                                    } else {
                                        textField.removeAttribute("required");
                                        textField.removeAttribute("minlength");
                                        paragraph.setAttribute("hidden", "");
                                    }
                                })
                            );

                            document.querySelectorAll("textarea.textfield").forEach(textarea => {
                                textarea.addEventListener("input", function () {
                                    const countNewlines = (this.value.match(/\n/g) || []).length;

                                    if (this.value.length >= textarea.getAttribute("maxlength")) {
                                        // Remove the amount of newlines, due to the fact that
                                        // they are counted as 2 characters in the API request (/r/n),
                                        // yet only 1 in the textarea.
                                        this.value = this.value.substring(0, this.value.length - countNewlines);
                                    }
                                });
                            });
                        });
                    </script>
                </div>
            </div>
        </th:block>
    </body>
</html>
