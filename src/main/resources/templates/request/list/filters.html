<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
    <!--@thymesVar id="editions" type="java.util.List<nl.tudelft.labracore.api.dto.EditionDetailsDTO>"-->
    <!--@thymesVar id="labs" type="java.util.List<nl.tudelft.queue.dto.view.QueueSessionSummaryDTO>"-->
    <!--@thymesVar id="courses" type="java.util.List<nl.tudelft.labracore.api.dto.CourseSummaryDTO>"-->
    <!--@thymesVar id="assignments" type="java.util.List<nl.tudelft.labracore.api.dto.AssignmentDetailsDTO>"-->
    <!--@thymesVar id="assignmentsWithCourses" type="java.util.Map<java.lang.Long, nl.tudelft.labracore.api.dto.CourseSummaryDTO>"-->
    <!--@thymesVar id="buildings" type="java.util.List<nl.tudelft.labracore.api.dto.BuildingSummaryDTO>"-->
    <!--@thymesVar id="rooms" type="java.util.List<nl.tudelft.labracore.api.dto.RoomSummaryDTO>"-->
    <!--@thymesVar id="assistants" type="java.util.List<nl.tudelft.labracore.api.dto.PersonSummaryDTO>"-->

    <!--@thymesVar id="filter" type="nl.tudelft.queue.dto.util.RequestTableFilterDTO"-->
    <!--@thymesVar id="returnPath" type="java.lang.String"-->

    <body>
        <div class="mb-5" th:fragment="filters(returnPath, multipleLabs)">
            <div>
                <button type="button" class="button" data-style="outlined" data-dialog="filter-modal">
                    <span class="fa fa-filter"></span>
                    <span>Filters</span>
                    <span th:if="${filter.countActiveFilters() > 0}" th:text="|(${filter.countActiveFilters()})|"></span>
                </button>
            </div>

            <dialog class="dialog" id="filter-modal" data-closable>
                <form class="flex vertical p-7" method="post" th:action="@{/filter}" th:object="${filter}" style="overflow-y: visible">
                    <h3 class="font-500 underlined">Filters</h3>

                    <div id="filter-form" class="grid col-2 align-center" style="--col-1: minmax(0, 8rem)">
                        <input type="hidden" name="return-path" th:value="${returnPath}" />

                        <th:block th:if="${multipleLabs}">
                            <label class="form-control-label" for="lab-select">Lab</label>
                            <select multiple class="select" data-select data-controls id="lab-select" th:field="*{labs}">
                                <th:block th:each="lab : ${labs}">
                                    <option th:value="${lab.id}" th:selected="${filter.labs.contains(lab.id)}" th:text="${lab.readableName}"></option>
                                </th:block>
                            </select>
                        </th:block>

                        <label class="form-control-label" for="assignment-select">Assignment</label>
                        <select multiple class="select" data-select data-controls id="assignment-select" th:field="*{assignments}">
                            <optgroup th:each="course : ${courses}" th:label="${course.name}">
                                <option
                                    th:each="assignment : ${assignments.?[#root.assignmentsWithCourses.get(#this.id)?.id == #root.course.id]}"
                                    th:value="${assignment.id}"
                                    th:selected="${filter.assignments.contains(assignment.id)}"
                                    th:text="${assignment.name}"></option>
                            </optgroup>
                        </select>

                        <label class="form-control-label" for="room-select">Room</label>
                        <select
                            multiple
                            class="select"
                            data-select
                            data-controls
                            id="room-select"
                            th:field="*{rooms}"
                            data-actions-box="true"
                            data-live-search="true">
                            <optgroup th:each="building : ${buildings}" th:label="${building.name}">
                                <option
                                    th:each="room : ${rooms.?[#this.building.id == #root.building.id]}"
                                    th:value="${room.id}"
                                    th:selected="${filter.rooms.contains(room.id)}"
                                    th:text="${room.name}"></option>
                            </optgroup>
                        </select>

                        <label class="form-control-label" for="online-mode-select">Online Mode</label>
                        <select multiple class="select" data-select id="online-mode-select" th:field="*{onlineModes}">
                            <th:block th:each="onlineMode : ${T(nl.tudelft.queue.model.enums.OnlineMode).values()}">
                                <option
                                    th:value="${onlineMode}"
                                    th:selected="${filter.onlineModes.contains(onlineMode)}"
                                    th:text="|${onlineMode.displayName}|"></option>
                            </th:block>
                        </select>

                        <label class="form-control-label" for="assigned-select">Assigned</label>
                        <select multiple class="select" data-select data-controls id="assigned-select" th:field="*{assigned}">
                            <th:block th:each="assistant : ${assistants}">
                                <option
                                    th:value="${assistant.id}"
                                    th:selected="${filter.assigned.contains(assistant.id)}"
                                    th:text="${assistant.displayName}"></option>
                            </th:block>
                        </select>

                        <label class="form-control-label" for="status-select">Status</label>
                        <select multiple class="select" data-select data-controls id="status-select" th:field="*{requestStatuses}">
                            <th:block th:each="status : ${T(nl.tudelft.queue.model.enums.RequestStatus).values()}">
                                <option
                                    th:value="${status.name()}"
                                    th:selected="${filter.requestStatuses.contains(status)}"
                                    th:text="${status.displayName}"></option>
                            </th:block>
                        </select>

                        <label class="form-control-label" for="request-type-select">Type</label>
                        <select multiple class="select" data-select id="request-type-select" th:field="*{requestTypes}">
                            <th:block th:each="requestType : ${T(nl.tudelft.queue.model.enums.RequestType).values()}">
                                <option
                                    th:value="${requestType.name()}"
                                    th:selected="${filter.requestTypes.contains(requestType)}"
                                    th:text="${requestType.displayName()}"></option>
                            </th:block>
                        </select>
                    </div>

                    <div class="flex space-between">
                        <button th:disabled="${filter.isEmpty()}" class="button p-less" name="filter-clear" data-style="outlined">
                            <i class="fa-solid fa-xmark" aria-hidden="true"></i>
                            Clear current filters
                        </button>
                        <button type="submit" class="button" name="filter-submit">
                            <span class="fa-solid fa-filter"></span>
                            Filter
                        </button>
                    </div>
                </form>
            </dialog>

            <script type="text/javascript">
                $(document).ready(function () {
                    $("a.filters-reset").click(() => {
                        $("#filter-form")
                            .append(
                                $("<input>").attr({
                                    type: "hidden",
                                    name: "clear",
                                    value: "1",
                                })
                            )
                            .submit();
                    });
                });
            </script>
        </div>
    </body>
</html>
