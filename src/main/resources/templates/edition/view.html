<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" layout:decorate="~{container}">
    <!--@thymesVar id="ec" type="nl.tudelft.labracore.api.dto.EditionCollectionDetailsDTO"-->
    <!--@thymesVar id="edition" type="nl.tudelft.labracore.api.dto.EditionDetailsDTO"-->

    <!--@thymesVar id="message" type="java.lang.String"-->

    <body>
        <main layout:fragment="content">
            <nav class="breadcrumbs mb-5">
                <a th:href="@{/}">Home</a>
                <span>&gt;</span>
                <a th:if="${edition != null && @permissionService.canEnrolForEdition(edition.id)}" th:href="@{/editions}">Catalog</a>
                <a th:unless="${edition != null && @permissionService.canEnrolForEdition(edition.id)}" th:href="@{/}">My Courses</a>
                <span>&gt;</span>
                <a
                    class="breadcrumb-item active"
                    th:if="${ec == null}"
                    th:href="@{/edition/{id}(id=${edition.id})}"
                    th:text="|${edition.course.name} - ${edition.name}|"></a>
                <a class="breadcrumb-item active" th:unless="${ec == null}" th:href="@{/shared-edition/{id}(id=${ec.id})}" th:text="${ec.name}"></a>
            </nav>

            <th:block
                th:if="${ec == null && (edition instanceof T(nl.tudelft.queue.dto.view.QueueEditionViewDTO)) && edition.hidden && @permissionService.canManageEdition(edition.id)}">
                <div id="edition-hidden-banner" style="cursor: pointer" class="banner mb-3" data-type="warning">
                    <p>This edition is not visible to students in Queue. Click on this banner to unhide this edition.</p>
                </div>
                <form id="edition-unhide-form" method="post" th:action="@{/edition/{editionId}/visibility(editionId=${edition.id})}"></form>
                <script th:inline="javascript">
                    document.addEventListener("DOMContentLoaded", function () {
                        const editionUnhideForm = document.getElementById("edition-unhide-form");
                        const editionHiddenBanner = document.getElementById("edition-hidden-banner");
                        editionHiddenBanner.addEventListener("click", function () {
                            editionUnhideForm.submit();
                        });
                    });
                </script>
            </th:block>
            <th:block th:if="${ec == null && @permissionService.canManageEdition(edition.id)}">
                <div class="banner mb-3" data-type="error" th:if="${#lists.isEmpty(edition.modules)}">
                    <span>This edition does not have any modules, create a module first to be able to create sessions.</span>
                </div>
                <div
                    class="banner mb-3"
                    data-type="error"
                    th:if="${assignments != null && !#lists.isEmpty(edition.modules) && #lists.isEmpty(assignments)}">
                    <span>This edition does not have any assignments, create an assignments first to be able to create sessions.</span>
                </div>
            </th:block>

            <div class="flex gap-3 mb-5 space-between align-center | md:vertical md:align-start">
                <h1 th:if="${ec == null}" class="font-800" th:text="|${edition.course.name} - ${edition.name}|"></h1>
                <h1 th:unless="${ec == null}" class="font-800" th:text="${ec.name}"></h1>

                <div th:if="${ec == null}" class="flex gap-3">
                    <a th:href="@{/edition/{id}/enrol(id=${edition.id})}" th:if="${@permissionService.canEnrolForEdition(edition.id)}" class="button">
                        Enrol for this edition
                    </a>

                    <button
                        th:if="${ec == null and @permissionService.canViewEdition(edition.id)}"
                        class="button"
                        data-type="error"
                        data-style="outlined"
                        th:disabled="not ${@permissionService.canLeaveEdition(edition.id)}"
                        th:data-dialog="leave-edition-dialog">
                        Leave edition
                    </button>
                </div>
            </div>

            <div role="tablist" class="tabs mb-5" th:if="${ec == null}">
                <a role="tab" th:aria-selected="${#uri.matches('.*/edition/\d+')}" th:href="@{/edition/{id}(id=${edition.id})}">
                    <span class="fa-solid fa-th-large" aria-hidden="true"></span>
                    Info
                </a>
                <a
                    role="tab"
                    th:if="${@permissionService.canManageParticipants(edition.id)}"
                    th:aria-selected="${#uri.matches('.*/participants.*')}"
                    th:href="@{/edition/{id}/participants(id=${edition.id})}">
                    <span class="fa-solid fa-user" aria-hidden="true"></span>
                    Participants
                </a>
                <a
                    role="tab"
                    th:if="${@permissionService.canViewEdition(edition.id)}"
                    th:aria-selected="${#uri.matches('.*/modules.*')}"
                    th:href="@{/edition/{id}/modules(id=${edition.id})}">
                    <span class="fa-solid fa-boxes" aria-hidden="true"></span>
                    Modules
                </a>
                <a
                    role="tab"
                    th:if="${@permissionService.canViewEdition(edition.id) && !#lists.isEmpty(edition.modules) && !#lists.isEmpty(assignments)}"
                    th:aria-selected="${#uri.matches('.*/labs.*')}"
                    th:href="@{/edition/{id}/labs(id=${edition.id})}">
                    <span class="fa-solid fa-calendar" aria-hidden="true"></span>
                    Sessions
                </a>
                <a
                    role="tab"
                    th:if="${@permissionService.canManageAssignments(edition.id) && !#lists.isEmpty(edition.modules) && !#lists.isEmpty(assignments)}"
                    th:aria-selected="${#uri.matches('.*/status.*')}"
                    th:href="@{/edition/{id}/status(id=${edition.id})}">
                    <span class="fa-solid fa-bar-chart" aria-hidden="true"></span>
                    Status
                </a>
                <a
                    role="tab"
                    th:if="${@permissionService.canManageQuestions(edition.id)}"
                    th:aria-selected="${#uri.matches('.*/questions.*')}"
                    th:href="@{/edition/{id}/questions(id=${edition.id})}">
                    <span class="fa-solid fa-question" aria-hidden="true"></span>
                    Questions
                </a>
            </div>
            <th:block th:unless="${ec == null}">
                <th:block th:replace="~{shared-edition/tabs :: tabs}"></th:block>
            </th:block>

            <div class="alert alert-info mt-md-3" role="alert" th:unless="${#strings.isEmpty(message)}">
                <th:block th:text="${message}"></th:block>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <dialog class="dialog" id="leave-edition-dialog" th:if="${ec == null and @permissionService.canViewEdition(edition.id)}">
                <form th:action="@{/edition/{id}/leave(id=${edition.id})}" method="post" class="flex vertical p-7">
                    <h3 class="underlined font-500">Leave Edition</h3>

                    <p>
                        Are you sure you want to leave the edition
                        <strong class="fw-500" th:text="|${edition.course.name} - ${edition.name}|"></strong>
                        ?
                    </p>

                    <div class="flex space-between">
                        <button type="button" data-cancel class="button p-less" data-style="outlined">Cancel</button>
                        <button type="submit" class="button p-less" data-type="error">Leave Edition</button>
                    </div>
                </form>
            </dialog>

            <div layout:fragment="subcontent">Sub content page</div>
        </main>
    </body>
</html>
