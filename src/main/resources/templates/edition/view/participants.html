<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" layout:decorate="~{edition/view}">
    <!--@thymesVar id="edition" type="nl.tudelft.labracore.api.dto.EditionDetailsDTO"-->

    <!--@thymesVar id="students" type="org.springframework.data.domain.Page<nl.tudelft.labracore.api.dto.PersonSummaryDTO>"-->

    <head>
        <title th:text="|Participants - ${edition.course.name} (${edition.name})|"></title>
    </head>

    <body>
        <section layout:fragment="subcontent" th:with="staffTabActive = ${param.get('staff-tab-active')?.toString() == 'true'}">
            <div class="flex gap-3 mb-5">
                <a th:href="@{/edition/{id}/participants/create(id=${edition.id})}" class="button" data-style="outlined">Add participant</a>
            </div>

            <div class="tabs mb-5" role="tablist">
                <a
                    role="tab"
                    th:aria-selected="not ${staffTabActive}"
                    th:href="@{/edition/{id}/participants(id=${edition.id}, staff-tab-active=false)}">
                    Students
                </a>
                <a role="tab" th:aria-selected="${staffTabActive}" th:href="@{/edition/{id}/participants(id=${edition.id}, staff-tab-active=true)}">
                    Staff
                </a>
            </div>

            <div class="flex vertical" th:if="${staffTabActive}">
                <div class="surface p-0" th:with="teachers = ${@roleDTOService.teachers(edition)}">
                    <h3 class="surface__header">Teachers</h3>

                    <div class="surface__content" th:if="${#lists.isEmpty(teachers)}">There are no teachers participating in this edition.</div>

                    <ul class="surface__content divided list" role="list" th:unless="${#lists.isEmpty(teachers)}">
                        <li class="pbl-3 flex space-between" th:each="person : ${teachers}">
                            <span th:text="${person.displayName}"></span>
                            <div class="flex gap-3" th:if="${@permissionService.canManageTeachers(edition.id)}">
                                <a
                                    class="button p-less fa-solid fa-trash"
                                    data-style="outlined"
                                    data-type="error"
                                    th:href="@{/edition/{editionId}/participants/{personId}/remove(editionId=${edition.id},personId=${person.id})}"
                                    th:unless="${@roleDTOService.isTheOnlyTeacherInEdition(person.getId(),edition)}"></a>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="surface p-0" th:with="headTAs = ${@roleDTOService.headTAs(edition)}">
                    <h3 class="surface__header">Head TAs</h3>

                    <div class="surface__content" th:if="${#lists.isEmpty(headTAs)}">There are no head TAs participating in this edition.</div>

                    <ul class="surface__content divided list" role="list" th:unless="${#lists.isEmpty(headTAs)}">
                        <li class="pbl-3 flex space-between" th:each="person : ${headTAs}">
                            <th:block th:if="${@permissionService.canViewFeedback(person.id)}">
                                <a class="link" th:href="@{/feedback/{id}/manager(id=${person.id})}" th:text="${person.displayName}"></a>
                            </th:block>
                            <span th:unless="${@permissionService.canViewFeedback(person.id)}" th:text="${person.displayName}"></span>
                            <div class="flex gap-3">
                                <a
                                    class="button p-less fa-solid fa-trash"
                                    data-style="outlined"
                                    data-type="error"
                                    th:href="@{/edition/{editionId}/participants/{id}/remove(editionId=${edition.id},id=${person.id})}"></a>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="surface p-0" th:with="assistants = ${@roleDTOService.assistants(edition)}">
                    <h3 class="surface__header">TAs</h3>

                    <div class="surface__content" th:if="${#lists.isEmpty(assistants)}">There are no TAs participating in this edition.</div>

                    <ul class="surface__content divided list" role="list" th:unless="${#lists.isEmpty(assistants)}">
                        <li class="pbl-3 flex space-between" th:each="person : ${assistants}">
                            <th:block th:if="${@permissionService.canViewFeedback(person.id)}">
                                <a class="link" th:href="@{/feedback/{id}/manager(id=${person.id})}" th:text="${person.displayName}"></a>
                            </th:block>
                            <span th:unless="${@permissionService.canViewFeedback(person.id)}" th:text="${person.displayName}"></span>
                            <div class="flex gap-3">
                                <a
                                    class="button p-less fa-solid fa-trash"
                                    data-style="outlined"
                                    data-type="error"
                                    th:href="@{/edition/{editionId}/participants/{id}/remove(editionId=${edition.id},id=${person.id})}"></a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="flex vertical" th:unless="${staffTabActive}">
                <form action="" class="flex gap-3 align-center" method="get">
                    <label for="filter">Find a student:</label>
                    <div class="search">
                        <input
                            type="search"
                            id="filter"
                            class="textfield"
                            name="student-search"
                            placeholder="Student name"
                            th:value="${param.get('student-search')}" />
                        <button class="fa-solid fa-search"></button>
                    </div>
                </form>

                <div class="surface p-0">
                    <h3 class="surface__header">Students</h3>

                    <div class="surface__content" th:if="${#lists.isEmpty(students)}">There are no students participating in this edition.</div>

                    <ul role="list" class="surface__content divided list" th:unless="${students.size == 0}">
                        <li class="pbl-3" th:each="student : ${students}">
                            <th:block th:if="${@permissionService.isAdminOrTeacher()}">
                                <a
                                    class="link"
                                    th:href="@{/history/edition/{editionId}/student/{id}(editionId=${edition.id},id=${student.id})}"
                                    th:text="${student.displayName}"></a>
                            </th:block>
                            <th:block th:unless="${@permissionService.isAdminOrTeacher()}" th:text="${student.displayName}"></th:block>
                            <!-- Disabled at the moment because of referential constraint errors in labracore (groups) -->
                            <!--                    <div class="float-right">-->
                            <!--                        <a class="btn btn-xs btn-danger"-->
                            <!--                           th:href="@{/edition/{editionId}/participants/{id}/remove(editionId=${edition.id},id=${student.id})}">-->
                            <!--                            <i class="fa fa-trash-o" aria-hidden="true"></i>-->
                            <!--                        </a>-->
                            <!--                    </div>-->
                        </li>
                    </ul>
                </div>

                <div th:if="${students != null && students.totalPages > 1}">
                    <th:block th:replace="~{pagination :: pagination (page=${students}, size=3)}"></th:block>
                </div>
            </div>

            <!--    <div th:if="${cleaned != null}">-->
            <!--        <script th:inline="javascript">-->
            <!--            /*<![CDATA[*/-->
            <!--            var count = /*[[${cleaned}]]*/ '0';-->
            <!--            var message = "Cleaned " + count + " users";-->
            <!--            alert(message);-->
            <!--            /*]]>*/-->
            <!--        </script>-->
            <!--    </div>-->
        </section>
    </body>
</html>
