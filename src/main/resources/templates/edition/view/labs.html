<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" layout:decorate="~{edition/view}">
    <!--@thymesVar id="edition" type="nl.tudelft.labracore.api.dto.EditionDetailsDTO"-->
    <!--@thymesVar id="labs" type="java.util.List<nl.tudelft.queue.dto.view.QueueSessionSummaryDTO>"-->
    <!--@thymesVar id="allLabTypes" type="nl.tudelft.queue.models.enums.LabType[]"-->
    <!--@thymesVar id="queueSessionTypes" type="java.util.List<nl.tudelft.queue.model.enums.QueueSessionType>"-->
    <!--@thymesVar id="allModules" type="java.util.List<nl.tudelft.labracore.dto.view.structured.summary.ModuleSummaryDTO>"-->
    <!--@thymesVar id="modules" type="java.util.List<Long>"-->

    <!--@thymesVar id="alert" type="java.lang.String"-->

    <head>
        <title th:text="|Labs - ${edition.course.name} (${edition.name})|"></title>
    </head>

    <body>
        <main layout:fragment="subcontent">
            <div class="flex align-center space-between mb-5">
                <th:block th:unless="${#lists.isEmpty(labs) and #lists.isEmpty(queueSessionTypes) and #lists.isEmpty(modules)}">
                    <form id="filter-form" method="get" class="flex align-end" th:action="@{/edition/{id}/labs(id=${edition.id})}">
                        <div>
                            <label for="lab-type-select">Type</label>
                            <select multiple class="select" data-select th:name="queueSessionTypes" id="lab-type-select">
                                <th:block th:each="labType : ${allLabTypes}">
                                    <option
                                        th:value="${labType}"
                                        th:text="${labType.displayName}"
                                        th:selected="${queueSessionTypes.contains(labType)}"></option>
                                </th:block>
                            </select>
                        </div>

                        <div>
                            <label for="module-select">Module</label>
                            <select multiple class="select" th:name="modules" data-select id="module-select">
                                <th:block th:each="module : ${allModules}">
                                    <option th:value="${module.id}" th:text="${module.name}" th:selected="${modules.contains(module.id)}"></option>
                                </th:block>
                            </select>
                        </div>

                        <div class="flex gap-3">
                            <button type="submit" class="button" data-style="outlined">Filter</button>
                            <a
                                th:unless="${#lists.isEmpty(queueSessionTypes) and #lists.isEmpty(modules)}"
                                class="button"
                                data-style="outlined"
                                th:href="@{/edition/{id}/labs(id=${edition.id})}">
                                Clear current filters
                            </a>
                        </div>
                    </form>
                </th:block>

                <button type="button" class="button" data-dialog="create-session-modal" th:if="${@permissionService.canManageSessions(edition.id)}">
                    <span>Create session</span>
                </button>
            </div>

            <th:block th:if="${#lists.isEmpty(labs)}">No labs for this edition.</th:block>

            <th:block th:unless="${#lists.isEmpty(labs)}">
                <ul class="surface divided list" role="list">
                    <th:block th:each="lab : ${labs}">
                        <li class="flex space-between pbl-3">
                            <div class="flex gap-3 align-center">
                                <a
                                    href="#"
                                    th:href="@{/lab/{id}(id=${lab.id})}"
                                    class="link"
                                    th:text="|${lab.name} ${#temporals.format(lab.slot.opensAt, 'dd MMMM yyyy HH:mm')} - ${#temporals.format(lab.slot.closesAt, 'dd MMMM yyyy HH:mm')} ${lab.slotOccupationString}|"></a>

                                <div class="flex wrap gap-2 p-1 align-center">
                                    <span class="chip" th:if="${lab.slot.open()}">Active</span>
                                    <span class="chip" th:if="${lab.slot.closed()}">Completed</span>

                                    <span class="chip" th:if="${lab.type == T(nl.tudelft.queue.model.enums.QueueSessionType).EXAM}">Exam</span>
                                    <span class="chip" th:if="${lab.type == T(nl.tudelft.queue.model.enums.QueueSessionType).SLOTTED}">Slotted</span>
                                    <span class="chip" th:if="${lab.type == T(nl.tudelft.queue.model.enums.QueueSessionType).CAPACITY}">
                                        Limited Capacity
                                    </span>

                                    <span class="chip" data-type="warning" th:if="${!lab.slot.closed() && lab.status.isOpenToEnqueueing()}">
                                        Open for enqueueing
                                    </span>
                                </div>
                            </div>

                            <div class="flex gap-3">
                                <th:block th:if="${@permissionService.canManageSessions(edition.id)}">
                                    <a th:href="@{/lab/{id}/edit(id=${lab.id})}" class="button p-less fa-solid fa-pencil" data-style="outlined"></a>
                                </th:block>
                                <th:block th:if="${@permissionService.canManageSessions(edition.id)}">
                                    <a
                                        th:href="@{/lab/{labId}/copy(labId=${lab.id})}"
                                        class="button p-less fa-solid fa-copy"
                                        data-style="outlined"></a>
                                </th:block>
                                <th:block th:if="${@permissionService.canManageSessions(edition.id)}">
                                    <button
                                        class="button p-less fa-solid fa-trash"
                                        data-style="outlined"
                                        data-type="error"
                                        th:data-dialog="'delete-session-modal-' + ${lab.id}"></button>
                                </th:block>
                            </div>
                        </li>
                    </th:block>
                </ul>
            </th:block>

            <dialog class="dialog" id="create-session-modal" th:if="${@permissionService.canManageSessions(edition.id)}">
                <form class="flex vertical p-7" method="get" th:action="@{/edition/{id}/lab/create(id=${edition.id})}">
                    <h3 class="underlined font-500">Create session</h3>

                    <div id="create-session-form" class="grid col-2 align-center" style="--col-1: minmax(0, 8rem)">
                        <label for="session-type">Session type</label>
                        <select class="textfield" id="session-type" name="type">
                            <option
                                th:each="lType : ${T(nl.tudelft.queue.model.enums.QueueSessionType).values()}"
                                th:unless="${lType.name() == 'EXAM'}"
                                th:value="${lType.name()}"
                                th:text="${lType.displayName}"></option>
                        </select>
                    </div>

                    <div class="flex space-between">
                        <button type="button" data-cancel class="button p-less" data-style="outlined">Cancel</button>
                        <button type="submit" class="button p-less">Create</button>
                    </div>
                </form>
            </dialog>

            <dialog
                th:each="lab : ${labs}"
                th:id="'delete-session-modal-' + ${lab.id}"
                class="dialog"
                th:if="${@permissionService.canManageSessions(edition.id)}">
                <form th:action="@{/lab/{id}/remove(id=${lab.id})}" method="post" class="flex vertical p-7">
                    <h3 class="underlined font-500">Delete session</h3>

                    <p th:text="|You are about to delete session #${lab.id} (${lab.name})!|"></p>
                    <p>
                        <strong class="fw-500">WARNING:</strong>
                        Deleting labs that already have existing requests is generally not recommended, as it can cause adverse effects. Therefore,
                        please avoid deleting such labs whenever possible.
                    </p>

                    <div class="flex space-between">
                        <button type="button" data-cancel class="button p-less" data-style="outlined">Cancel</button>
                        <button type="submit" class="button p-less" data-type="error">Delete Session</button>
                    </div>
                </form>
            </dialog>
        </main>
    </body>
</html>
