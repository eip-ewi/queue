<!--

    Queue - A Queueing system that can be used to handle labs in higher education
    Copyright (C) 2016-2024  Delft University of Technology

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

-->
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org" xmlns:layout="http://www.ultraq.net.nz/thymeleaf/layout" layout:decorate="~{edition/view}">
    <!--@thymesVar id="edition" type="nl.tudelft.labracore.api.dto.EditionDetailsDTO"-->

    <!--@thymesVar id="modules" type="java.util.List<nl.tudelft.labracore.api.dto.ModuleDetailsDTO>"-->
    <!--@thymesVar id="groups", type="java.util.Map<java.lang.Long, java.util.List<nl.tudelft.labracore.api.dto.StudentGroupDetailsDTO>>"-->

    <head>
        <title th:text="|Modules - ${edition.course.name} (${edition.name})|"></title>
    </head>

    <body>
        <section layout:fragment="subcontent">
            <div class="flex gap-3 mb-5">
                <a
                    class="button"
                    data-style="outlined"
                    th:href="@{/edition/{id}/modules/create(id=${edition.id})}"
                    th:if="${@permissionService.canManageModules(edition.id)}">
                    Create Module
                </a>
            </div>

            <p th:if="${#lists.isEmpty(modules)}">
                No modules.
                <th:block th:if="${@permissionService.canManageModules(edition.id)}">
                    Click on the "Create Module" button to make the first module.
                </th:block>
            </p>

            <div th:unless="${#lists.isEmpty(modules)}" class="flex vertical">
                <div class="flex space-between align-center | md:vertical md:align-start">
                    <div class="flex gap-3 align-center">
                        <span>Select module:</span>
                        <select id="module-select" aria-label="Select module" class="textfield" autocomplete="off">
                            <option th:each="module, idx : ${modules}" th:value="${module.id}" th:text="${module.name}"></option>
                        </select>
                    </div>

                    <div
                        th:each="m, iter : ${modules}"
                        th:id="|module-${m.id}-controls|"
                        class="flex gap-3"
                        th:classappend="${iter.first ? '' : 'hidden'}">
                        <a
                            class="button"
                            data-style="outlined"
                            data-type="error"
                            th:if="${@permissionService.canManageModule(m.id)}"
                            th:href="@{/module/{mId}/remove(mId=${m.id})}"
                            th:text="|Remove module - ${m.name}|"></a>
                    </div>
                </div>

                <div th:each="m, idx : ${modules}" th:hidden="not ${idx.first}" th:id="'module-' + ${m.id}">
                    <div class="grid" th:classappend="${@permissionService.canManageModule(m.id) ? 'col-2 lg:col-1' : ''}">
                        <div class="flex vertical gap-3">
                            <div class="flex space-between align-center">
                                <h3 class="font-500">Assignments</h3>
                            </div>

                            <div class="flex vertical align-start" th:if="${#lists.isEmpty(m.assignments)}">
                                <p>There are no assignments in this module.</p>
                                <a
                                    class="button"
                                    th:if="${@permissionService.canManageModule(m.id)}"
                                    th:href="@{/module/{mId}/assignment/create(mId=${m.id})}">
                                    Create assignment
                                </a>
                            </div>

                            <table class="table" data-style="surface" th:unless="${#lists.isEmpty(m.assignments)}">
                                <tr class="table__header">
                                    <th class="fit-content">Id</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th class="flex justify-end">
                                        <a
                                            class="button p-min"
                                            data-style="outlined"
                                            th:if="${@permissionService.canManageModule(m.id)}"
                                            th:href="@{/module/{mId}/assignment/create(mId=${m.id})}">
                                            Create assignment
                                        </a>
                                    </th>
                                </tr>
                                <tr th:each="assignment, aIdx : ${m.getAssignments()}">
                                    <td class="fit-content" th:text="${assignment.id}"></td>
                                    <td th:text="${assignment.name}"></td>
                                    <td th:text="${assignment.description.text}"></td>

                                    <td class="flex gap-2 justify-end">
                                        <a
                                            th:href="@{/assignment/{aId}/edit(aId=${assignment.id})}"
                                            th:if="${@permissionService.canManageModule(m.id)}"
                                            class="button p-min fa-solid fa-pencil"
                                            data-style="outlined"></a>
                                        <a
                                            th:href="@{/assignment/{aId}/remove(aId=${assignment.id})}"
                                            th:if="${@permissionService.canManageModule(m.id)}"
                                            class="button p-min fa-solid fa-trash"
                                            data-style="outlined"
                                            data-type="error"></a>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div class="flex vertical gap-3" th:if="${@permissionService.canManageModule(m.id)}">
                            <h3 class="font-500">Student Groups</h3>

                            <p th:if="${#lists.isEmpty(groups.get(m.id))}">There are no student groups in this module, configure them in Portal</p>

                            <table class="table" data-style="surface" th:unless="${#lists.isEmpty(groups.get(m.id))}">
                                <tr class="table__header">
                                    <th class="fit-content">Id</th>
                                    <th>Name</th>
                                    <th>Members</th>
                                    <th class="fit-content">Capacity</th>
                                </tr>
                                <tr th:each="group, gIdx : ${groups.get(m.id)}" th:with="students = ${@roleDTOService.studentsL1(group.members)}">
                                    <td class="fit-content" th:text="${group.id}"></td>
                                    <td th:text="${group.name}"></td>
                                    <td th:text="${#strings.listJoin(@roleDTOService.names(students), ', ')}"></td>
                                    <td class="fit-content">
                                        <span
                                            class="chip"
                                            th:classappend="${group.capacity == students.size() ? 'badge-danger' : 'badge-primary'}"
                                            th:text="|${group.capacity} / ${group.capacity}|"></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                document.addEventListener("DOMContentLoaded", function () {
                    const moduleSelect = document.getElementById("module-select");
                    moduleSelect.addEventListener("change", function () {
                        // Hide all
                        moduleSelect.querySelectorAll("option").forEach(option => {
                            document.getElementById(`module-${option.value}`).setAttribute("hidden", "hidden");
                            document.getElementById(`module-${option.value}-controls`).classList.add("hidden");
                        });
                        // Show selected
                        const selectedModule = moduleSelect.value;
                        document.getElementById(`module-${selectedModule}`).removeAttribute("hidden");
                        document.getElementById(`module-${selectedModule}-controls`).classList.remove("hidden");
                    });
                });
            </script>
        </section>
    </body>
</html>
