/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * Returns a contrasting color to the color that was used adjacently in something like a bar graph or a pie chart.
 *
 * @param pos The color to be used, if it exceeds the color palette size, the modulo is used.
 * @returns {string} A string representation in RGB(x,x,x) format, representing the desired color.
 */
function getColorForPosition(pos) {
    pos = Math.abs(pos);
    const colorPalette = [
        "rgb(0, 114, 178)", // Blue
        "rgb(255, 182, 53)", // Yellow
        "rgb(255, 127, 0)", // Orange
        "rgb(0, 158, 115)", // Teal
        "rgb(148, 103, 189)", // Purple
    ];

    return colorPalette[pos % colorPalette.length];
}

/**
 * Function which takes ms and converts it to a nice time display
 *
 * @param ms The amount of ms that the duration covers
 * @returns {string} A string which has a human representation of the spanned time.
 */
function msToHumanReadableTime(ms) {
    let seconds = (ms / 1000).toFixed(1);
    let minutes = (ms / (1000 * 60)).toFixed(1);
    let hours = (ms / (1000 * 60 * 60)).toFixed(1);
    let days = (ms / (1000 * 60 * 60 * 24)).toFixed(1);
    if (seconds < 60) return seconds + " Seconds";
    else if (minutes < 60) return minutes + " Minutes";
    else if (hours < 24) return hours + " Hours";
    else return days + " Days";
}
