/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
"use strict";

useWebsockets();

document.addEventListener("SocketConnected", function (event) {
    let client = event.client;

    client.subscribe(`/user/topic/lab/${labId}/position`, msg => {
        const event = JSON.parse(msg.body);

        console.log(event);

        if (event["type"] === "position-update") {
            $("#position").text(event["position"]);
        } else if (event["type"] === "request-taken") {
            $("#position").text("0");
            new Notification("You are in front of the Queue");
            toast("You are in front of the queue", "info");
        } else if (event["type"] === "request-finished") {
            const url = new URL(window.location.href);
            url.searchParams.set("requestFinished", event.id);
            window.location = url.toString();
        }
    });
});
