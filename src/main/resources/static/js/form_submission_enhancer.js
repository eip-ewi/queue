/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 * Ctrl + Enter submits the form
 */
$(document).keypress(function (e) {
    if (e.ctrlKey && e.keyCode === 10) {
        $(".ctrl-enter-submit").click();
    }
});

/**
 * Disables the button once a TA presses submit and if all text areas are valid.
 */
$(".disable-on-submit").on("click", () => {
    let allFieldsValid = true;
    $("textarea").each((i, obj) => {
        if (!obj.checkValidity()) {
            allFieldsValid = false;
        }
    });
    if (allFieldsValid) {
        setTimeout(() => {
            $(".disable-on-submit").prop("disabled", true);
        }, 0);
    }
});
