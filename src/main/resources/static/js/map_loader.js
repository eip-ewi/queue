/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
$(() => {
    $("#input-room").change(() => {
        // Get the room ID of the currently selected room.
        const roomId = $("#input-room").find(":selected").attr("value");
        updateRequestInfo(roomId);
    });
});

function updateRequestInfo(roomId) {
    const imageHolder = document.getElementById("image-holder");
    if (imageHolder == null) return;
    const image = imageHolder.querySelector("img");
    $.get({
        url: "/room/map/" + roomId,
        success: function (response) {
            imageHolder.removeAttribute("hidden");
            image.setAttribute("src", `/maps/${JSON.parse(response).fileName}`);
        },
        error: function () {
            imageHolder.setAttribute("hidden", "");
        },
    });
}
