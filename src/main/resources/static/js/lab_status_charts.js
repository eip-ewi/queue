/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
let actRequestFreqChart;
let actStatusFreqChart;
let actAssignmentFreqChart;
let actRoomFreqChart;

function drawRequestFreqChart(canvas) {
    return function (freqs) {
        const buckets = freqs.buckets;

        const center = (buckets[1] - buckets[0]) / 2;
        const labels = buckets.map(function (x) {
            return x + center;
        });
        labels.pop();

        if (actRequestFreqChart != null) {
            actRequestFreqChart.data.labels = labels.map(function (x) {
                return new Date(2000, 1, 1, Math.floor(x / 3600), (x % 3600) / 60, x % 60);
            });
            actRequestFreqChart.data.datasets[0].data = freqs.created;
            actRequestFreqChart.data.datasets[1].data = freqs.handled;
            actRequestFreqChart.data.datasets[2].data = freqs.open;
            actRequestFreqChart.update();
        } else {
            actRequestFreqChart = new Chart(canvas, {
                type: "bar",
                data: {
                    labels: labels.map(function (x) {
                        return new Date(2000, 1, 1, Math.floor(x / 3600), (x % 3600) / 60, x % 60);
                    }),
                    datasets: [
                        {
                            label: "Requests created",
                            data: freqs.created,
                            backgroundColor: "rgba(255, 99, 132, 0.2)",
                            borderColor: "rgba(255, 99, 132, 1)",
                            borderWidth: 1,
                        },
                        {
                            label: "Requests handled",
                            data: freqs.handled,
                            backgroundColor: "rgba(255, 206, 86, 0.2)",
                            borderColor: "rgba(255, 206, 86, 1)",
                            borderWidth: 1,
                        },
                        {
                            label: "Requests open",
                            data: freqs.open,
                            backgroundColor: "rgba(54, 162, 235, 0.2)",
                            borderColor: "rgba(54, 162, 235, 1)",
                            borderWidth: 1,
                        },
                    ],
                },
                options: {
                    scales: {
                        y: {
                            ticks: {
                                beginAtZero: true,
                            },
                        },
                        x: {
                            offset: "true",
                            type: "time",
                            distribution: "timeseries",
                            ticks: {
                                source: "labels",
                                min: buckets[0],
                                max: buckets[buckets.size - 1],
                            },
                            time: {
                                unit: "minute",
                                displayFormats: {
                                    minute: "H:mm",
                                },
                            },
                        },
                    },
                },
            });
        }
    };
}

function drawStatusFreqChart(canvas) {
    return function (data) {
        if (actStatusFreqChart != null) {
            actStatusFreqChart.data.datasets[0].data = [data.pending, data.processing, data.approved, data.rejected, data.notFound, data.revoked];
            actStatusFreqChart.update();
        } else {
            actStatusFreqChart = new Chart(canvas, {
                type: "bar",
                data: {
                    labels: ["Pending", "Processing", "Approved", "Rejected", "Not found", "Revoked"],
                    datasets: [
                        {
                            data: [data["pending"], data["processing"], data["approved"], data["rejected"], data["not_found"], data["revoked"]],
                            backgroundColor: [
                                "rgba(0, 123, 255, 0.2)",
                                "rgba(23, 162, 184, 0.2)",
                                "rgba(40, 167, 69, 0.2)",
                                "rgba(255, 99, 132, 0.2)",
                                "rgba(255, 206, 86, 0.2)",
                                "rgba(108, 117, 125, 0.2)",
                            ],
                            borderColor: [
                                "rgba(0, 123, 255, 1)",
                                "rgba(23, 162, 184, 1)",
                                "rgba(40, 167, 69, 1)",
                                "rgba(255, 99, 132, 1)",
                                "rgba(255, 206, 86, 1)",
                                "rgba(108, 117, 125, 1)",
                            ],
                            borderWidth: 1,
                        },
                    ],
                },
                options: {
                    plugins: {
                        legend: {
                            display: false,
                        },
                    },
                    scales: {
                        y: {
                            ticks: {
                                beginAtZero: true,
                            },
                        },
                        x: {
                            offset: "true",
                        },
                    },
                },
            });
        }
    };
}

function drawAssignmentFreqChart(canvas, popAssignment) {
    return function (freqs) {
        const keys = [];
        const values = [];
        Object.entries(freqs).forEach(([key, value]) => {
            keys.push(key.substring(0, key.indexOf("(#")));
            values.push(value);
        });

        if (actAssignmentFreqChart != null) {
            actAssignmentFreqChart.data.datasets[0].data = values;
            actAssignmentFreqChart.data.labels = keys;
            actAssignmentFreqChart.update();
        } else {
            actAssignmentFreqChart = new Chart(canvas, {
                type: "bar",
                data: {
                    labels: keys,
                    datasets: [
                        {
                            data: values,
                            backgroundColor: "rgba(54, 162, 235, 0.2)",
                            borderColor: "rgba(54, 162, 235, 1)",
                            borderWidth: 1,
                        },
                    ],
                },
                options: {
                    plugins: {
                        legend: {
                            display: false,
                        },
                    },
                    scales: {
                        x: {
                            ticks: {
                                autoSkip: false,
                            },
                        },
                        y: {
                            ticks: {
                                beginAtZero: true,
                                precision: 0,
                            },
                        },
                    },
                },
            });
        }
    };
}

function drawRoomFreqChart(canvas) {
    return function (freqs) {
        const keys = Object.keys(freqs);
        const values = keys.map(function (k) {
            return freqs[k];
        });

        if (actRoomFreqChart != null) {
            actRoomFreqChart.data.datasets[0].data = values;
            actRoomFreqChart.data.labels = keys;
            actRoomFreqChart.update();
        } else {
            actRoomFreqChart = new Chart(canvas, {
                type: "bar",
                data: {
                    labels: keys,
                    datasets: [
                        {
                            data: values,
                            backgroundColor: Array(keys.length).fill("rgba(255, 154, 96, 0.2)"),
                            borderColor: Array(keys.length).fill("rgba(255, 154, 96, 1)"),
                            borderWidth: 1,
                        },
                    ],
                },
                options: {
                    plugins: {
                        legend: {
                            display: false,
                        },
                    },
                    scales: {
                        x: {
                            ticks: {
                                autoSkip: false,
                            },
                        },
                        y: {
                            ticks: {
                                beginAtZero: true,
                            },
                        },
                    },
                },
            });
        }
    };
}
