/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
$(function () {
    drawGraph("#complete-handledData", "#complete-handledGraph", "#007bff", "# of handled requests");
    drawGraph("#complete-totalData", "#complete-totalGraph", "#2ec1cc", "total # of requests");
    drawGraph("#complete-approvedData", "#complete-approvedGraph", "#28a745", "# of approved requests");
    drawGraph("#complete-rejectedData", "#complete-rejectedGraph", "#dc3545", "# of rejected requests");
    const nrOfLabs = $(".labstatus").length;
    for (let labCount = 1; labCount <= nrOfLabs; labCount++) {
        drawGraph("#" + labCount + "-handledData", "#" + labCount + "-handledGraph", "#007bff", "# of handled requests");
        drawGraph("#" + labCount + "-totalData", "#" + labCount + "-totalGraph", "#2ec1cc", "total # of requests");
        drawGraph("#" + labCount + "-approvedData", "#" + labCount + "-approvedGraph", "#28a745", "# of approved requests");
        drawGraph("#" + labCount + "-rejectedData", "#" + labCount + "-rejectedGraph", "#dc3545", "# of rejected requests");
    }
});

function drawGraph(element, graph, color, label) {
    const graphData = JSON.parse($(element).html());
    const ctx = $(graph).get(0).getContext("2d");
    new Chart(ctx, {
        type: "bar",
        data: {
            labels: graphData[0],
            datasets: [
                {
                    label: label,
                    data: graphData[1],
                    borderWidth: 1,
                    backgroundColor: color,
                },
            ],
        },
        options: {
            scales: {
                y: {
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1,
                    },
                },
            },
        },
    });
}
