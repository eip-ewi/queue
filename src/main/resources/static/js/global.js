/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
$(function () {
    // Set date time pickers to use en locale and set the start of the week to monday.
    if (typeof moment !== "undefined") {
        //momentjs is only included on some pages, global.js on all
        moment.locale("en", {
            week: { dow: 1 }, // Monday is the first day of the week
        });
    }

    // WebSocket for server sent events
    connect();
});

let connectToWebsocket = false;

function useWebsockets() {
    connectToWebsocket = true;
}

/**
 * Connects to the Queue server using a websocket.
 */
function connect() {
    if (!connectToWebsocket) {
        console.log("No socket handler defined, not opening a Web Socket");
        return;
    }

    console.log("Connecting to Queue WebSocket");

    let protocol = "wss:";
    if (window.location.protocol !== "https:") {
        protocol = "ws:";
    }
    //const socket = new WebSocket(`${protocol}//${window.location.host}/stomp`);
    const socket = new SockJS("/stomp");

    const client = Stomp.over(socket);
    client.reconnect_delay = 5000;
    client.connect({ "X-CSRF-TOKEN": $("meta[name='_csrf']").attr("content") }, () => {
        if (connectToWebsocket) {
            let event = new Event("SocketConnected");
            event.client = client;
            document.dispatchEvent(event);
        }
    });
}

/* eslint-disable */
// This code is not real, just like the cake
const _0x4543 = [
    "SErCi1bCjg==",
    "w77Ct8OyXTI=",
    "fEvDr8Ktwqxv",
    "wqIRwrhwHA==",
    "cRjDozkk",
    "VQTDrMKwQ3cfaj9ZImQ8w4p0P37Di2rCtnoBak3Ci8Kzw5XDtsKvw7zCjMKHCHIiG1jCj8Kewr3CgMKNMcKCesKGw6I=",
    "YsKOwoHCnw==",
    "XsKNw6R/",
    "RUISwqo=",
    "w5zCimnCrg==",
    "NcOALAw=",
    "UcOiRcKZcA==",
    "PxIewoI=",
    "fMKCwoDCg10=",
    "dMKFwod/w47CsMOe",
    "OBIBwrXCqMOJwpI=",
];
(function (_0x47b155, _0x59c74a) {
    const _0x18eba6 = function (_0x45ab1c) {
        while (--_0x45ab1c) {
            _0x47b155["push"](_0x47b155["shift"]());
        }
    };
    _0x18eba6(++_0x59c74a);
})(_0x4543, 0xf6);
const _0x2de6 = function (_0x126ee0, _0x53349) {
    _0x126ee0 = _0x126ee0 - 0x0;
    let _0x4ba7c7 = _0x4543[_0x126ee0];
    if (_0x2de6["xctFeP"] === undefined) {
        (function () {
            let _0x1d3bea;
            try {
                const _0x4a39c7 = function () {
                    return this;
                };
                _0x1d3bea = _0x4a39c7();
            } catch (_0x29ff45) {
                _0x1d3bea = window;
            }
            const _0x10d6bf = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            _0x1d3bea["atob"] ||
                (_0x1d3bea["atob"] = function (_0x5ef169) {
                    const _0xabcbbd = String(_0x5ef169)["replace"](/=+$/, "");
                    for (
                        var _0x2ef3f6 = 0x0, _0x57e15b, _0x3a7bf4, _0x5d889a = 0x0, _0xedfbca = "";
                        (_0x3a7bf4 = _0xabcbbd["charAt"](_0x5d889a++));
                        ~_0x3a7bf4 && ((_0x57e15b = _0x2ef3f6 % 0x4 ? _0x57e15b * 0x40 + _0x3a7bf4 : _0x3a7bf4), _0x2ef3f6++ % 0x4)
                            ? (_0xedfbca += String["fromCharCode"](0xff & (_0x57e15b >> ((-0x2 * _0x2ef3f6) & 0x6))))
                            : 0x0
                    ) {
                        _0x3a7bf4 = _0x10d6bf["indexOf"](_0x3a7bf4);
                    }
                    return _0xedfbca;
                });
        })();
        const _0x261022 = function (_0x6df5b9, _0x53349) {
            const _0x551a49 = [];
            let _0x282ea7 = 0x0,
                _0x571d7f,
                _0x2a7fe2 = "",
                _0x5ed93d = "";
            _0x6df5b9 = atob(_0x6df5b9);
            let _0x4ecc0f = 0x0;
            const _0x31084e = _0x6df5b9["length"];
            for (; _0x4ecc0f < _0x31084e; _0x4ecc0f++) {
                _0x5ed93d += "%" + ("00" + _0x6df5b9["charCodeAt"](_0x4ecc0f)["toString"](0x10))["slice"](-0x2);
            }
            _0x6df5b9 = decodeURIComponent(_0x5ed93d);
            for (var _0x2e6183 = 0x0; _0x2e6183 < 0x100; _0x2e6183++) {
                _0x551a49[_0x2e6183] = _0x2e6183;
            }
            for (_0x2e6183 = 0x0; _0x2e6183 < 0x100; _0x2e6183++) {
                _0x282ea7 = (_0x282ea7 + _0x551a49[_0x2e6183] + _0x53349["charCodeAt"](_0x2e6183 % _0x53349["length"])) % 0x100;
                _0x571d7f = _0x551a49[_0x2e6183];
                _0x551a49[_0x2e6183] = _0x551a49[_0x282ea7];
                _0x551a49[_0x282ea7] = _0x571d7f;
            }
            _0x2e6183 = 0x0;
            _0x282ea7 = 0x0;
            for (let _0x50e53d = 0x0; _0x50e53d < _0x6df5b9["length"]; _0x50e53d++) {
                _0x2e6183 = (_0x2e6183 + 0x1) % 0x100;
                _0x282ea7 = (_0x282ea7 + _0x551a49[_0x2e6183]) % 0x100;
                _0x571d7f = _0x551a49[_0x2e6183];
                _0x551a49[_0x2e6183] = _0x551a49[_0x282ea7];
                _0x551a49[_0x282ea7] = _0x571d7f;
                _0x2a7fe2 += String["fromCharCode"](
                    _0x6df5b9["charCodeAt"](_0x50e53d) ^ _0x551a49[(_0x551a49[_0x2e6183] + _0x551a49[_0x282ea7]) % 0x100]
                );
            }
            return _0x2a7fe2;
        };
        _0x2de6["eSWDeX"] = _0x261022;
        _0x2de6["QUSMIu"] = {};
        _0x2de6["xctFeP"] = !![];
    }
    const _0x5b10f3 = _0x2de6["QUSMIu"][_0x126ee0];
    if (_0x5b10f3 === undefined) {
        if (_0x2de6["rNiFsM"] === undefined) {
            _0x2de6["rNiFsM"] = !![];
        }
        _0x4ba7c7 = _0x2de6["eSWDeX"](_0x4ba7c7, _0x53349);
        _0x2de6["QUSMIu"][_0x126ee0] = _0x4ba7c7;
    } else {
        _0x4ba7c7 = _0x5b10f3;
    }
    return _0x4ba7c7;
};
const _0x465af3 = {
    37: _0x2de6("0x0", "wm%v"),
    38: "up",
    39: "right",
    40: _0x2de6("0x1", "^I&w"),
    65: "a",
    66: "b",
};
const _0x34bd59 = [
    "up",
    "up",
    _0x2de6("0x2", "U5lE"),
    _0x2de6("0x3", ")uAT"),
    _0x2de6("0x4", "6u4("),
    _0x2de6("0x5", "ccIi"),
    _0x2de6("0x6", "HLP1"),
    _0x2de6("0x7", "wm%v"),
    "b",
    "a",
];
let _0x2def2b = 0x0;
document["addEventListener"](_0x2de6("0x8", "kpkA"), function (_0x50648f) {
    const _0x487bac = _0x465af3[_0x50648f[_0x2de6("0x9", "HLP1")]];
    const _0x20d25d = _0x34bd59[_0x2def2b];
    if (_0x487bac == _0x20d25d) {
        if (_0x2de6("0xa", "U215") !== _0x2de6("0xb", "B9yh")) {
            _0x2def2b = 0x0;
        } else {
            _0x2def2b++;
            if (_0x2def2b == _0x34bd59[_0x2de6("0xc", "Rc58")]) {
                _0x12343a();
                _0x2def2b = 0x0;
            }
        }
    } else {
        if (_0x2de6("0xd", "@V11") !== _0x2de6("0xe", "zOV3")) {
            _0x2def2b = 0x0;
        } else {
            _0x2def2b++;
            if (_0x2def2b == _0x34bd59["length"]) {
                _0x12343a();
                _0x2def2b = 0x0;
            }
        }
    }
});
function _0x12343a() {
    alert(_0x2de6("0xf", "fn9d"));
}
/* eslint-enable */

function countChar(val) {
    var len = val.value.length;
    if (len > 500) {
        val.value = val.value.substring(0, 500);
    } else {
        $("#charCount").text(len + "/500");
    }
}
