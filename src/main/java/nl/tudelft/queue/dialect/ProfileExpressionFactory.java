/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dialect;

import java.util.Optional;
import java.util.Set;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.expression.IExpressionObjectFactory;

import nl.tudelft.labracore.lib.security.LabradorUserDetails;
import nl.tudelft.queue.repository.ProfileRepository;

public class ProfileExpressionFactory implements IExpressionObjectFactory {
	private static final String PROFILE_NAME = "profile";

	private final ProfileRepository profileRepository;

	public ProfileExpressionFactory(ProfileRepository profileRepository) {
		this.profileRepository = profileRepository;
	}

	@Override
	public Set<String> getAllExpressionObjectNames() {
		return Set.of(PROFILE_NAME);
	}

	@Override
	public Object buildObject(IExpressionContext context, String expressionObjectName) {
		if (PROFILE_NAME.equals(expressionObjectName)) {
			return getUserDetails()
					.map(LabradorUserDetails::getUser)
					.map(profileRepository::findProfileForPerson)
					.orElse(null);
		}

		return null;
	}

	@Override
	public boolean isCacheable(String expressionObjectName) {
		return true;
	}

	private Optional<LabradorUserDetails> getUserDetails() {
		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		if (auth == null) {
			return Optional.empty();
		}

		if (auth.getPrincipal() instanceof LabradorUserDetails) {
			return Optional.of((LabradorUserDetails) auth.getPrincipal());
		}

		return Optional.empty();
	}
}
