/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.startup;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import nl.tudelft.queue.model.QueueSession;
import nl.tudelft.queue.model.Request;
import nl.tudelft.queue.repository.QueueSessionRepository;

/**
 * A service that runs during startup. This startup procedure goes through all requests in the database and
 * makes sure the request status updates are applied to the latest update.
 */
@Service
@ConditionalOnExpression("#{'${queue.startup.repair-requests}' == 'true'}")
public class RequestRepairStartupService {
	private static final Logger logger = LoggerFactory.getLogger(RequestRepairStartupService.class);

	@Autowired
	private QueueSessionRepository lr;

	@Transactional
	@EventListener(ApplicationReadyEvent.class)
	public void bla() {
		for (QueueSession<?> qSession : lr.findAll()) {
			// Fetch all requests through repository to allow for garbage collection to pick up this list
			var requests = qSession.getRequests();

			logger.info("Attempting to apply status for {} requests in lab #{}", requests.size(),
					qSession.getId());

			// For each of the requests in this list of requests, perform the status application
			for (Request<?> request : requests) {
				request.getEventInfo().apply(request.getEventInfo());
			}
		}
	}
}
