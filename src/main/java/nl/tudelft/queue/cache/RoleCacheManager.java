/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.cache;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import nl.tudelft.labracore.api.RoleControllerApi;
import nl.tudelft.labracore.api.dto.RoleDetailsDTO;
import nl.tudelft.labracore.api.dto.RoleId;
import nl.tudelft.librador.cache.CoreCacheManager;

@Component
@RequestScope
public class RoleCacheManager extends CoreCacheManager<RoleId, RoleDetailsDTO> {
	@Autowired
	private RoleControllerApi api;

	@Autowired
	private PersonCacheManager pCache;

	@Override
	protected Set<RoleDetailsDTO> fetch(Set<RoleId> ids) {
		return api.getRolesById(
				ids.stream().map(RoleId::getEditionId).collect(Collectors.toSet()),
				ids.stream().map(RoleId::getPersonId).collect(Collectors.toSet()))
				.collect(Collectors.toSet()).block();
	}

	@Override
	protected RoleId getId(RoleDetailsDTO dto) {
		return dto.getId();
	}

	@Override
	protected void registerAdditionally(RoleDetailsDTO dto) {
		pCache.register(dto.getPerson());
	}
}
