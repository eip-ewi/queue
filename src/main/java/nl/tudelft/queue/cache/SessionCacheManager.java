/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.cache;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import nl.tudelft.labracore.api.SessionControllerApi;
import nl.tudelft.labracore.api.dto.SessionDetailsDTO;
import nl.tudelft.librador.cache.CoreCacheManager;
import nl.tudelft.queue.scope.CacheScope;

@Component
@CacheScope
@NoArgsConstructor
@AllArgsConstructor
public class SessionCacheManager extends CoreCacheManager<Long, SessionDetailsDTO> {
	@Autowired
	private SessionControllerApi api;

	@Override
	protected Set<SessionDetailsDTO> fetch(Set<Long> ids) {
		return api.getSessionsById(ids.stream().toList()).collect(Collectors.toSet()).block();
	}

	@Override
	protected Long getId(SessionDetailsDTO dto) {
		return dto.getId();
	}
}
