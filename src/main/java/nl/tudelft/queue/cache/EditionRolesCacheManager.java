/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.cache;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import lombok.AllArgsConstructor;
import lombok.Data;
import nl.tudelft.labracore.api.EditionControllerApi;
import nl.tudelft.labracore.api.dto.RolePersonDetailsDTO;
import nl.tudelft.librador.cache.CoreCacheManager;

@Component
@RequestScope
public class EditionRolesCacheManager extends CoreCacheManager<Long, EditionRolesCacheManager.RoleHolder> {
	/**
	 * Holds a list of roles cached by the ID of the edition that was to be looked up.
	 */
	@Data
	@AllArgsConstructor
	public static class RoleHolder {
		private Long id;
		private List<RolePersonDetailsDTO> roles;
	}

	private final EditionControllerApi eApi;

	public EditionRolesCacheManager(@Autowired EditionControllerApi eApi) {
		this.eApi = eApi;
	}

	@Override
	protected Set<RoleHolder> fetch(Set<Long> ids) {
		return ids.stream()
				.map(id -> new RoleHolder(id, eApi.getEditionParticipants(id).collectList().block()))
				.collect(Collectors.toSet());
	}

	@Override
	protected Long getId(RoleHolder roleHolder) {
		return roleHolder.id;
	}

	@Override
	protected int batchSize() {
		// Set batch size to MAX because we already do batches of n=1 in fetch.
		return Integer.MAX_VALUE;
	}
}
