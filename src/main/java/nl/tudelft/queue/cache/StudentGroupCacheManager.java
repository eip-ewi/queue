/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.cache;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import nl.tudelft.labracore.api.StudentGroupControllerApi;
import nl.tudelft.labracore.api.dto.RolePersonLayer1DTO;
import nl.tudelft.labracore.api.dto.StudentGroupDetailsDTO;
import nl.tudelft.labracore.api.dto.StudentGroupSummaryDTO;
import nl.tudelft.librador.cache.CoreCacheManager;
import nl.tudelft.queue.scope.CacheScope;

@Component
@NoArgsConstructor
@AllArgsConstructor
@CacheScope
public class StudentGroupCacheManager extends CoreCacheManager<Long, StudentGroupDetailsDTO> {
	private final Map<Long, Set<StudentGroupDetailsDTO>> attemptedPersons = new HashMap<>();

	@Autowired
	private StudentGroupControllerApi api;

	@Autowired
	private PersonCacheManager pCache;

	/**
	 * Gets all student groups of a single person by their id.
	 *
	 * @param  personId The id of the person to lookup student groups for.
	 * @return          The list of student group details fetched for this person.
	 */
	public Set<StudentGroupDetailsDTO> getByPerson(Long personId) {
		if (attemptedPersons.containsKey(personId)) {
			return attemptedPersons.get(personId);
		}

		Set<StudentGroupDetailsDTO> sgs = fetch(api.getGroupsForPerson(personId)
				.map(StudentGroupSummaryDTO::getId)
				.collectList().block().stream().collect(Collectors.toSet()));
		attemptedPersons.put(personId, sgs);

		return sgs;
	}

	@Override
	protected Set<StudentGroupDetailsDTO> fetch(@Nullable Set<Long> ids) {
		if (ids == null || ids.isEmpty()) {
			return Set.of();
		}
		return api.getStudentGroupsById(ids.stream().toList()).collect(Collectors.toSet()).block();
	}

	@Override
	protected Long getId(StudentGroupDetailsDTO dto) {
		return dto.getId();
	}

	@Override
	protected void registerAdditionally(StudentGroupDetailsDTO dto) {
		pCache.register(Objects.requireNonNull(dto.getMembers()).stream()
				.map(RolePersonLayer1DTO::getPerson).collect(Collectors.toList()));
	}
}
