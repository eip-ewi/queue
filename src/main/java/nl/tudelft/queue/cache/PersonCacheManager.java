/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.cache;

import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import nl.tudelft.labracore.api.PersonControllerApi;
import nl.tudelft.labracore.api.dto.PersonSummaryDTO;
import nl.tudelft.librador.cache.TimedCacheManager;

@Component
@ApplicationScope
public class PersonCacheManager extends TimedCacheManager<Long, PersonSummaryDTO> {
	@Autowired
	private PersonControllerApi api;

	/**
	 * Creates a new person cache by setting the timeout on its timed cache implementation to 5 minutes.
	 */
	public PersonCacheManager() {
		super(5 * 60 * 1000L);
	}

	@Override
	protected Set<PersonSummaryDTO> fetch(Set<Long> ids) {
		return api.getPeopleById(ids.stream().toList()).collect(Collectors.toSet()).block();
	}

	@Override
	protected Long getId(PersonSummaryDTO dto) {
		return dto.getId();
	}
}
