/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.embeddables;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Basic;
import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class SlottedLabConfig {
	private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm";

	/**
	 * Constructor for the SlottedLabConfig, for non-consecutive labs.
	 *
	 * @param duration         The duration of the slotted lab
	 * @param capacity         The capacity of the slotted lab
	 * @param selectionOpensAt The time when selection opens.
	 */
	public SlottedLabConfig(LocalDateTime selectionOpensAt) {
		this.selectionOpensAt = selectionOpensAt;
		this.consideredFullPercentage = 0;
		this.previousEmptyAllowedThreshold = 0;
	}

	/**
	 * The time starting which a slot can be selected and requests can be made for this slot lab.
	 */
	@NotNull
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime selectionOpensAt;

	/**
	 * The percentage of a slot that has to be filled prior to opening subsequent slots. Used mainly for
	 * consecutive slot filing
	 */
	@NotNull
	@Min(value = 0L, message = "Negative Percentages are not permitted")
	@Max(value = 100L, message = "The Upper Bound is 100")
	@Builder.Default
	@Basic
	private Integer consideredFullPercentage = 0;

	/**
	 * The number of previous slots that are permitted to be below the percentage threshold.
	 */
	@NotNull
	@Min(value = 0L, message = "Negative Values are not permitted")
	@Builder.Default
	@Basic
	private Integer previousEmptyAllowedThreshold = 0;

	/**
	 * Dictates whether students can enqueue in a slot that has already started. When it is true, it means
	 * that students can only enrol for slots that have not yet started.
	 */
	@NotNull
	@Builder.Default
	private Boolean disableLateEnrollment = false;

	/**
	 * Check for consecutive filing mode.
	 *
	 * @return true iff the considered full percentage is not 0, false otherwise.
	 */
	public boolean isConsecutive() {
		return this.consideredFullPercentage != 0;
	}

}
