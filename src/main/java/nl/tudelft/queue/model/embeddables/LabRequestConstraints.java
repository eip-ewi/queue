/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.embeddables;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Embeddable;
import jakarta.persistence.OneToOne;
import lombok.*;
import nl.tudelft.queue.model.LabRequestConstraint;
import nl.tudelft.queue.model.QueueSession;
import nl.tudelft.queue.model.constraints.ClusterConstraint;
import nl.tudelft.queue.model.constraints.ModuleDivisionConstraint;

@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class LabRequestConstraints {
	/**
	 * An optional {@link ClusterConstraint} that can be added to the lab.
	 */
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@OneToOne(mappedBy = "session", cascade = CascadeType.ALL, orphanRemoval = true)
	private ClusterConstraint clusterConstraint;

	/**
	 * An optional {@link ModuleDivisionConstraint} that can be added to the lab.
	 */
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@OneToOne(mappedBy = "session", cascade = CascadeType.ALL, orphanRemoval = true)
	private ModuleDivisionConstraint moduleDivisionConstraint;

	/**
	 * @return A list of all the (individual) constraints held by this embeddable.
	 */
	public List<LabRequestConstraint> toList() {
		return Stream.concat(
				Stream.ofNullable(clusterConstraint),
				Stream.ofNullable(moduleDivisionConstraint)).collect(Collectors.toList());
	}

	/**
	 * Sets the lab to link the constraint to.
	 *
	 * @param session The lab to set.
	 */
	public void setLab(QueueSession<?> session) {
		for (LabRequestConstraint constraint : toList()) {
			constraint.setSession(session);
		}
	}
}
