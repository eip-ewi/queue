/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.embeddables;

import jakarta.persistence.Embeddable;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.model.enums.RequestType;

@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class AllowedRequest {
	@NotNull
	private Long assignment;

	@NotNull
	@Enumerated(EnumType.STRING)
	private RequestType type;

	/**
	 * Creates a new {@link AllowedRequest} from the assignment and type it holds.
	 *
	 * @param  assignment The assignment that is allowed.
	 * @param  type       The type that is allowed.
	 * @return            The created AllowedRequest object.
	 */
	public static AllowedRequest of(Long assignment, RequestType type) {
		return new AllowedRequest(assignment, type);
	}
}
