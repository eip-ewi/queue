/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.embeddables;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.model.enums.SelectionProcedure;

@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class CapacitySessionConfig {
	private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm";

	/**
	 * Enables the use of more advances random selection based on e.g. previous attendance.
	 */
	@NotNull
	private SelectionProcedure procedure;

	/**
	 * The time at which enrolment into the capacity lab starts.
	 */
	@NotNull
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime enrolmentOpensAt;

	/**
	 * The time at which enrolment into the capacity lab ends.
	 */
	@NotNull
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime enrolmentClosesAt;

	/**
	 * The time at which to execute picking and show students who got picked.
	 */
	@NotNull
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime selectionAt;

	/**
	 * @return Whether enrolment into the lab is currently open.
	 */
	public boolean enrolmentOpen() {
		return (LocalDateTime.now().isAfter(enrolmentOpensAt)
				&& LocalDateTime.now().isBefore(enrolmentClosesAt));
	}
}
