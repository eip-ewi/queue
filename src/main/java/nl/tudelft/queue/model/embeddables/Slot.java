/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.embeddables;

import static java.time.LocalDateTime.now;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.Embeddable;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Slot {
	private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm";
	private static final String SIMPLE_SLOT_FORMAT = "HH:mm";

	/**
	 * The time this slot opens at.
	 */
	@NotNull
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime opensAt;

	/**
	 * The time this slot closes at.
	 */
	@NotNull
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime closesAt;

	/**
	 * @return Whether either the close time or open time is the same day as today.
	 */
	public boolean today() {
		var today = LocalDate.now();
		return today.isEqual(opensAt.toLocalDate()) || today.isEqual(closesAt.toLocalDate());
	}

	/**
	 * @return Whether this slot should currently already be closed.
	 */
	public boolean closed() {
		return closesAt.isBefore(now());
	}

	/**
	 * @return Whether this slot is currently open.
	 */
	public boolean open() {
		var now = now();
		return opensAt.isBefore(now) && now.isBefore(closesAt);
	}

	/**
	 * @param  eolGracePeriod The end-of-lab grace period in minutes.
	 * @return                Whether this slot is currently open if the grace period is taken into account.
	 */
	public boolean openWithGracePeriod(int eolGracePeriod) {
		var now = now();
		return opensAt.isBefore(now) && now.isBefore(closesAt.plusMinutes(eolGracePeriod));
	}

	/**
	 * Converts the timeslot into a sentence readable for humans.
	 *
	 * @return The human-readable sentence representing this slot.
	 */
	public String toSentence() {
		var formatter = DateTimeFormatter.ofPattern(SIMPLE_SLOT_FORMAT);

		String slotString = opensAt.format(formatter) + " - " + closesAt.format(formatter);
		if (!today()) {
			formatter = DateTimeFormatter.ofPattern("d MMM uuuu");
			slotString += " on " + closesAt.format(formatter);
		}

		return slotString;
	}

}
