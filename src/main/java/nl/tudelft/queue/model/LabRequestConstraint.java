/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import nl.tudelft.librador.dto.create.Create;

/**
 * A constraint for one lab. For instance, when making a lab that can only be entered once the student has
 * completed and handed in an assignment for that lab, a constraint should be added to that lab. The first
 * example of this can be found in the use-case of only allowing people within a module division to enter the
 * lab.
 */
@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class LabRequestConstraint {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * The name to display when having to describe the constraint with a few words.
	 *
	 * @return The display name as a string.
	 */
	public abstract String displayName();

	/**
	 * Copies this lab request constraint into a create DTO with the same properties.
	 *
	 * @return A new Create DTO to recreate this lab request constraint.
	 */
	public abstract Create<? extends LabRequestConstraint> toCreateDTO();

	/**
	 * Sets the lab to link the constraint to.
	 *
	 * @param session The lab to set.
	 */
	public abstract void setSession(@NotNull QueueSession<?> session);
}
