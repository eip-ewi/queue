/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.misc;

import java.time.LocalDateTime;

import jakarta.persistence.*;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(indexes = {
		@Index(name = "opened", columnList = "startTime, endTime")
})
public class Announcement {
	public static final Integer DEFAULT_BACKGROUND_COLOUR = 0xE75E40;
	public static final Integer DEFAULT_TEXT_COLOUR = 0x000000;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * The message to be displayed in the announcement.
	 */
	@NotBlank
	private String message;

	/**
	 * The colour of the background of the announcement represented as an integer. The integer representation
	 * is the value of the hexadecimal representation of the RGB values of the colour.
	 */
	@NotNull
	@Min(0)
	@Max(16777215)
	@Builder.Default
	private Integer backgroundColour = DEFAULT_BACKGROUND_COLOUR;

	/**
	 * The colour of the text in the announcement represented as an integer. The integer representation is the
	 * value of the hexadecimal representation of the RGB values of the colour.
	 */
	@NotNull
	@Min(0)
	@Max(16777215)
	@Builder.Default
	private Integer textColour = DEFAULT_TEXT_COLOUR;

	/**
	 * The start time of the announcement. From this moment on, the announcement can be seen by everyone.
	 */
	@NotNull
	@Builder.Default
	@Column(name = "startTime")
	private LocalDateTime startTime = LocalDateTime.now();

	/**
	 * The time that the announcement should end. Can be null if the announcement should be manually ended.
	 */
	@Column(name = "endTime")
	private LocalDateTime endTime;

	/**
	 * Whether the announcement is dismissible by the user or not. A non-dismissible announcement will always
	 * be visible to the user and is more interruptive.
	 */
	@NotNull
	@Builder.Default
	private Boolean isDismissible = false;

	/**
	 * @return The 6-digit hexadecimal string representing the background colour for this announcement.
	 */
	public String hexBackgroundColour() {
		return String.format("%06X", backgroundColour);
	}

	/**
	 * @return The 6-digit hexadecimal string representing the text colour for this announcement.
	 */
	public String hexTextColour() {
		return String.format("%06X", textColour);
	}
}
