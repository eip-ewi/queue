/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.constraints;

import java.util.HashSet;
import java.util.Set;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import nl.tudelft.queue.dto.create.constraints.ClusterConstraintCreateDTO;
import nl.tudelft.queue.model.LabRequestConstraint;
import nl.tudelft.queue.model.QueueSession;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ClusterConstraint extends LabRequestConstraint {
	/**
	 * The ids of the clusters of people that are allowed to participate in the lab.
	 */
	@NotNull
	@ElementCollection
	private Set<Long> clusters;

	/**
	 * The Lab that this constraint is linked to.
	 */
	@NotNull
	@OneToOne
	private QueueSession<?> session;

	@Override
	public String displayName() {
		return "Cluster/Mentor Group";
	}

	@Override
	public ClusterConstraintCreateDTO toCreateDTO() {
		return new ClusterConstraintCreateDTO(new HashSet<>(clusters));
	}
}
