/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import nl.tudelft.queue.dto.create.QueueSessionCreateDTO;
import nl.tudelft.queue.dto.create.labs.*;

@Getter
@AllArgsConstructor
public enum QueueSessionType {
	REGULAR(
		"Regular"
	),
	SLOTTED(
		"Slotted"
	),
	@Deprecated
	EXAM(
		"Exam"
	),
	CAPACITY(
		"Limited Capacity"
	);

	/**
	 * The name of the type of lab to display to students and assistants.
	 */
	private final String displayName;

	/**
	 * @return A newly constructed instance of a {@link LabCreateDTO} subclass depending on the type of the
	 *         lab.
	 */
	public QueueSessionCreateDTO<?> newCreateDto() {
		switch (this) {
			case SLOTTED:
				return new SlottedLabCreateDTO();
			case EXAM:
				return new ExamLabCreateDTO();
			case CAPACITY:
				return new CapacitySessionCreateDTO();
			default:
				return new RegularLabCreateDTO();
		}
	}
}
