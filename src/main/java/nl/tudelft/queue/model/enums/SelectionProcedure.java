/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SelectionProcedure {
	FCFS(
		"First Come, First Serve"
	),
	RANDOM(
		"Pure random"
	),
	WEIGHTED_BY_COUNT(
		"Random, but people that got selected less often are prioritized"
	),
	WEIGHTED_BY_LEAST_RECENT(
		"Random, but people that got selected least recently are prioritized"
	);

	/**
	 * The name of the procedure to display to students and assistants.
	 */
	private final String displayName;

	/**
	 * @return {@code true} when this selection procedure is first come first serve.
	 */
	public boolean isFcfs() {
		return this == FCFS;
	}
}
