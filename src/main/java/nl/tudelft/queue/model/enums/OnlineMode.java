/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.enums;

import java.util.Set;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OnlineMode {

	JITSI(
		"Jitsi"
	);

	private final String displayName;

	/**
	 * Maps the online mode(s) to the display name of the respective mode(s) and concatenates them. Used
	 * mainly for displaying the available online modes to end users.
	 *
	 * @param  onlineModes A set of online modes
	 * @return             A concatenated string with all the display names
	 */
	public static String displayNames(Set<OnlineMode> onlineModes) {
		return onlineModes.stream().map(OnlineMode::getDisplayName)
				.sorted(String::compareToIgnoreCase)
				.collect(Collectors.joining(", "));
	}
}
