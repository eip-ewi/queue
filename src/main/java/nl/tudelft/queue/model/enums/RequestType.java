/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.enums;

import org.apache.commons.lang3.StringUtils;

public enum RequestType {
	QUESTION,
	SUBMISSION;

	/**
	 * Converts the request type into a human readable sentence representing what this request type indicates.
	 *
	 * @return The string indicating what this request type means.
	 */
	public String toSentence() {
		switch (this) {
			case QUESTION:
				return "has a question about";
			case SUBMISSION:
				return "wants to submit";
			default:
				return null;
		}
	}

	/**
	 * Gets the display name of this request type. This is just the name of the type with the first letter
	 * capitalized and all other letters lower case.
	 *
	 * @return The display name of this request type.
	 */
	public String displayName() {
		return StringUtils.capitalize(name().toLowerCase());
	}
}
