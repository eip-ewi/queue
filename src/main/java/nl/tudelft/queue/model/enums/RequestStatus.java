/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.enums;

import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public enum RequestStatus {
	PENDING,
	PROCESSING,
	APPROVED,
	REJECTED,
	FORWARDED,
	REVOKED,
	SELECTED,
	NOTSELECTED(
		"Not selected"
	),
	NOTFOUND(
		"Not found"
	),
	NOTPICKED(
		"Not picked"
	);

	@Getter
	@SuppressWarnings("FieldMayBeFinal")
	private String displayName = name().substring(0, 1) + name().substring(1).toLowerCase();

	/**
	 * @return {@code true} when this status reflects that a request is still pending.
	 */
	public boolean isPending() {
		return Set.of(PENDING, FORWARDED).contains(this);
	}

	/**
	 * @return {@code true} when this status is processing.
	 */
	public boolean isProcessing() {
		return PROCESSING == this;
	}

	/**
	 * @return {@code true} when this status reflects that a request has been handled by an assistant.
	 */
	public boolean isHandled() {
		return Set.of(APPROVED, REJECTED, NOTFOUND).contains(this);
	}

	/**
	 * @return {@code true} when this status reflects a request that is fully finished, either by a TA or by
	 *         the requester.
	 */
	public boolean isFinished() {
		return isHandled() || Set.of(REVOKED, NOTPICKED, NOTSELECTED).contains(this);
	}

	/**
	 * @return {@code true} when this status is the revoked status.
	 */
	public boolean isRevoked() {
		return REVOKED == this;
	}

	/**
	 * @return {@code true} when this status is rejected
	 */
	public boolean isRejected() {
		return REJECTED == this;
	}

	/**
	 * @return {@code true} when this status is the approved status.
	 */
	public boolean isApproved() {
		return APPROVED == this;
	}

	/**
	 * @return {@code true} when this status is the selected status.
	 */
	public boolean isSelected() {
		return SELECTED == this;
	}

	/**
	 * @return {@code true} when this status reflects a request that has a selection status. This could either
	 *         be selected or not selected.
	 */
	public boolean hasSelectedState() {
		return Set.of(SELECTED, NOTSELECTED).contains(this);
	}
}
