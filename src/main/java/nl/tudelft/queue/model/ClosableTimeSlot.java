/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model;

import jakarta.persistence.Entity;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.model.embeddables.Slot;
import nl.tudelft.queue.model.labs.AbstractSlottedLab;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ClosableTimeSlot extends TimeSlot {
	/**
	 * Whether the TimeSlot is open for taking new requests from.
	 */
	private Boolean active = true;

	/**
	 * Constructs a new ClosableTimeSlot with defaults for openness.
	 *
	 * @param lab      The lab registering this TimeSlot.
	 * @param slot     The times this TimeSlot represents.
	 * @param capacity The capacity of this single slot.
	 */
	public ClosableTimeSlot(
			@NotNull AbstractSlottedLab<ClosableTimeSlot> lab,
			@NotNull Slot slot,
			@Min(0) @NotNull int capacity) {
		super(lab, slot, capacity);
	}
}
