/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.ManyToOne;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.cqsr.Event;
import nl.tudelft.queue.dto.view.RequestEventViewDTO;
import nl.tudelft.queue.model.embeddables.RequestEventInfo;
import nl.tudelft.queue.model.enums.RequestStatus;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class RequestEvent<R extends Request<?>> extends Event<RequestEventInfo> {
	/**
	 * The request this event happened for.
	 */
	@ManyToOne(targetEntity = Request.class)
	private R request;

	/**
	 * Creates this event using the request that the event is linked to.
	 *
	 * @param request The request this event links to.
	 */
	public RequestEvent(R request) {
		this.request = request;
	}

	@Override
	public RequestEventInfo aggregate() {
		return request.getEventInfo();
	}

	/**
	 * Applies some RequestEvent information to the given info.
	 *
	 * @param status The status that the RequestEvent wants to set the request to.
	 */
	public void apply(RequestEventInfo info, RequestStatus status) {
		info.setStatus(status);
		info.setLastEventAt(getTimestamp());

		if (status.isFinished()) {
			info.setFinishedAt(getTimestamp());
		}
		if (status.isHandled()) {
			info.setHandledAt(getTimestamp());
		}
		if (status.isProcessing()) {
			info.setLastAssignedAt(getTimestamp());
		}

		info.setReasonForAssistant(null);
		info.setReasonForStudent(null);

		info.setAssignedTo(null);
		info.setForwardedTo(null);
	}

	/**
	 * Applies some RequestEvent information to the given info.
	 *
	 * @param status The status that the RequestEvent wants to set the request to.
	 */
	public void apply(RequestEventInfo info, RequestStatus status, Long assignedTo) {
		apply(info, status);

		info.setAssignedTo(assignedTo);
	}

	/**
	 * Applies some RequestEvent information to the given info.
	 *
	 * @param status             The status that the RequestEvent wants to set the request to.
	 * @param reasonForAssistant The reason for handling the request other assistants will see.
	 * @param reasonForStudent   The reason for handling the request the student will see.
	 */
	public void apply(RequestEventInfo info, RequestStatus status, String reasonForAssistant,
			String reasonForStudent) {
		apply(info, status);

		info.setReasonForAssistant(reasonForAssistant);
		info.setReasonForStudent(reasonForStudent);
	}

	/**
	 * Applies some RequestEvent information to the given info.
	 *
	 * @param status The status that the RequestEvent wants to set the request to.
	 */
	public void apply(RequestEventInfo info, RequestStatus status, Long assistant,
			String reasonForAssistant, String reasonForStudent) {
		apply(info, status, reasonForAssistant, reasonForStudent);

		info.setAssignedTo(assistant);
	}

	public abstract Class<? extends RequestEventViewDTO<? extends RequestEvent<R>>> viewClass();

}
