/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.labs;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Stream;

import jakarta.persistence.Embedded;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.queue.model.LabRequest;
import nl.tudelft.queue.model.TimeSlot;
import nl.tudelft.queue.model.embeddables.SlottedLabConfig;

@Data
@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class AbstractSlottedLab<TS extends TimeSlot> extends Lab {
	/**
	 * The embedded configuration of time slots within this lab.
	 */
	@NotNull
	@Embedded
	@Builder.Default
	private SlottedLabConfig slottedLabConfig = new SlottedLabConfig();

	/**
	 * Whether students can grab slots after the slot is finished. This option should be off by default as it
	 * is often undesirable. When turned on, previously finished slots may be selected after their closing
	 * time.
	 */
	@NotNull
	@Builder.Default
	private Boolean canSelectDueSlots = false;

	/**
	 * The time (in minutes) before a slot that the requests in the time slot can already be taken by student
	 * assistants. This time should be carefully considered when configuring a lab, because it is currently
	 * not shown clearly to teaching assistants whether it was possible to get an earlier request than the one
	 * they got. 10 minutes for 20 minute slots is often too much to distinguish and will leave TAs too busy.
	 */
	@Min(0)
	@NotNull
	@Builder.Default
	private Integer earlyOpenTime = 0;

	/**
	 * @return The mapped list of TimeSlots that are created for this lab.
	 */
	public abstract List<TS> getTimeSlots();

	public List<LocalDate> getDays() {
		return getTimeSlots().stream()
				.flatMap(ts -> Stream.of(ts.getSlot().getOpensAt(), ts.getSlot().getClosesAt()))
				.map(LocalDateTime::toLocalDate)
				.distinct()
				.sorted()
				.toList();
	}

	public boolean allowsRequest(LabRequest request) {
		return request.getTimeSlot() != null &&
				((canSelectDueSlots && !request.getTimeSlot().isFull())
						|| request.getTimeSlot().canTakeSlot())
				&&
				super.allowsRequest(request);
	}

	/**
	 * Is the lab in consecutive filling mode. Mainly used in the UI, to render elements conditionally.
	 *
	 * @return true iff lab is in consecutive filling mode, false otherwise.
	 */
	public boolean isConsecutive() {
		return slottedLabConfig.isConsecutive();
	}

}
