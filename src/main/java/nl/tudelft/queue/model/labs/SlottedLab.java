/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.labs;

import java.util.ArrayList;
import java.util.List;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.labracore.api.dto.SessionDetailsDTO;
import nl.tudelft.librador.dto.view.View;
import nl.tudelft.queue.dto.create.labs.LabCreateDTO;
import nl.tudelft.queue.dto.create.labs.SlottedLabCreateDTO;
import nl.tudelft.queue.dto.patch.LabPatchDTO;
import nl.tudelft.queue.dto.patch.labs.SlottedLabPatchDTO;
import nl.tudelft.queue.dto.view.labs.SlottedLabViewDTO;
import nl.tudelft.queue.model.LabRequest;
import nl.tudelft.queue.model.QueueSession;
import nl.tudelft.queue.model.TimeSlot;
import nl.tudelft.queue.model.enums.QueueSessionType;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@DiscriminatorValue("SLOTTED")
@EqualsAndHashCode(callSuper = true)
public class SlottedLab extends AbstractSlottedLab<TimeSlot> {
	/**
	 * The regular timeslots
	 */
	@Builder.Default
	@ToString.Exclude
	@EqualsAndHashCode.Exclude
	@OrderBy("slot.opensAt ASC")
	@OneToMany(mappedBy = "lab", cascade = { CascadeType.ALL })
	private List<TimeSlot> timeSlots = new ArrayList<>();

	@Override
	protected QueueSessionType getLabType() {
		return QueueSessionType.SLOTTED;
	}

	@Override
	public LabCreateDTO<?> copyLabCreateDTO(SessionDetailsDTO session) {
		return new SlottedLabCreateDTO(session, this);
	}

	@Override
	public LabPatchDTO<?> newPatchDTO() {
		return new SlottedLabPatchDTO();
	}

	@Override
	public Class<? extends View<? extends QueueSession<LabRequest>>> viewClass() {
		return SlottedLabViewDTO.class;
	}
}
