/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.events;

import static nl.tudelft.queue.model.enums.RequestStatus.REJECTED;

import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.dto.view.RequestEventViewDTO;
import nl.tudelft.queue.dto.view.events.RequestRejectedEventViewDTO;
import nl.tudelft.queue.model.LabRequest;
import nl.tudelft.queue.model.RequestEvent;
import nl.tudelft.queue.model.enums.RequestStatus;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RequestRejectedEvent extends RequestHandledEvent {
	public RequestRejectedEvent(LabRequest request, Long assistant,
			String reasonForAssistant, String reasonForStudent) {
		super(request, assistant, reasonForAssistant, reasonForStudent);
	}

	@Override
	public RequestStatus status() {
		return REJECTED;
	}

	@Override
	public Class<? extends RequestEventViewDTO<? extends RequestEvent<LabRequest>>> viewClass() {
		return RequestRejectedEventViewDTO.class;
	}
}
