/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.events;

import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.model.LabRequest;
import nl.tudelft.queue.model.RequestEvent;
import nl.tudelft.queue.model.embeddables.RequestEventInfo;
import nl.tudelft.queue.model.enums.RequestStatus;

@Data
@MappedSuperclass
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class RequestHandledEvent extends RequestEvent<LabRequest> implements EventWithAssistant {

	/**
	 * The person that handled the request, the final assistant.
	 */
	@NotNull
	private Long assistant;

	/**
	 * The reason that the request was rejected that will be visible to other TAs.
	 */
	@Size(max = 250)
	private String reasonForAssistant;

	/**
	 * The reason that the request was rejected that will be visible to the students.
	 */
	@Size(max = 250)
	private String reasonForStudent;

	public RequestHandledEvent(LabRequest request,
			@NotNull Long assistant,
			@Size(max = 250) String reasonForAssistant,
			@Size(max = 250) String reasonForStudent) {
		super(request);

		this.assistant = assistant;
		this.reasonForAssistant = reasonForAssistant;
		this.reasonForStudent = reasonForStudent;
	}

	/**
	 * @return The status that this event will set the request to.
	 */
	public abstract RequestStatus status();

	@Override
	public void apply(RequestEventInfo info) {
		apply(info, status(), getAssistant(), reasonForAssistant, reasonForStudent);
	}
}
