/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.events;

/**
 * An interface implemented exclusively by {@link nl.tudelft.queue.model.RequestEvent}s with an assistant
 * field. This interface exists for convenience, it allows for summing up all the assistants that are involved
 * with a single request using just one loop with an instanceof check and subsequent cast.
 */
public interface EventWithAssistant {
	/**
	 * Gets the id of the assistant that caused this request event.
	 *
	 * @return The id of the assistant involved in this request event.
	 */
	Long getAssistant();
}
