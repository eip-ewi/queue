/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.events;

import jakarta.persistence.Entity;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.dto.view.RequestEventViewDTO;
import nl.tudelft.queue.dto.view.events.RequestNotSelectedEventViewDTO;
import nl.tudelft.queue.model.RequestEvent;
import nl.tudelft.queue.model.SelectionRequest;
import nl.tudelft.queue.model.embeddables.RequestEventInfo;
import nl.tudelft.queue.model.enums.RequestStatus;

@Entity
@NoArgsConstructor
public class RequestNotSelectedEvent extends RequestEvent<SelectionRequest> {
	public RequestNotSelectedEvent(SelectionRequest request) {
		super(request);
	}

	@Override
	public void apply(RequestEventInfo info) {
		apply(info, RequestStatus.NOTSELECTED);
	}

	@Override
	public Class<? extends RequestEventViewDTO<? extends RequestEvent<SelectionRequest>>> viewClass() {
		return RequestNotSelectedEventViewDTO.class;
	}
}
