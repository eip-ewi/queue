/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.events;

import static nl.tudelft.queue.model.enums.RequestStatus.REVOKED;

import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.dto.view.RequestEventViewDTO;
import nl.tudelft.queue.dto.view.events.RequestRevokedEventViewDTO;
import nl.tudelft.queue.model.Request;
import nl.tudelft.queue.model.RequestEvent;
import nl.tudelft.queue.model.embeddables.RequestEventInfo;

@Entity
@NoArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
public class RequestRevokedEvent extends RequestEvent<Request<?>> {

	/**
	 * The person who revoked the request. Can be staff or student themself.
	 */
	@NotNull
	private Long revoker;

	public RequestRevokedEvent(Request<?> request, @NotNull Long revoker) {
		super(request);
		this.revoker = revoker;
	}

	@Override
	public void apply(RequestEventInfo info) {
		apply(info, REVOKED);
	}

	@Override
	public Class<? extends RequestEventViewDTO<? extends RequestEvent<Request<?>>>> viewClass() {
		return RequestRevokedEventViewDTO.class;
	}
}
