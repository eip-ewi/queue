/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model.events;

import static nl.tudelft.queue.model.enums.RequestStatus.NOTFOUND;

import jakarta.persistence.Entity;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.dto.view.RequestEventViewDTO;
import nl.tudelft.queue.dto.view.events.StudentNotFoundEventViewDTO;
import nl.tudelft.queue.model.LabRequest;
import nl.tudelft.queue.model.RequestEvent;
import nl.tudelft.queue.model.embeddables.RequestEventInfo;

@Data
@Entity
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StudentNotFoundEvent extends RequestEvent<LabRequest> implements EventWithAssistant {

	/**
	 * The assistant that did not find the request for this {@link RequestEvent}.
	 */
	@NotNull
	private Long assistant;

	public StudentNotFoundEvent(LabRequest request, Long assistant) {
		super(request);

		this.assistant = assistant;
	}

	@Override
	public void apply(RequestEventInfo info) {
		apply(info, NOTFOUND, assistant);
	}

	@Override
	public Class<? extends RequestEventViewDTO<? extends RequestEvent<LabRequest>>> viewClass() {
		return StudentNotFoundEventViewDTO.class;
	}
}
