/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import org.hibernate.annotations.*;

import jakarta.persistence.*;
import jakarta.persistence.Entity;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.model.labs.Lab;

/**
 * Entity class representing feedback given to a TA about one request they contributed to handling. Feedback
 * may be given multiple times per request, but can only given once per TA per request. In other words, one
 * can give feedback on every TA that participated on a request.
 */
@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Where(clause = "deleted_at IS NULL")
@SQLDelete(sql = "UPDATE feedback SET deleted_at = NOW() WHERE assistant_id = ? AND request_id = ?")
public class Feedback {
	/**
	 * The embeddable id of a Feedback object.
	 */
	@Data
	@Embeddable
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Id implements Serializable {
		private static final long serialVersionUID = -1299374329281277135L;

		/**
		 * The id of the request that this feedback is placed on.
		 */
		@NotNull
		private Long requestId;

		/**
		 * The id of the assistant this feedback is about.
		 */
		@NotNull
		private Long assistantId;
	}

	/**
	 * The embedded ID of this Feedback entity.
	 */
	@NotNull
	@EmbeddedId
	private Id id;

	/**
	 * The request from which feedback is given. Feedback is assumed to be given by the request entity
	 * (student group) placing the original request.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@MapsId("requestId")
	private Request<Lab> request;

	/**
	 * The feedback provided by the request group in words.
	 */
	@Size(max = 250)
	private String feedback;

	/**
	 * The rating given by the request group to the assistant.
	 */
	@Min(1)
	@Max(5)
	private Integer rating;

	/**
	 * The time that this feedback was placed on the site. (not the last updated time, but the time that the
	 * feedback first appeared).
	 */
	@NotNull
	@Builder.Default
	private LocalDateTime createdAt = LocalDateTime.now();

	/**
	 * The time that this feedback was last updated by the student group.
	 */
	@NotNull
	@Builder.Default
	private LocalDateTime lastUpdatedAt = LocalDateTime.now();

	@Builder.Default
	private LocalDateTime deletedAt = null;

	@Builder.Default
	@NotNull
	private Boolean isReported = false;

}
