/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;

import nl.tudelft.queue.model.labs.CapacitySession;

public interface CapacitySessionRepository extends QueueRepository<CapacitySession, Long> {

	/**
	 * @return All capacity labs that should select the students that may go to the session.
	 */
	@Query("""
				select distinct session from CapacitySession session
				inner join session.requests request
				where session.capacitySessionConfig.selectionAt < now()
					and request.eventInfo.status = 'PENDING'
					and session.capacitySessionConfig.procedure <> 0
			""")
	List<CapacitySession> findAllThatShouldPickStudents();

	/**
	 * @param  moduleIds The module ids to find labs for.
	 * @return           All capacity labs that covered any of the given list of modules.
	 */
	@Query("""
				select distinct session from CapacitySession session
				inner join session.modules module
				where session.capacitySessionConfig.selectionAt < now()
					and module in ?1
					and session.capacitySessionConfig.procedure <> 0
			""")
	List<CapacitySession> findAllPastLabsByModules(Collection<Long> moduleIds);
}
