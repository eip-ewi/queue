/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

/**
 * A repository base interface providing functionality to throw a 404 error whenever an object lookup by ID
 * fails.
 *
 * @param <T>  The type of the entity class.
 * @param <ID> The type of the id class used to identify the entity.
 */
@NoRepositoryBean
public interface ThrowingRepository<T, ID> extends CrudRepository<T, ID> {
	/**
	 * Looks up the entity object by its id and throws a ResourceNotFoundException when lookup fails,
	 * resulting in Spring MVC handling a 404 error.
	 *
	 * @param  id The id of the object to lookup.
	 * @return    The found entity object.
	 */
	default T findByIdOrThrow404(ID id) {
		return findById(id).orElseThrow(ResourceNotFoundException::new);
	}
}
