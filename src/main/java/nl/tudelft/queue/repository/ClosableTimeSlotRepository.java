/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import nl.tudelft.queue.model.ClosableTimeSlot;

public interface ClosableTimeSlotRepository extends JpaRepository<ClosableTimeSlot, Long> {

	/**
	 * Finds all {@link ClosableTimeSlot}s that are still active and can be closed for evaluation. These time
	 * slots are now defined as all slots that have passed more than 15 minutes ago.
	 *
	 * @return All {@link ClosableTimeSlot}s that can now be closed.
	 */
	default List<ClosableTimeSlot> findAllClosablyActive() {
		return findActiveAndClosesBefore(LocalDateTime.now().minusMinutes(15));
	}

	@Query("""
				select timeslot from ClosableTimeSlot timeslot
				where timeslot.active and timeslot.slot.closesAt < ?1
			""")
	List<ClosableTimeSlot> findActiveAndClosesBefore(LocalDateTime closesBefore);
}
