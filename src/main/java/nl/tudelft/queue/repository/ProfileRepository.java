/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.repository;

import nl.tudelft.labracore.lib.security.user.Person;
import nl.tudelft.queue.model.Profile;

public interface ProfileRepository extends QueueRepository<Profile, Long> {

	/**
	 * Finds the profile belonging to the specified person. If the person does not have a profile yet, creates
	 * a default one.
	 *
	 * @param  person The person whose profile to find
	 * @return        The profile belonging to the person
	 */
	default Profile findProfileForPerson(Person person) {
		return findById(person.getId()).orElseGet(() -> save(Profile.builder().id(person.getId()).build()));
	}

}
