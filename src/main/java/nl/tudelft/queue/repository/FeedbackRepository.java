/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import nl.tudelft.queue.model.Feedback;

public interface FeedbackRepository extends JpaRepository<Feedback, Feedback.Id> {

	/**
	 * @param  requestId   The id of the request to lookup feedback for.
	 * @param  assistantId The id of the assistant to lookup feedback for.
	 * @return             The feedback given to the given assistant on the given request if one exists or
	 *                     none if one cannot be found.
	 */
	default Optional<Feedback> findById(Long requestId, Long assistantId) {
		return findById(new Feedback.Id(requestId, assistantId));
	}

	/**
	 * @param  assistantId The id of the assistant to lookup feedback for.
	 * @return             All feedback objects submitted for the assistant with the given id.
	 */
	@Query("select feedback from Feedback feedback where feedback.id.assistantId = ?1")
	List<Feedback> findByAssistant(Long assistantId);

	/**
	 * Gets the feedback for an assistant, but leaves out too recent feedback.
	 *
	 * @param  assistantId The assistant to find the feedback for.
	 * @return             The list of feedbacks that match the aforementioned criteria.
	 */
	default List<Feedback> findByAssistantAnonymised(Long assistantId) {
		return findByAssistantCreatedBeforeAndSkipSome(assistantId, LocalDateTime.now().minusWeeks(2), 5);
	}

	@Query("""
				select feedback from Feedback feedback
				where feedback.id.assistantId = ?1
					and feedback.createdAt < ?2
				order by feedback.createdAt desc
				offset ?3
			""")
	List<Feedback> findByAssistantCreatedBeforeAndSkipSome(Long assistantId, LocalDateTime createdBefore,
			int skip);

	/**
	 * Gets feedback for an assistant that is marked as deleted.
	 *
	 * @param  assistantId The assistant that we want to look up deleted feedback for.
	 * @return             The list of deleted feedback.
	 */
	@Query(value = "SELECT * FROM feedback WHERE assistant_id = ?1 AND deleted_at IS NOT NULL", nativeQuery = true)
	List<Feedback> findByAssistantDeleted(Long assistantId);

}
