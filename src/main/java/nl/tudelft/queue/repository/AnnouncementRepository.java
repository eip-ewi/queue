/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import nl.tudelft.queue.model.misc.Announcement;

public interface AnnouncementRepository extends
		JpaRepository<Announcement, Long>,
		ThrowingRepository<Announcement, Long> {

	/**
	 * Finds all announcements that should currently be displayed.
	 *
	 * @return The list of active announcements.
	 */
	@Query("""
				select announcement from Announcement announcement
				where announcement.startTime < now()
					and (announcement.endTime is null or announcement.endTime > now())
			""")
	List<Announcement> findActiveAnnouncements();

	/**
	 * Finds all announcements that can be edited by an admin of the Queue. Any announcement that is not
	 * editable can be removed or forgotten. We can still keep them in the database for looking back on when
	 * maintenance was done etc.
	 *
	 * @return The list of announcements that can still be edited by an admin.
	 */
	@Query("""
				select announcement from Announcement announcement
				where announcement.endTime is null or announcement.endTime > now()
			""")
	List<Announcement> findEditableAnnouncements();

}
