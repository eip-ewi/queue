/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.cqsr;

import java.time.LocalDateTime;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@MappedSuperclass
public abstract class Event<T> {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * The exact timestamp that this event occurred.
	 */
	@NotNull
	private LocalDateTime timestamp = LocalDateTime.now();

	/**
	 * Gets the target aggregate.
	 *
	 * @return The target aggregate object.
	 */
	public abstract T aggregate();

	/**
	 * Applies the event to the target aggregate object.
	 *
	 * @param target The target aggregate object to which this event gets applied.
	 */
	public abstract void apply(T target);

	/**
	 * Applies the event to the aggregate object targeted by the event.
	 */
	public void apply() {
		apply(aggregate());
	}
}
