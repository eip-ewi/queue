/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.view;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.labracore.api.dto.ModuleDetailsDTO;
import nl.tudelft.labracore.api.dto.SessionDetailsDTO;
import nl.tudelft.librador.dto.view.View;
import nl.tudelft.queue.misc.QueueSessionStatus;
import nl.tudelft.queue.model.QueueSession;
import nl.tudelft.queue.model.Request;
import nl.tudelft.queue.model.embeddables.LabRequestConstraints;
import nl.tudelft.queue.model.enums.QueueSessionType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public abstract class QueueSessionViewDTO<D extends QueueSession<?>> extends View<D> {
	private QueueSessionType type;

	private Long id;

	private SessionDetailsDTO session;

	private Boolean enqueueClosed;
	private LocalDateTime deletedAt;

	private LabRequestConstraints constraints;

	private Set<ModuleDetailsDTO> modules;

	private List<Request<?>> requests = new ArrayList<>();

	private QueueSessionStatus status;

	private String extraInfo;

	/**
	 * @return Whether the lab session is not yet started.
	 */
	public boolean isBeforeSession() {
		return LocalDateTime.now().isBefore(session.getStart());
	}

	/**
	 * @return Whether this lab is currently open.
	 */
	public boolean isActive() {
		return session.getStart().isBefore(LocalDateTime.now()) &&
				LocalDateTime.now().isBefore(session.getEndTime());
	}

	/**
	 * @return Whether the lab session is finished.
	 */
	public boolean isAfterSession() {
		return session.getEndTime().isBefore(LocalDateTime.now());
	}

	/**
	 * @return Whether enqueueing in this lab is currently opened.
	 */
	public boolean isEnqueueingOpen() {
		return status.isOpenToEnqueueing();
	}

	/**
	 * @return For what reason the lab is closed.
	 */
	public String closedReason() {
		return status.getBadgeLabel();
	}

	/**
	 * @return The underlying lab data.
	 */
	public D getData() {
		return data;
	}
}
