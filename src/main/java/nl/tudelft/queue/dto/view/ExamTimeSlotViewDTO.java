/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.view;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.librador.dto.view.View;
import nl.tudelft.queue.dto.view.requests.ExamRequestViewDTO;
import nl.tudelft.queue.model.ClosableTimeSlot;
import nl.tudelft.queue.model.embeddables.Slot;

@Data
@Deprecated
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ExamTimeSlotViewDTO extends View<ClosableTimeSlot> {
	private Long id;

	private Slot slot;

	private Integer occupied;
	private Integer handled;
	private Integer capacity;

	private Boolean active;

	private List<ExamRequestViewDTO> examRequests;

	@Override
	public void postApply(DTOConverter converter) {
		super.postApply();

		occupied = data.countSlotsOccupied();
		handled = data.countHandledRequests();

		examRequests = converter.convert(new ArrayList<>(data.getRequests()), ExamRequestViewDTO.class);
	}

	/**
	 * Gets the ith request from the request list or an empty optional if none could be located at index i.
	 *
	 * @param  i The index of the request to get.
	 * @return   An Optional either containing the requested request view or an empty Optional.
	 */
	public Optional<ExamRequestViewDTO> getRequest(int i) {
		if (i < examRequests.size()) {
			return Optional.of(examRequests.get(i));
		}
		return Optional.empty();
	}
}
