/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.view.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.labracore.api.dto.PersonSummaryDTO;
import nl.tudelft.queue.dto.view.RequestEventViewDTO;
import nl.tudelft.queue.model.events.RequestHandledEvent;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class RequestHandledEventViewDTO<T extends RequestHandledEvent>
		extends RequestEventViewDTO<T> implements EventWithAssistantView {
	private PersonSummaryDTO assistant;
	private String reasonForAssistant;
	private String reasonForStudent;

	@Override
	public Long getAssistantId() {
		return data.getAssistant();
	}
}
