/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.view.statistics;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.tudelft.labracore.api.dto.PersonSummaryDTO;

/**
 * A data transfer object for forming the assistant table in the statistics page.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssistantRatingViewDto {
	private PersonSummaryDTO person;
	private Long numberOfRequests;
	private Double avgRating;

	private static final double EPSILON = 0.0001d; // offset to make rounding precision related errors impossible.

	/**
	 * Gets the number of filled stars that have to be rendered on a 5 star scale calculated based on the
	 * average rating.
	 *
	 * @return Gets the number of filled stars required.
	 */
	public long getNumFilledStars() {
		return (long) Math.floor(avgRating + EPSILON); // offset to make precision related errors impossible.
	}

	/**
	 * Gets the number of half-filled stars that have to be rendered on a 5 star scale calculated based on the
	 * average rating.
	 *
	 * @return Gets the number of half-filled stars required.
	 */
	public boolean hasHalfFilledStar() {
		double closestHalf = Math.round(avgRating * 2) / 2.0;
		return avgRating >= closestHalf && getNumFilledStars() != Math.round(avgRating + EPSILON);
	}

	/**
	 * Gets the number of empty stars that have to be rendered on a 5 star scale calculated based on the
	 * average rating.
	 *
	 * @return Gets the number of empty stars required.
	 */
	public long getNumEmptyStars() {
		return 5 - (getNumFilledStars() + (hasHalfFilledStar() ? 1 : 0));
	}

}
