/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.view;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.librador.dto.view.View;
import nl.tudelft.queue.model.RequestEvent;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public abstract class RequestEventViewDTO<T extends RequestEvent<?>> extends View<T> {
	private Long id;
	private LocalDateTime timestamp;

	/**
	 * Gets textual description for this event
	 *
	 * @return The textual description of this event.
	 */
	public abstract String getDescription();

	/**
	 * Gets the textual description for this event with assistant-only information included.
	 *
	 * @return The textual description of this event that should be shown to the assistant.
	 */
	public String getDescriptionForAssistant() {
		return getDescription();
	}

	/**
	 * Gets the name of the class that the icon for this event should be using in the request timeline.
	 *
	 * @return The CSS class name for the event icon in a timeline.
	 */
	public abstract String getIconClass();
}
