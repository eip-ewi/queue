/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.view.labs;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.queue.dto.view.ExamTimeSlotViewDTO;
import nl.tudelft.queue.model.embeddables.ExamLabConfig;
import nl.tudelft.queue.model.labs.ExamLab;

@Data
@Deprecated
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ExamLabViewDTO extends AbstractSlottedLabViewDTO<ExamLab> {
	private static final int SLOTS_PER_PAGE = 4;

	private ExamLabConfig examLabConfig;

	private int page;
	private int pages;

	private List<ExamTimeSlotViewDTO> examTimeSlots;

	private int totalHandled;
	private int totalNeeded;
	private int percentageHandled;

	@Override
	public void postApply(DTOConverter converter) {
		super.postApply();

		page = currentPage();
		pages = data.getTimeSlots().size() / SLOTS_PER_PAGE + 1;

		examTimeSlots = converter.convert(data.getTimeSlots(), ExamTimeSlotViewDTO.class);
		totalHandled = data.getHandled().size();
		totalNeeded = (int) Math
				.ceil(data.getRequests().size() * ((double) examLabConfig.getPercentage() / 100.0));
		percentageHandled = totalNeeded == 0 ? 0
				: (int) (((double) totalHandled / (double) totalNeeded) * 100);
	}

	/**
	 * Calculates the index of the page that the exam lab should currently be on based on the time.
	 *
	 * @return The current page index.
	 */
	private int currentPage() {
		int index = getTimeSlots().stream().filter(ts -> ts.getSlot().open()).findFirst()
				.map(ts -> getTimeSlots().indexOf(ts))
				.orElse(0);

		return index / SLOTS_PER_PAGE;
	}

}
