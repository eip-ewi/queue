/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.view.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.queue.dto.view.RequestEventViewDTO;
import nl.tudelft.queue.model.LabRequest;
import nl.tudelft.queue.model.events.StudentNotFoundEvent;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class StudentNotFoundEventViewDTO extends RequestEventViewDTO<StudentNotFoundEvent> {
	private LabRequest request;

	/**
	 * Get the description of the event based on the request's Communication Method.
	 *
	 * @return the description of the event
	 */
	@Override
	public String getDescription() {
		if (request.getOnlineMode() != null) {
			return "Student did not connect on time";
		}
		switch (getRequest().getSession().getCommunicationMethod()) {
			case STUDENT_VISIT_TA:
				return "Student did not show up";
			case TA_VISIT_STUDENT:
				return "Could not find student to handle the request";
			default:
				return "Could not find each other";
		}
	}

	@Override
	public String getIconClass() {
		return "request-not-found";
	}
}
