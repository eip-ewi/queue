/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.view;

import static java.time.LocalDateTime.now;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.labracore.api.dto.EditionSummaryDTO;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.librador.dto.view.View;
import nl.tudelft.queue.misc.QueueSessionStatus;
import nl.tudelft.queue.model.QueueSession;
import nl.tudelft.queue.model.TimeSlot;
import nl.tudelft.queue.model.embeddables.Slot;
import nl.tudelft.queue.model.enums.QueueSessionType;
import nl.tudelft.queue.model.labs.SlottedLab;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QueueSessionSummaryDTO extends View<QueueSession<?>> {
	private Long id;

	private QueueSessionType type;

	private List<EditionSummaryDTO> associatedEditions;

	private String name;
	private String readableName;
	private Slot slot;

	private Integer slotsOccupied;
	private String slotOccupationString = "";

	private Boolean isSlotSelectionOpen = false;
	private Boolean isActiveOrGracePeriod = false;

	private QueueSessionStatus status;

	private Boolean isShared = false;

	@Override
	public void postApply(DTOConverter converter) {
		if (data instanceof SlottedLab) {
			slotsOccupied = slotsOccupied((SlottedLab) data);
			slotOccupationString = slotOccupationString((SlottedLab) data);

			isSlotSelectionOpen = ((SlottedLab) data).getSlottedLabConfig().getSelectionOpensAt()
					.isBefore(now());
		}
	}

	/**
	 * Gets the string describing the current slot occupation of the given lab.
	 *
	 * @param  lab The lab to calculate slot occupation over.
	 * @return     The string representing slot occupation (empty string for non-slotted labs).
	 */
	private String slotOccupationString(SlottedLab lab) {
		long slotsTotal = lab.getTimeSlots().stream().mapToInt(TimeSlot::getCapacity).sum();
		long slotsTaken = lab.getTimeSlots().stream().mapToInt(TimeSlot::countSlotsOccupied).sum();

		return "(" + slotsTaken + "/" + slotsTotal + " slots taken)";
	}

	/**
	 * Gets the current number of slots occupied by student requests in this lab.
	 *
	 * @param  lab The lab to find the number of occupied slots for.
	 * @return     The number of slots in this lab that are currently occupied.
	 */
	private Integer slotsOccupied(SlottedLab lab) {
		return lab.getTimeSlots().stream().mapToInt(TimeSlot::countSlotsOccupied).sum();
	}

	public QueueSession<?> getData() {
		return this.data;
	}

}
