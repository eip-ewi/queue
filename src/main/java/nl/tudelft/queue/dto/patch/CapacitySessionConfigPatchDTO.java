/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.*;
import nl.tudelft.librador.dto.patch.Patch;
import nl.tudelft.queue.model.embeddables.CapacitySessionConfig;
import nl.tudelft.queue.model.enums.SelectionProcedure;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CapacitySessionConfigPatchDTO extends Patch<CapacitySessionConfig> {
	private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm";

	private SelectionProcedure procedure;

	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime enrolmentOpensAt;

	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime enrolmentClosesAt;

	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime selectionAt;

	@Override
	protected void validate() {
	}

	@Override
	protected void applyOneToOne() {
		updateNonNull(procedure, data::setProcedure);
		updateNonNull(enrolmentOpensAt, data::setEnrolmentOpensAt);
		updateNonNull(enrolmentClosesAt, data::setEnrolmentClosesAt);
		updateNonNull(selectionAt, data::setSelectionAt);
	}
}
