/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch;

import static java.time.LocalDateTime.now;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import nl.tudelft.librador.dto.patch.Patch;
import nl.tudelft.queue.model.Feedback;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class FeedbackPatchDTO extends Patch<Feedback> {
	private String feedback;
	private Integer rating;

	@Override
	protected void applyOneToOne() {
		updateNonNull(feedback, data::setFeedback);
		updateNonNull(rating, data::setRating);

		data.setLastUpdatedAt(now());
	}

	@Override
	protected void validate() {
		if (rating != null && (rating < 1 || rating > 5)) {
			errors.rejectValue("feedbackRating", "Feedback rating should be between 1 and 5");
		}
	}
}
