/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch.labs;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import nl.tudelft.labracore.api.dto.AssignmentIdDTO;
import nl.tudelft.queue.dto.patch.CapacitySessionConfigPatchDTO;
import nl.tudelft.queue.dto.patch.QueueSessionPatchDTO;
import nl.tudelft.queue.model.labs.CapacitySession;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CapacitySessionPatchDTO extends QueueSessionPatchDTO<CapacitySession> {

	@Builder.Default
	private CapacitySessionConfigPatchDTO capacitySessionConfig = new CapacitySessionConfigPatchDTO();

	@Override
	public void validate() {
		super.validate();
		nonEmpty("rooms", rooms);
	}

	@Override
	protected void postApply() {
		super.postApply();

		if (capacitySessionConfig != null) {
			converter.apply(capacitySessionConfig, data.getCapacitySessionConfig());
		}
	}

	@Override
	public List<AssignmentIdDTO> assignmentIdDTOs() {
		return null;
	}
}
