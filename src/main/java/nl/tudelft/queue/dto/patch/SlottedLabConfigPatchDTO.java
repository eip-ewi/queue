/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.*;
import nl.tudelft.librador.dto.patch.Patch;
import nl.tudelft.queue.model.embeddables.SlottedLabConfig;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SlottedLabConfigPatchDTO extends Patch<SlottedLabConfig> {
	private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm";

	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime selectionOpensAt;

	@Builder.Default
	private Integer consideredFullPercentage = 0;

	@Builder.Default
	private Integer previousEmptyAllowedThreshold = 0;

	@Builder.Default
	private Boolean disableLateEnrollment = false;

	@Override
	protected void applyOneToOne() {
		updateNonNull(selectionOpensAt, data::setSelectionOpensAt);
		updateNonNull(consideredFullPercentage, data::setConsideredFullPercentage);
		updateNonNull(previousEmptyAllowedThreshold, data::setPreviousEmptyAllowedThreshold);
		updateNonNull(disableLateEnrollment, data::setDisableLateEnrollment);
	}

	@Override
	protected void validate() {
		if (consideredFullPercentage < 0 || consideredFullPercentage > 100) {
			errors.rejectValue("consideredFullPercentage", "Percentage must be an integer between 1-100");
		}

		if (previousEmptyAllowedThreshold < 0) {
			errors.rejectValue("previousEmptyAllowedThreshold", "Value must be greater or equal to 0");
		}

	}
}
