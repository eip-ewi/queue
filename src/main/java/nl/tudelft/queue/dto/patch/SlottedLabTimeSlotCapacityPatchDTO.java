/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import lombok.*;
import nl.tudelft.librador.dto.patch.Patch;
import nl.tudelft.queue.model.TimeSlot;
import nl.tudelft.queue.model.labs.AbstractSlottedLab;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SlottedLabTimeSlotCapacityPatchDTO extends Patch<AbstractSlottedLab<?>> {
	@Builder.Default
	private Map<Long, Integer> capacities = new HashMap<>();

	@Override
	protected void validate() {
		var timeSlots = data.getTimeSlots().stream()
				.collect(Collectors.toMap(TimeSlot::getId, Function.identity()));

		if (!capacities.keySet().stream().allMatch(timeSlots::containsKey)) {
			errors.rejectValue("capacities", "Not all time slots could be matched with slots in this lab.");
		}
	}

	@Override
	protected void applyOneToMany() {
		super.applyOneToMany();

		for (TimeSlot ts : data.getTimeSlots()) {
			if (capacities.containsKey(ts.getId())) {
				ts.setCapacity(capacities.get(ts.getId()));
			}
		}
	}
}
