/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch;

import java.util.*;
import java.util.stream.Collectors;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.labracore.api.dto.AssignmentIdDTO;
import nl.tudelft.labracore.api.dto.RoomIdDTO;
import nl.tudelft.queue.model.enums.CommunicationMethod;
import nl.tudelft.queue.model.enums.OnlineMode;
import nl.tudelft.queue.model.enums.RequestType;
import nl.tudelft.queue.model.labs.Lab;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public abstract class LabPatchDTO<D extends Lab> extends QueueSessionPatchDTO<D> {
	private CommunicationMethod communicationMethod;

	@Min(0)
	@Max(180)
	private Integer eolGracePeriod;

	@Builder.Default
	private Map<Long, Set<RequestType>> requestTypes = null;

	@Builder.Default
	private Set<OnlineMode> onlineModes = null;

	private Boolean enableExperimental;
	private Boolean isBilingual;

	@Override
	protected void applyOneToOne() {
		updateNonNull(communicationMethod, data::setCommunicationMethod);
		updateNonNull(eolGracePeriod, data::setEolGracePeriod);
		if (onlineModes == null && rooms != null) {
			onlineModes = new HashSet<>();
		}
		updateNonNull(onlineModes, data::setOnlineModes);
		data.setEnableExperimental(Boolean.TRUE.equals(enableExperimental)); // null is false as well
		data.setIsBilingual(Boolean.TRUE.equals(isBilingual));
	}

	@Override
	protected void applyManyToManyDominant() {
		super.applyManyToManyDominant();
		updateNonNull(requestTypes, data::setAllowedRequestsFromMap);
	}

	@Override
	protected void validate() {
		super.validate();

		nonEmpty("requestTypes", requestTypes);

		// A null value does not update labracore or queue, therefore we set it to an empty list.
		// No verification needed to check
		if (rooms == null && onlineModes != null) {
			rooms = new HashSet<>();
		} else if (onlineModes == null && rooms != null) {
			onlineModes = new HashSet<>();
		}

	}

	@Override
	public List<AssignmentIdDTO> assignmentIdDTOs() {
		if (requestTypes == null) {
			return null;
		}

		return requestTypes.entrySet().stream()
				.filter(e -> e.getValue() != null && !e.getValue().isEmpty())
				.map(e -> new AssignmentIdDTO().id(e.getKey()))
				.collect(Collectors.toList());
	}

	/**
	 * Overriden method, since if it returns null, it usually signifies "no change" in labracore. From now it
	 * will signify that this is a sesion with online modes only.
	 *
	 * @return A list of roomIdDTOs
	 */
	@Override
	public List<RoomIdDTO> roomIdDTOs() {
		if (rooms == null) {
			if (onlineModes != null) {
				return new ArrayList<>();
			}
			return null;
		}

		return rooms.stream().map(al -> new RoomIdDTO().id(al)).collect(Collectors.toList());
	}

	private void nonEmpty(String field, Map<?, ?> set) {
		if (set != null && set.isEmpty()) {
			errors.rejectValue(field, "Field should not be empty");
		}
	}
}
