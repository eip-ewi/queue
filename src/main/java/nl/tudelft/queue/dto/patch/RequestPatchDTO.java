/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch;

import java.util.List;

import lombok.*;
import nl.tudelft.librador.dto.patch.Patch;
import nl.tudelft.queue.model.LabRequest;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class RequestPatchDTO extends Patch<LabRequest> {
	private Long room;

	private String comment;
	private String question;

	@Override
	protected void applyOneToOne() {
		updateNonNull(room, data::setRoom);

		updateNonNull(comment, data::setComment);
		updateNonNull(question, data::setQuestion);
	}

	@Override
	protected void validate() {
		nonEmpty("question", question);
	}

	public void validateRooms(List<Long> validRoomIds) {
		if (room != null && !validRoomIds.contains(room)) {
			errors.rejectValue("room", "Room is not found in the surrounding lab");
		}
	}
}
