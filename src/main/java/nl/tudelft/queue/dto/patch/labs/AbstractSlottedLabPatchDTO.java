/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch.labs;

import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.queue.dto.patch.LabPatchDTO;
import nl.tudelft.queue.dto.patch.SlottedLabConfigPatchDTO;
import nl.tudelft.queue.model.labs.AbstractSlottedLab;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
@SuperBuilder
public abstract class AbstractSlottedLabPatchDTO<D extends AbstractSlottedLab<?>> extends LabPatchDTO<D> {
	@NotNull
	@Builder.Default
	private SlottedLabConfigPatchDTO slottedLabConfig = new SlottedLabConfigPatchDTO();

	private Boolean canSelectDueSlots;

	private Integer earlyOpenTime;

	@Override
	protected void applyOneToOne() {
		super.applyOneToOne();

		updateNonNull(canSelectDueSlots, data::setCanSelectDueSlots);
		updateNonNull(earlyOpenTime, data::setEarlyOpenTime);
	}

	@Override
	protected void postApply() {
		data.setSlottedLabConfig(converter.apply(slottedLabConfig, data.getSlottedLabConfig()));
	}
}
