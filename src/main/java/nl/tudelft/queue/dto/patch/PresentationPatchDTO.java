/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch;

import java.util.List;

import lombok.*;
import nl.tudelft.librador.dto.patch.Patch;
import nl.tudelft.queue.dto.create.CustomSlideCreateDTO;
import nl.tudelft.queue.model.misc.Presentation;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class PresentationPatchDTO extends Patch<Presentation> {

	private Boolean showCurrentRoom;
	private Boolean showQueueSite;
	private Boolean showLabInfo;
	private Boolean showFeedbackReminder;
	private Boolean showExamEnrolment;

	private List<CustomSlideCreateDTO> customSlides;

	@Override
	protected void applyOneToOne() {
		data.getDefaultSlides().setShowCurrentRoom(Boolean.TRUE.equals(showCurrentRoom));
		data.getDefaultSlides().setShowQueueSite(Boolean.TRUE.equals(showQueueSite));
		data.getDefaultSlides().setShowLabInfo(Boolean.TRUE.equals(showLabInfo));
		data.getDefaultSlides().setShowFeedbackReminder(Boolean.TRUE.equals(showFeedbackReminder));
		data.getDefaultSlides().setShowExamEnrolment(Boolean.TRUE.equals(showExamEnrolment));
	}

	@Override
	protected void validate() {
	}
}
