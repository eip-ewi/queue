/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.patch.embeddables;

import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.librador.dto.patch.Patch;
import nl.tudelft.queue.dto.patch.constraints.ClusterConstraintPatchDTO;
import nl.tudelft.queue.dto.patch.constraints.ModuleDivisionConstraintPatchDTO;
import nl.tudelft.queue.model.QueueSession;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LabRequestConstraintsPatchDTO extends Patch<QueueSession<?>> {

	@Builder.Default
	private ClusterConstraintPatchDTO clusterConstraint = new ClusterConstraintPatchDTO();
	@Builder.Default
	private ModuleDivisionConstraintPatchDTO moduleDivisionConstraint = new ModuleDivisionConstraintPatchDTO();

	@Override
	protected void applyOneToOne() {
	}

	@Override
	protected void validate() {
	}
}
