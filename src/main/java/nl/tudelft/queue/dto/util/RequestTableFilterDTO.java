/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.util;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.librador.dto.Validated;
import nl.tudelft.queue.model.enums.OnlineMode;
import nl.tudelft.queue.model.enums.RequestStatus;
import nl.tudelft.queue.model.enums.RequestType;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class RequestTableFilterDTO extends Validated implements Serializable {
	@NotNull
	private Set<Long> labs = new HashSet<>();
	@NotNull
	private Set<Long> assignments = new HashSet<>();
	@NotNull
	private Set<Long> rooms = new HashSet<>();

	@NotNull
	private Set<OnlineMode> onlineModes = new HashSet<>();
	@NotNull
	private Set<Long> assigned = new HashSet<>();
	@NotNull
	private Set<RequestStatus> requestStatuses = new HashSet<>();
	@NotNull
	private Set<RequestType> requestTypes = new HashSet<>();

	public boolean isEmpty() {
		return getAllFiltersAsStream().filter(s -> !s.isEmpty()).toList().isEmpty();
	}

	/**
	 * Counts the number of filters which are active.
	 *
	 * @return The number of lists which contain at least 1 element to filter on.
	 */
	public long countActiveFilters() {
		return Stream.of(labs, assigned, rooms, onlineModes, assigned, assignments, requestStatuses,
				requestTypes).filter(s -> !s.isEmpty()).count();
	}

	/**
	 * Get all existing filters as a stream.
	 *
	 * @return A stream of Sets.
	 */
	private Stream<Set<?>> getAllFiltersAsStream() {
		return Stream.of(labs, assigned, rooms, onlineModes, assigned, assignments, requestStatuses,
				requestTypes);
	}

	@Override
	protected void validate() {
	}
}
