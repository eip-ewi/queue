/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.id;

import org.modelmapper.TypeToken;
import org.springframework.data.repository.CrudRepository;

import lombok.NoArgsConstructor;
import nl.tudelft.librador.dto.id.IdDTO;
import nl.tudelft.queue.model.QueueSession;
import nl.tudelft.queue.repository.QueueSessionRepository;

@NoArgsConstructor
public class QueueSessionIdDTO extends IdDTO<QueueSession<?>, Long> {
	public QueueSessionIdDTO(Long id) {
		super(id);
	}

	@Override
	public Class<? extends CrudRepository<QueueSession<?>, Long>> repositoryClass() {
		return QueueSessionRepository.class;
	}

	@Override
	public Class<? extends QueueSession<?>> targetClass() {
		return TypeToken.<QueueSession<?>>of(QueueSession.class).getRawType();
	}
}
