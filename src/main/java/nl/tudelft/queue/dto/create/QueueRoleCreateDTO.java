/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import static nl.tudelft.labracore.api.dto.RoleCreateDTO.TypeEnum.*;

import java.util.Set;

import lombok.*;
import nl.tudelft.labracore.api.dto.EditionIdDTO;
import nl.tudelft.labracore.api.dto.RoleCreateDTO;
import nl.tudelft.librador.dto.create.Create;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QueueRoleCreateDTO extends Create<RoleCreateDTO> {
	private Long editionId;
	private String identifier;
	private RoleCreateDTO.TypeEnum type;

	public QueueRoleCreateDTO(Long editionId) {
		this.editionId = editionId;
	}

	@Override
	public Class<nl.tudelft.labracore.api.dto.RoleCreateDTO> clazz() {
		return RoleCreateDTO.class;
	}

	@Override
	public void validate() {
		if (Set.of(ADMIN, TEACHER_RO, BLOCKED).contains(type)) {
			errors.rejectValue("type", "The 'type' field for a role cannot be ADMIN, BLOCKED or TEACHER_RO");
		}
	}

	@Override
	protected void postApply(RoleCreateDTO data) {
		data.edition(new EditionIdDTO().id(editionId));
	}
}
