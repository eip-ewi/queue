/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import nl.tudelft.labracore.api.dto.AssignmentCreateDTO;
import nl.tudelft.labracore.api.dto.Description;
import nl.tudelft.labracore.api.dto.ModuleIdDTO;
import nl.tudelft.librador.dto.create.Create;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QueueAssignmentCreateDTO extends Create<AssignmentCreateDTO> {
	private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm";

	@NotBlank
	private String name;
	@NotNull
	@Builder.Default
	private String description = "";

	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime deadline;

	@NotNull
	private Long moduleId;

	public QueueAssignmentCreateDTO(Long moduleId) {
		this.moduleId = moduleId;
	}

	@Override
	public Class<AssignmentCreateDTO> clazz() {
		return AssignmentCreateDTO.class;
	}

	@Override
	protected void postApply(AssignmentCreateDTO data) {
		data.description(new Description().text(description));
		data.module(new ModuleIdDTO().id(moduleId));
	}
}
