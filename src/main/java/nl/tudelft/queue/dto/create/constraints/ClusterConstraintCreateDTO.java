/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.constraints;

import java.util.HashSet;
import java.util.Set;

import org.modelmapper.TypeToken;

import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.librador.dto.create.Create;
import nl.tudelft.queue.model.constraints.ClusterConstraint;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ClusterConstraintCreateDTO extends Create<ClusterConstraint> {
	@Builder.Default
	private Set<Long> clusters = new HashSet<>();

	@Override
	public Class<ClusterConstraint> clazz() {
		return TypeToken.<ClusterConstraint>of(ClusterConstraint.class).getRawType();
	}
}
