/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.misc;

import java.time.LocalDateTime;

import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.librador.dto.create.Create;
import nl.tudelft.queue.model.misc.Announcement;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AnnouncementCreateDTO extends Create<Announcement> {
	private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm";

	@NotBlank
	private String message;

	@NotNull
	@Builder.Default
	@Length(min = 7, max = 7)
	private String backgroundColourHex = String.format("#%06X", Announcement.DEFAULT_BACKGROUND_COLOUR);

	@NotNull
	@Builder.Default
	@Length(min = 7, max = 7)
	private String textColourHex = String.format("#%06X", Announcement.DEFAULT_TEXT_COLOUR);

	@NotNull
	@Builder.Default
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime startTime = LocalDateTime.now();

	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime endTime;

	@NotNull
	@Builder.Default
	private Boolean isDismissible = false;

	@Override
	protected void postApply(Announcement data) {
		super.postApply(data);

		data.setBackgroundColour(Integer.parseInt(backgroundColourHex, 1, 7, 16));
		data.setTextColour(Integer.parseInt(textColourHex, 1, 7, 16));
	}

	@Override
	public Class<Announcement> clazz() {
		return Announcement.class;
	}
}
