/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.embeddables;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import lombok.*;
import nl.tudelft.librador.dto.create.Create;
import nl.tudelft.queue.model.embeddables.ExamLabConfig;

@Data
@Deprecated
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ExamLabConfigCreateDTO extends Create<ExamLabConfig> {

	/**
	 * The percentage of requests that should be picked by the exam lab at the very least.
	 */
	@Min(value = 10L, message = "Percentage must be at least 10")
	@Max(value = 100L, message = "Percentage cannot exceed 100")
	private Integer percentage;

	public ExamLabConfigCreateDTO(ExamLabConfig config) {
		this.percentage = config.getPercentage();
	}

	@Override
	public Class<ExamLabConfig> clazz() {
		return ExamLabConfig.class;
	}
}
