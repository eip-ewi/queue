/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import nl.tudelft.labracore.api.dto.CohortIdDTO;
import nl.tudelft.labracore.api.dto.CourseIdDTO;
import nl.tudelft.labracore.api.dto.EditionCreateDTO;
import nl.tudelft.librador.dto.create.Create;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QueueEditionCreateDTO extends Create<EditionCreateDTO> {
	private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm";

	@NotBlank
	private String name;

	@NotNull
	private Long course;

	@NotNull
	private Long cohort;

	@NotNull
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime startDate;

	@NotNull
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime endDate;

	private EditionCreateDTO.EnrollabilityEnum enrollability;

	@Builder.Default
	private Boolean importFromCohort = false;

	@Override
	public Class<EditionCreateDTO> clazz() {
		return EditionCreateDTO.class;
	}

	@Override
	protected void postApply(EditionCreateDTO data) {
		super.postApply(data);

		data.course(new CourseIdDTO().id(course));
		data.cohort(new CohortIdDTO().id(cohort));
	}
}
