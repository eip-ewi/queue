/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.labs;

import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.labracore.api.dto.SessionDetailsDTO;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.queue.dto.create.embeddables.ExamLabConfigCreateDTO;
import nl.tudelft.queue.model.labs.ExamLab;

@Data
@Deprecated
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ExamLabCreateDTO extends AbstractSlottedLabCreateDTO<ExamLab> {
	@Builder.Default
	private ExamLabConfigCreateDTO examLabConfig = new ExamLabConfigCreateDTO();

	public ExamLabCreateDTO(SessionDetailsDTO session, ExamLab lab) {
		super(session, lab);

		this.examLabConfig = new ExamLabConfigCreateDTO(lab.getExamLabConfig());
	}

	@Override
	protected void postApply(ExamLab data, DTOConverter converter) {
		super.postApply(data, converter);

		data.setExamLabConfig(examLabConfig.apply(converter));
	}

	@Override
	public void validate() {
		super.validate();

		nonNull("examLabConfig", examLabConfig);
	}

	@Override
	public Class<ExamLab> clazz() {
		return ExamLab.class;
	}
}
