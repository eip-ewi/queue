/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import jakarta.validation.constraints.NotBlank;
import lombok.*;
import nl.tudelft.labracore.api.dto.EditionIdDTO;
import nl.tudelft.labracore.api.dto.ModuleCreateDTO;
import nl.tudelft.librador.dto.create.Create;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QueueModuleCreateDTO extends Create<ModuleCreateDTO> {
	private Long editionId;

	@NotBlank
	private String name;

	public QueueModuleCreateDTO(Long editionId) {
		this.editionId = editionId;
	}

	@Override
	public Class<ModuleCreateDTO> clazz() {
		return ModuleCreateDTO.class;
	}

	@Override
	protected void postApply(ModuleCreateDTO data) {
		data.edition(new EditionIdDTO().id(editionId));
	}
}
