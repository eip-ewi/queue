/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import jakarta.validation.constraints.NotBlank;
import lombok.*;
import nl.tudelft.labracore.api.dto.CourseCreateDTO;
import nl.tudelft.labracore.api.dto.ProgramIdDTO;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.librador.dto.create.Create;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QueueCourseCreateDTO extends Create<CourseCreateDTO> {

	@NotBlank
	private String name;

	@NotBlank
	private String code;

	private Long program;

	@NotBlank
	private String courseManager;

	@Override
	public Class<CourseCreateDTO> clazz() {
		return CourseCreateDTO.class;
	}

	@Override
	protected void postApply(CourseCreateDTO data, DTOConverter converter) {
		super.postApply(data);
		data.program(new ProgramIdDTO().id(program));
	}
}
