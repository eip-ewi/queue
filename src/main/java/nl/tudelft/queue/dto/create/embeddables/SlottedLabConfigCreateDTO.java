/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.embeddables;

import java.time.LocalDateTime;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import nl.tudelft.librador.dto.create.Create;
import nl.tudelft.queue.model.embeddables.SlottedLabConfig;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SlottedLabConfigCreateDTO extends Create<SlottedLabConfig> {
	private static final String TIME_FORMAT = "yyyy-MM-dd'T'HH:mm";

	/**
	 * The time starting which a slot can be selected and requests can be made for this slot lab.
	 */
	@NotNull
	@DateTimeFormat(pattern = TIME_FORMAT)
	private LocalDateTime selectionOpensAt;

	/**
	 * The percentage of a slot that has to be filled prior to opening subsequent slots
	 */
	@NotNull
	@Min(value = 0L, message = "Negative Percentages are not permitted")
	@Max(value = 100L, message = "The Upper Bound is 100")
	@Builder.Default
	private Integer consideredFullPercentage = 0;

	/**
	 * The number of previous slots that are permitted to be below the percentage threshold.
	 */
	@NotNull
	@Min(value = 0L, message = "Negative Values are not permitted")
	@Builder.Default
	private Integer previousEmptyAllowedThreshold = 0;

	@NotNull
	@Builder.Default
	private Boolean disableLateEnrollment = false;

	public SlottedLabConfigCreateDTO(SlottedLabConfig config) {
		this.selectionOpensAt = config.getSelectionOpensAt();
		this.consideredFullPercentage = config.getConsideredFullPercentage();
		this.previousEmptyAllowedThreshold = config.getPreviousEmptyAllowedThreshold();
		this.disableLateEnrollment = config.getDisableLateEnrollment();
	}

	@Override
	public Class<SlottedLabConfig> clazz() {
		return SlottedLabConfig.class;
	}

}
