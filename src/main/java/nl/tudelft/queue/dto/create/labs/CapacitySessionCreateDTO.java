/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create.labs;

import java.util.List;

import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.labracore.api.dto.AssignmentIdDTO;
import nl.tudelft.labracore.api.dto.SessionDetailsDTO;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.queue.dto.create.QueueSessionCreateDTO;
import nl.tudelft.queue.dto.create.embeddables.CapacitySessionConfigCreateDTO;
import nl.tudelft.queue.model.labs.CapacitySession;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class CapacitySessionCreateDTO extends QueueSessionCreateDTO<CapacitySession> {

	@Builder.Default
	private CapacitySessionConfigCreateDTO capacitySessionConfig = new CapacitySessionConfigCreateDTO();

	public CapacitySessionCreateDTO(SessionDetailsDTO session,
			CapacitySession lab) {
		super(session, lab);

		this.capacitySessionConfig = new CapacitySessionConfigCreateDTO(lab.getCapacitySessionConfig());
	}

	@Override
	protected void postApply(CapacitySession data, DTOConverter converter) {
		super.postApply(data, converter);

		data.setCapacitySessionConfig(capacitySessionConfig.apply(converter));
	}

	@Override
	public void validate() {
		super.validate();

		nonEmpty("rooms", rooms);

		if (capacitySessionConfig.getEnrolmentClosesAt()
				.isBefore(capacitySessionConfig.getEnrolmentOpensAt())) {
			errors.rejectValue("capacitySessionConfig.enrolmentClosesAt",
					"Enrolment should close after it opens");
		}

		if (capacitySessionConfig.getSelectionAt() != null &&
				capacitySessionConfig.getSelectionAt()
						.isBefore(capacitySessionConfig.getEnrolmentClosesAt())) {
			errors.rejectValue("capacitySessionConfig.selectionAt",
					"Selection should be after enrolment closes");
		}
	}

	@Override
	public void plusWeeks(Long weeks) {
		super.plusWeeks(weeks);

		capacitySessionConfig
				.setEnrolmentOpensAt(capacitySessionConfig.getEnrolmentOpensAt().plusWeeks(weeks));
		capacitySessionConfig
				.setEnrolmentClosesAt(capacitySessionConfig.getEnrolmentClosesAt().plusWeeks(weeks));
		capacitySessionConfig
				.setSelectionAt((capacitySessionConfig.getSelectionAt() != null)
						? capacitySessionConfig.getSelectionAt().plusWeeks(weeks)
						: null);
	}

	@Override
	public List<AssignmentIdDTO> assignmentIdDTOs() {
		return List.of();
	}

	@Override
	public Class<CapacitySession> clazz() {
		return CapacitySession.class;
	}
}
