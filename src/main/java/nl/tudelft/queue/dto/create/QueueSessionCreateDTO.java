/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import java.util.*;
import java.util.stream.Collectors;

import lombok.*;
import lombok.experimental.SuperBuilder;
import nl.tudelft.labracore.api.dto.*;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.librador.dto.create.Create;
import nl.tudelft.queue.dto.create.embeddables.LabRequestConstraintsCreateDTO;
import nl.tudelft.queue.model.QueueSession;
import nl.tudelft.queue.model.embeddables.Slot;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public abstract class QueueSessionCreateDTO<D extends QueueSession<?>> extends Create<D> {
	private String name;
	private Slot slot;

	@Builder.Default
	private Set<Long> modules = new HashSet<>();

	@Builder.Default
	protected Set<Long> rooms = new HashSet<>();

	@Builder.Default
	private LabRequestConstraintsCreateDTO constraints = new LabRequestConstraintsCreateDTO();

	@Builder.Default
	private Integer repeatForXWeeks = null;

	private String extraInfo;

	public QueueSessionCreateDTO(SessionDetailsDTO session, D lab) {
		name = session.getName();
		slot = new Slot(session.getStart(), session.getEndTime());

		modules = lab.getModules();

		rooms = session.getRooms().stream()
				.map(RoomDetailsDTO::getId)
				.collect(Collectors.toSet());

		constraints = new LabRequestConstraintsCreateDTO(lab.getConstraints());
	}

	@Override
	public void validate() {
		nonEmpty("name", name);
		nonNull("slot", slot);

		nonEmpty("modules", modules);

	}

	@Override
	protected void postApply(D data, DTOConverter converter) {
		data.setConstraints(converter.apply(constraints));
		data.getConstraints().toList().forEach(constraint -> constraint.setSession(data));
	}

	/**
	 * Add a number of weeks to this lab create to create the same lab, but a week later.
	 *
	 * @param weeks The number of weeks to add to the created lab.
	 */
	public void plusWeeks(Long weeks) {
		slot.setOpensAt(slot.getOpensAt().plusWeeks(weeks));
		slot.setClosesAt(slot.getClosesAt().plusWeeks(weeks));
	}

	/**
	 * @return The list of assignments associated with this session, null otherwise.
	 */
	public abstract List<AssignmentIdDTO> assignmentIdDTOs();

	/**
	 * @return The list of room ids if rooms were entered, null otherwise.
	 */
	public List<RoomIdDTO> roomIdDTOs() {
		return rooms.stream().map(al -> new RoomIdDTO().id(al)).collect(Collectors.toList());
	}

	protected void nonNull(String field, Object o) {
		if (o == null) {
			errors.rejectValue(field, "Field cannot be null");
		}
	}

	protected void nonEmpty(String field, Collection<?> i) {
		if (i == null || i.isEmpty()) {
			errors.rejectValue(field, "Field cannot be null or empty");
		}
	}

	protected void nonEmpty(String field, Map<?, ?> i) {
		if (i == null || i.isEmpty()) {
			errors.rejectValue(field, "Field cannot be null or empty");
		}
	}
}
