/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.dto.create;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import nl.tudelft.librador.dto.create.Create;
import nl.tudelft.queue.model.QueueSession;
import nl.tudelft.queue.model.Request;

@Data
@SuperBuilder
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public abstract class RequestCreateDTO<R extends Request<QS>, QS extends QueueSession<R>> extends Create<R> {
	/**
	 * Sets the session that this request will be associated with.
	 *
	 * @param session The session to set to.
	 */
	public abstract void setSession(QS session);

	/**
	 * @return The session that this request is bound to. Filled in after serialization at the server side by
	 *         the controller method handling this DTO before doing the post-apply call. This is required for
	 *         validating this request create DTO.
	 */
	public abstract QS getSession();

	/**
	 * @return The module that this request will be a part of.
	 */
}
