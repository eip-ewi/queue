/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.config;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;
import java.util.Set;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;

import nl.tudelft.queue.dialect.AuthenticatedPersonDialect;
import nl.tudelft.queue.dialect.ProfileDialect;
import nl.tudelft.queue.repository.ProfileRepository;

@Configuration
public class ThymeleafConfig {

	@Bean
	public AuthenticatedPersonDialect authenticatedPersonDialect() {
		return new AuthenticatedPersonDialect();
	}

	@Bean
	public ProfileDialect profileDialect(ProfileRepository profileRepository) {
		return new ProfileDialect(profileRepository);
	}

	/**
	 * Checks whether today is the day.
	 *
	 * @return {@code true} when today is the day, {@code false} if today is not the day.
	 */
	public boolean isTheDay() {
		return DateTimeFormatter.ofPattern("dd-MM").format(LocalDateTime.now()).equals("01-04");
	}

	@Bean
	public UriDialect uriDialect() {
		return new UriDialect();
	}

	public static class UriDialect implements IExpressionObjectDialect {
		@Override
		public IExpressionObjectFactory getExpressionObjectFactory() {
			return new UriExpressionFactory();
		}

		@Override
		public String getName() {
			return "Uri";
		}
	}

	public static class UriExpressionFactory implements IExpressionObjectFactory {
		@Override
		public Set<String> getAllExpressionObjectNames() {
			return Set.of("uri");
		}

		@Override
		public Object buildObject(IExpressionContext context, String expressionObjectName) {
			RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
			if (Objects.equals("uri", expressionObjectName)
					&& attributes instanceof ServletRequestAttributes attr) {
				return attr.getRequest().getRequestURI();
			}
			return null;
		}

		@Override
		public boolean isCacheable(String expressionObjectName) {
			return false;
		}
	}

	@Bean
	public SessionDialect sessionDialect() {
		return new SessionDialect();
	}

	public static class SessionDialect implements IExpressionObjectDialect {
		@Override
		public IExpressionObjectFactory getExpressionObjectFactory() {
			return new SessionExpressionFactory();
		}

		@Override
		public String getName() {
			return "Session";
		}
	}

	public static class SessionExpressionFactory implements IExpressionObjectFactory {
		@Override
		public Set<String> getAllExpressionObjectNames() {
			return Set.of("maxInactiveInterval");
		}

		@Override
		public Object buildObject(IExpressionContext context, String expressionObjectName) {
			RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
			if (Objects.equals("maxInactiveInterval", expressionObjectName)
					&& attributes instanceof ServletRequestAttributes attr) {
				return attr.getRequest().getSession().getMaxInactiveInterval();
			}
			return null;
		}

		@Override
		public boolean isCacheable(String expressionObjectName) {
			return false;
		}
	}
}
