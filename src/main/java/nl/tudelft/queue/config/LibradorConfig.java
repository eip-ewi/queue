/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.config;

import static org.modelmapper.convention.MatchingStrategies.STRICT;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;

import nl.tudelft.librador.EnableLibrador;
import nl.tudelft.librador.LibradorConfigAdapter;
import nl.tudelft.librador.dto.id.IdMapperBuilder;
import nl.tudelft.queue.dto.id.*;
import nl.tudelft.queue.model.*;
import nl.tudelft.queue.model.labs.*;
import nl.tudelft.queue.model.misc.Announcement;

@EnableLibrador
@Configuration
public class LibradorConfig extends LibradorConfigAdapter {

	@Autowired
	private ApplicationContext applicationContext;

	/**
	 * Bean for the {@link IdMapperBuilder} that is used by Librador to create proper conversions within the
	 * used {@link org.modelmapper.ModelMapper}.
	 *
	 * @return The {@link IdMapperBuilder} with all ID DTOs registered.
	 */

	@Override
	public ModelMapper modelMapper(ApplicationContext applicationContext) {
		ModelMapper modelMapper = super.modelMapper(applicationContext);
		modelMapper.getConfiguration().setMatchingStrategy(STRICT);

		return modelMapper;
	}

	@Override
	protected void configure(IdMapperBuilder builder) {
		builder.register(QueueSessionIdDTO.class,
				TypeToken.<QueueSession<?>>of(QueueSession.class).getRawType());
		builder.register(LabIdDTO.class, Lab.class);
		builder.register(CapacitySessionIdDTO.class, CapacitySession.class);
		builder.register(RegularLabIdDTO.class, RegularLab.class);
		builder.register(SlottedLabIdDTO.class, SlottedLab.class);
		builder.register(ExamLabIdDTO.class, ExamLab.class);

		builder.register(RequestIdDTO.class, TypeToken.<Request<?>>of(Request.class).getRawType());
		builder.register(SelectionRequestIdDTO.class, SelectionRequest.class);
		builder.register(LabRequestIdDTO.class, LabRequest.class);

		builder.register(ClosableTimeSlotIdDTO.class, ClosableTimeSlot.class);
		builder.register(TimeSlotIdDTO.class, TimeSlot.class);

		builder.register(AnnouncementIdDTO.class, Announcement.class);
	}
}
