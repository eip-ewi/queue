/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@ConfigurationProperties(prefix = "queue")
public class QueueProperties {
	/**
	 * The url under which Queue is currently deployed and to use when compiling links for external users.
	 */
	@NotNull
	private String deployUrl;

	/**
	 * The url to which websocket requests are to be made.
	 */
	@NotNull
	private String websocketUrl;

	/**
	 * The directory in which to store files temporarily.
	 */
	@NotNull
	private String tempFileDirectory = "/tmp/queue";

	/**
	 * The path in which content can be stored such that they can be accessed from outside. For dev purposes,
	 * this will automatically store it in resource folder. For deployment this should be overwritten in the
	 * application.disabled.yml file.
	 */
	@NotNull
	private String staticallyServedPath = "src/main/resources";

	@NotNull
	private String codeOfConduct = "classpath:/CodeofConduct.md";
}
