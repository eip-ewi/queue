/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.lib.security.user.AuthenticatedPerson;
import nl.tudelft.labracore.lib.security.user.Person;
import nl.tudelft.queue.dto.create.reporting.AcademicCounsellorReportDTO;
import nl.tudelft.queue.dto.create.reporting.ConfidentialAdvisorsReportDTO;
import nl.tudelft.queue.service.ReportService;

@Controller
@RequestMapping("/report")
@AllArgsConstructor
public class ReportController {

	private final ReportService reportService;

	@GetMapping("/{id}")
	public String getReportPage(@PathVariable Long id,
			@AuthenticatedPerson Person person, Model model) {
		reportService.getReportDetails(id, person, model);
		return "report/view";
	}

	@PostMapping("/submit/academic-counsellor")
	public String submitReport(AcademicCounsellorReportDTO dto,
			@AuthenticatedPerson Person person, Model model) {
		reportService.doReportConfidential(dto, person);
		return "redirect:/";
	}

	@PostMapping("/submit/confidential-advisor")
	public String submitReportConfidentialAdvisor(ConfidentialAdvisorsReportDTO dto,
			@AuthenticatedPerson Person person,
			Model model) {
		reportService.doReportConfidential(dto, person);
		return "redirect:/";
	}
}
