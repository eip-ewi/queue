/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.api.StudentGroupControllerApi;
import nl.tudelft.labracore.lib.security.user.AuthenticatedPerson;
import nl.tudelft.labracore.lib.security.user.Person;
import nl.tudelft.queue.cache.ModuleCacheManager;
import nl.tudelft.queue.cache.StudentGroupCacheManager;

@Controller
@AllArgsConstructor
@RequestMapping("group")
public class StudentGroupController {

	private ModuleCacheManager mCache;
	private StudentGroupCacheManager sgCache;
	private StudentGroupControllerApi sgApi;

	/**
	 * Joins the specified group.
	 *
	 * @param  id The id of the group to join
	 * @return    Redirects to edition page
	 */
	@PostMapping("{id}/join")
	@PreAuthorize("@permissionService.canJoinGroup(#id)")
	public String joinGroup(@AuthenticatedPerson Person person, @PathVariable Long id) {
		sgApi.addMemberToGroup(id, person.getId()).block();
		return "redirect:/edition/"
				+ mCache.getRequired(sgCache.getRequired(id).getModule().getId()).getEdition().getId();
	}

}
