/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import jakarta.transaction.Transactional;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.librador.resolver.annotations.PathEntity;
import nl.tudelft.queue.dto.create.misc.AnnouncementCreateDTO;
import nl.tudelft.queue.model.misc.Announcement;
import nl.tudelft.queue.repository.AnnouncementRepository;

@Controller
@RequestMapping("/admin")
@PreAuthorize("@permissionService.isAdmin()")
public class AnnouncementController {
	@Autowired
	private AnnouncementRepository ar;

	@Autowired
	private DTOConverter dtoConverter;

	/**
	 * Sets a model attribute statically within every Thymeleaf resolution. This model attribute is to
	 * indicate the main tab that admin pages are in.
	 *
	 * @return The name of the tab that these pages are on ("admin").
	 */
	@ModelAttribute("page")
	public static String page() {
		return "admin";
	}

	/**
	 *
	 * @param  model The model to fill out for Thymeleaf template resolution.
	 * @return       The Thymeleaf template to resolve.
	 */
	@GetMapping("/announcements")
	public String getEditableAnnouncementsPage(Model model) {
		model.addAttribute("announcements", ar.findEditableAnnouncements());

		return "admin/announcements";
	}

	/**
	 *
	 * @param  model The model to fill out for Thymeleaf template resolution.
	 * @return       The Thymeleaf template to resolve.
	 */
	@GetMapping("/announcement/create")
	public String getCreateAnnouncementPage(Model model) {
		model.addAttribute("dto", new AnnouncementCreateDTO());

		return "admin/create/announcement";
	}

	/**
	 * Creates an announcement based on information filled in in the AnnouncementCreateDTO passed to this
	 * method. After converting successfully, the announcement is added to the database and the user is
	 * redirected back to the announcements overview.
	 *
	 * @param  dto   The DTO used to create the announcement.
	 * @param  model The model to fill out for Thymeleaf template resolution.
	 * @return       The Thymeleaf template to resolve.
	 */
	@PostMapping("/announcement/create")
	public String createAnnouncement(AnnouncementCreateDTO dto,
			Model model) {
		Announcement announcement = dto.apply(dtoConverter);

		if (dto.hasErrors()) {
			model.addAttribute("dto", dto);
			return "admin/create/announcement";
		}

		ar.save(announcement);

		return "redirect:/admin/announcements";
	}

	/**
	 * Ends the given announcement at this moment. This makes the announcement unavailable for further editing
	 * and viewing.
	 *
	 * @param  announcement The announcement to make unavailable.
	 * @return              A redirect back to the announcements overview page.
	 */
	@Transactional
	@PostMapping("/announcement/{announcement}/end")
	public String endAnnouncement(@PathEntity Announcement announcement) {
		announcement.setEndTime(LocalDateTime.now().minusMinutes(1));

		return "redirect:/admin/announcements";
	}
}
