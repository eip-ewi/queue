/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import nl.tudelft.queue.dto.util.RequestTableFilterDTO;
import nl.tudelft.queue.service.RequestTableService;

@Controller
public class RequestFilterController {

	@Autowired
	private RequestTableService rts;

	/**
	 * Submits filters for a request table page with a return-to-page path.
	 *
	 * @param  filter             The DTO containing information about the filters that are to be submitted.
	 * @param  path               The path this method should redirect to after submitting filters.
	 * @param  redirectAttributes Attributes used to fill in a model message upon redirection.
	 * @return                    A redirect to the associated requests table page.
	 */
	@PostMapping(value = "/filter", params = "filter-submit")
	public String submitRequestTableFilters(RequestTableFilterDTO filter,
			@RequestParam("return-path") String path,
			RedirectAttributes redirectAttributes) {
		rts.checkAndStoreFilterDTO(filter, path);

		redirectAttributes.addAttribute("page", 0);

		return "redirect:" + path;
	}

	/**
	 * Clears filters for a request table under the given return-to-page path.
	 *
	 * @param  path               The path this method should redirect to after clearing filters.
	 * @param  redirectAttributes Attributes used to fill in a model message upon redirection.
	 * @return                    A redirect to the associated requests table page.
	 */
	@PostMapping(value = "/filter", params = "filter-clear")
	public String clearRequestTableFilters(@RequestParam("return-path") String path,
			RedirectAttributes redirectAttributes) {
		rts.clearFilter(path);

		redirectAttributes.addAttribute("page", 0);

		return "redirect:" + path;
	}
}
