/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import nl.tudelft.labracore.lib.security.user.AuthenticatedPerson;
import nl.tudelft.labracore.lib.security.user.Person;
import nl.tudelft.queue.dto.create.CourseRequestCreateDTO;
import nl.tudelft.queue.service.mail.MailService;

@Controller
@ConditionalOnProperty(name = "queue.mail.enabled", havingValue = "true")
public class MailController {
	@Autowired
	private MailService ms;

	/**
	 * Sends an e-mail to the administrators of Queue to request a course using information given by the
	 * teacher requesting the course to be made.
	 *
	 * @param  person The person to request a new course to be made.
	 * @param  dto    The DTO with details about the course the user filled out.
	 * @return        The Thymeleaf template to resolve.
	 */
	@PostMapping("/editions/request-course")
	@PreAuthorize("@permissionService.isAdminOrTeacher()")
	public String requestCourse(@AuthenticatedPerson Person person,
			CourseRequestCreateDTO dto) {
		dto.validateOrThrow();
		ms.sendCourseRequest(person, dto);

		return "course/request-submitted";
	}
}
