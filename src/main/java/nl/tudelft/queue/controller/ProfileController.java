/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import java.time.LocalDateTime;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.lib.security.user.AuthenticatedPerson;
import nl.tudelft.labracore.lib.security.user.Person;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.queue.dto.patch.ProfilePatchDTO;
import nl.tudelft.queue.dto.view.CodeOfConductDTO;
import nl.tudelft.queue.repository.ProfileRepository;
import nl.tudelft.queue.service.CodeOfConductService;

@AllArgsConstructor
@RequestMapping("profile")
@Controller("queueProfileController")
public class ProfileController {

	private ProfileRepository profileRepository;

	private CodeOfConductService codeOfConductService;

	private DTOConverter converter;

	/**
	 * Updates the user's profile.
	 *
	 * @param  person The person whose profile to update
	 * @param  patch  The patch with new data
	 * @return        Empty http response
	 */
	@ResponseBody
	@PostMapping("update")
	public ResponseEntity<Void> updateProfile(@AuthenticatedPerson Person person,
			@RequestBody ProfilePatchDTO patch) {
		profileRepository.save(patch.apply(profileRepository.findProfileForPerson(person), converter));
		return ResponseEntity.ok().build();
	}

	/**
	 * Get the code of conduct information for the specific person. This allows the front-end to check the
	 * last time the code of conduct was read and display it if needed.
	 *
	 * @param  person                  The person fow whom to get the Code of Conduct information.
	 * @return
	 * @throws JsonProcessingException
	 */
	@GetMapping(value = "code-of-conduct", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> doesUserNeedToReadCoc(@AuthenticatedPerson Person person)
			throws JsonProcessingException {
		CodeOfConductDTO dto = codeOfConductService.getCodeOfConduct(person);
		return ResponseEntity.ok(dto.toJsonValue());
	}

	/**
	 * Update the latest read variable for a person regarding their code of conduct to the current date/time.
	 * This allows for keeping track on when the user last read the code of conduct.
	 *
	 * @param  person The person for which to update
	 * @return
	 */
	@PostMapping(value = "code-of-conduct/update")
	@ResponseBody
	public ResponseEntity<Void> updateLatestReadCoC(@AuthenticatedPerson Person person) {
		codeOfConductService.updateLatestCoC(person, LocalDateTime.now());
		return ResponseEntity.ok().build();
	}
}
