/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import nl.tudelft.queue.service.AdminService;

@Controller
public class RoomController {

	@Autowired
	private AdminService adminService;

	/**
	 * Gets the image file for a specific room.
	 *
	 * @param  id The id of the room
	 * @return    A resource with the image
	 */
	@PreAuthorize("@permissionService.isAuthenticated()")
	@GetMapping("/room/map/{id}")
	public ResponseEntity<String> getRoomImageFile(@PathVariable("id") Long id) {
		String fileName = adminService.getRoomFileName(id);
		if (fileName == null)
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		return ResponseEntity.ok("{\"fileName\": \"" + fileName + "\"}");
	}
}
