/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service;

import java.util.List;

import org.springframework.stereotype.Service;

import nl.tudelft.labracore.api.dto.AssignmentSummaryDTO;
import nl.tudelft.labracore.api.dto.ModuleDetailsDTO;

@Service
public class ModuleDTOService {

	/**
	 * Gets assignments with respect to a certain edition within several modules.
	 *
	 * @param  dto       The list of modules to consider.
	 * @param  editionId The edition to consider.
	 * @return           The assignments that belong to that edition within the modules.
	 */
	public List<AssignmentSummaryDTO> getAssignmentsInEdition(List<ModuleDetailsDTO> dto, Long editionId) {
		return dto.stream().filter(module -> module.getEdition().getId().equals(editionId))
				.flatMap(module -> module.getAssignments().stream())
				.collect(java.util.stream.Collectors.toList());
	}

}
