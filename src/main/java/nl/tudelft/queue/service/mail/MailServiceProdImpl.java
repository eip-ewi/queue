/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service.mail;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.mail.MailSender;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.lib.security.user.Person;
import nl.tudelft.queue.dto.create.CourseRequestCreateDTO;
import nl.tudelft.queue.dto.create.reporting.ReportDTO;
import nl.tudelft.queue.properties.MailProperties;
import nl.tudelft.queue.properties.QueueProperties;

@Service
@ConditionalOnProperty(name = "queue.mail.enabled", havingValue = "true")
@AllArgsConstructor
public class MailServiceProdImpl extends MailService {

	private QueueProperties qp;
	private MailProperties mp;
	private MailSender mailSender;

	/**
	 * Sends an e-mail to the Queue administration e-mail to request a new course.
	 *
	 * @param person The person that is requesting the new course.
	 * @param dto    The DTO containing the request for the new course.
	 */
	public void sendCourseRequest(Person person, CourseRequestCreateDTO dto) {
		var msg = createCourseRequestMail(person, dto, qp.getDeployUrl().replaceAll("^(.+)/$", "\1"));
		msg.setTo(mp.getSupportEmail());
		msg.setFrom(mp.getOriginEmail());
		msg.setReplyTo(person.getEmail());

		mailSender.send(msg);
	}

	@Override
	public void sendReport(ReportDTO dto, Person person) {
		var msg = createReportMail(dto, person);
		msg.setFrom(mp.getOriginEmail());
		msg.setReplyTo(person.getEmail());

		mailSender.send(msg);
	}
}
