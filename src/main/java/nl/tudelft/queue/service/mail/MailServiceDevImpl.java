/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service.mail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import nl.tudelft.labracore.lib.security.user.Person;
import nl.tudelft.queue.dto.create.CourseRequestCreateDTO;
import nl.tudelft.queue.dto.create.reporting.ReportDTO;

@Service
@ConditionalOnProperty(name = "queue.mail.enabled", havingValue = "false")
public class MailServiceDevImpl extends MailService {
	private static final Logger logger = LoggerFactory.getLogger(MailServiceDevImpl.class);

	@Override
	public void sendCourseRequest(Person person, CourseRequestCreateDTO dto) {
		var msg = createCourseRequestMail(person, dto, "localhost:8081/");
		logger.info(msg.toString());
	}

	@Override
	public void sendReport(ReportDTO dto, Person reporter) {
		var msg = createReportMail(dto, reporter);

		logger.info(msg.toString());
	}
}
