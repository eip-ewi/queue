/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import nl.tudelft.queue.cache.PersonCacheManager;
import nl.tudelft.queue.model.LabRequest;
import nl.tudelft.queue.properties.JitsiProperties;

/**
 * A service for dealing with with Jitsi integration.
 */
@Service
public class JitsiService {
	@Autowired
	private PersonCacheManager pCache;

	@Autowired
	private JitsiProperties properties;

	/**
	 * Creates and returns the room name for the given request. Room names start with the username of the
	 * requesting party and end with part of a randomly generated UUID (url-safe).
	 *
	 * @param  request The request for which we need to create a Jitsi room name.
	 * @return         The created room name.
	 */
	public String createJitsiRoomName(LabRequest request) {
		// Create a random UUID to add to the name
		UUID uuid = UUID.randomUUID();

		// Lookup the username of the requester and add it to the UUID to make a room name
		return pCache.getRequired(request.getRequester()).getUsername() + "-" +
				uuid.toString().substring(0, 8);
	}

	/**
	 * Returns the Jitsi room URL for a specific request.
	 *
	 * @param  request The request to get the jitsi URL for.
	 * @return         the URL to a Jitsi room linked with this request
	 */
	public String getJitsiRoomUrl(LabRequest request) {
		if (request.getJitsiRoom() != null) {
			return properties.getUrl() + "/" + request.getJitsiRoom();
		} else {
			return null;
		}
	}
}
