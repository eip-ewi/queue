/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service;

import java.util.ArrayList;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import nl.tudelft.labracore.api.dto.ClusterDetailsDTO;
import nl.tudelft.queue.cache.ClusterCacheManager;
import nl.tudelft.queue.model.LabRequestConstraint;
import nl.tudelft.queue.model.constraints.ClusterConstraint;
import nl.tudelft.queue.model.constraints.ModuleDivisionConstraint;

@Service
@AllArgsConstructor
public class ConstraintService {

	public final ClusterCacheManager clusterCacheManager;

	public String constraintDescription(LabRequestConstraint constraint) {
		if (constraint instanceof ModuleDivisionConstraint) {
			return constraintDescription((ModuleDivisionConstraint) constraint);
		} else if (constraint instanceof ClusterConstraint) {
			return constraintDescription((ClusterConstraint) constraint);
		}
		throw new IllegalArgumentException(
				"Invalid constraint type: " + constraint.getClass().getSimpleName());
	}

	public String constraintDescription(ClusterConstraint constraint) {
		return "in cluster " +
				clusterCacheManager.getAndIgnoreMissing(new ArrayList<>(constraint.getClusters()))
						.stream().map(ClusterDetailsDTO::getName).collect(Collectors.joining(", "));
	}

	public String constraintDescription(ModuleDivisionConstraint constraint) {
		return "in division "
				+ constraint.getDivisions().stream().map(div -> "#" + div).collect(Collectors.joining(", "));
	}
}
