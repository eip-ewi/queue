/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service.dto;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.queue.cache.PersonCacheManager;
import nl.tudelft.queue.dto.view.RequestEventViewDTO;
import nl.tudelft.queue.dto.view.events.EventWithAssistantView;
import nl.tudelft.queue.model.Request;
import nl.tudelft.queue.model.RequestEvent;

@Service
@AllArgsConstructor
public class RequestEventViewDTOService {

	private final PersonCacheManager personCache;

	private final DTOConverter converter;

	@SuppressWarnings("unchecked")
	public <R extends Request<?>, E extends RequestEvent<R>> RequestEventViewDTO<E> convert(E event) {
		RequestEventViewDTO<E> view = converter.convert(event,
				(Class<RequestEventViewDTO<E>>) event.viewClass());
		if (view instanceof EventWithAssistantView withAssistant) {
			withAssistant.setAssistant(personCache.getRequired(withAssistant.getAssistantId()));
		}
		return view;
	}

}
