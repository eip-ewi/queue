/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.service.dto;

import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.queue.dto.view.PresentationViewDTO;
import nl.tudelft.queue.model.misc.Presentation;

@Service
@AllArgsConstructor
public class PresentationViewDTOService {

	private final Parser parser;
	private final HtmlRenderer renderer;

	private final DTOConverter converter;

	public PresentationViewDTO convert(Presentation presentation) {
		PresentationViewDTO view = converter.convert(presentation, PresentationViewDTO.class);

		view.getCustomSlides().forEach(slide -> {
			Node document = parser.parse(slide.getContent() == null ? "" : slide.getContent());
			slide.setContent(renderer.render(document));
		});

		return view;
	}

}
