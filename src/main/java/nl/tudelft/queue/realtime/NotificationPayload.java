/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.realtime;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NotificationPayload {
	/**
	 * The title the notification will have.
	 */
	private String title;

	/**
	 * The body of the notification in text.
	 */
	private String body;

	/**
	 * The icon shown when the notification pops up.
	 */
	private String icon;

	/**
	 * The tag to give the notification for scripts to filter out the notification.
	 */
	private String tag;

	/**
	 * The url to which to send the user when clicking the notification.
	 */
	private String url;

	/**
	 * Converts this payload into a JSON String. This JSON String can then function as the payload for the
	 * notification sent to the user.
	 *
	 * @return A JSON String representing this payload object.
	 */
	@SneakyThrows
	public String toJson() {
		return new ObjectMapper().writeValueAsString(this);
	}
}
