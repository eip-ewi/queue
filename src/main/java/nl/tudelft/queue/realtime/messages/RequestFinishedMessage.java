/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.realtime.messages;

import lombok.*;
import nl.tudelft.queue.model.RequestEvent;
import nl.tudelft.queue.model.enums.RequestStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RequestFinishedMessage extends RequestStatusUpdateMessage<RequestEvent<?>> {
	private static final long serialVersionUID = -4937850360309831244L;

	private RequestStatus status;

	@Override
	public String getType() {
		return "request-finished";
	}

	@Override
	public RequestStatus getStatus() {
		return status;
	}

	public RequestFinishedMessage withStatus(RequestStatus status) {
		this.status = status;
		return this;
	}
}
