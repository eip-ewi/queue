/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.realtime.messages;

import lombok.*;
import nl.tudelft.librador.dto.DTOConverter;
import nl.tudelft.queue.model.enums.OnlineMode;
import nl.tudelft.queue.model.enums.RequestStatus;
import nl.tudelft.queue.model.enums.RequestType;
import nl.tudelft.queue.model.events.RequestForwardedToPersonEvent;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class RequestForwardedToPersonMessage extends RequestStatusUpdateMessage<RequestForwardedToPersonEvent>
		implements LabRequestMessage {
	private static final long serialVersionUID = 2074225878780704840L;

	private Long forwardedTo;

	private String requestedBy;

	private String organizationName;

	private Long roomId;
	private String roomName;
	private Long buildingId;
	private String buildingName;

	private OnlineMode onlineMode;
	private String onlineModeDisplayName;

	private Long assignmentId;
	private String assignmentName;
	private Long moduleId;
	private String moduleName;

	private RequestType requestType;
	private String requestTypeDisplayName;

	@Override
	public RequestStatus getStatus() {
		return RequestStatus.FORWARDED;
	}

	@Override
	public String getType() {
		return "request-forwarded-to-person";
	}

	@Override
	public void postApply(DTOConverter converter) {
		super.postApply();
		forwardedTo = data.getForwardedTo();
	}
}
