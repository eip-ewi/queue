/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.realtime.messages;

import nl.tudelft.queue.model.enums.OnlineMode;
import nl.tudelft.queue.model.enums.RequestType;

public interface LabRequestMessage extends Message {

	void setOrganizationName(String organizationName);

	void setRoomId(Long roomId);

	void setRoomName(String roomName);

	void setBuildingId(Long buildingId);

	void setBuildingName(String buildingName);

	void setOnlineMode(OnlineMode onlineMode);

	void setOnlineModeDisplayName(String onlineModeDisplayName);

	void setAssignmentId(Long assignmentId);

	void setAssignmentName(String assignmentName);

	void setModuleId(Long moduleId);

	void setModuleName(String moduleName);

	void setLabId(Long labId);

	void setRequestType(RequestType requestType);

	void setRequestTypeDisplayName(String requestTypeDisplayName);

	void setRequestedBy(String requestedBy);

}
