/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.realtime.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import nl.tudelft.librador.dto.view.View;
import nl.tudelft.queue.model.RequestEvent;
import nl.tudelft.queue.model.enums.RequestStatus;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public abstract class RequestStatusUpdateMessage<T extends RequestEvent<?>> extends View<T>
		implements Message {
	private static final long serialVersionUID = -5353828943219849816L;

	private Long id;
	private Long labId;

	public abstract RequestStatus getStatus();

	@Override
	public void postApply() {
		id = data.getRequest().getId();
		labId = data.getRequest().getSession().getId();
	}
}
