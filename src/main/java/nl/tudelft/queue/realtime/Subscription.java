/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.realtime;

import java.io.Serializable;
import java.time.LocalDateTime;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The subscription object that is to be stored in the session store (possibly multiple per user).
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Subscription implements Serializable {
	/**
	 * The Person this Subscription is owned by.
	 */
	@NotNull
	private Long person;

	/**
	 * The endpoint path to which this subscription writes.
	 */
	@NotNull
	private String endpoint;

	/**
	 * The key used for accessing the subscription endpoint.
	 */
	@NotNull
	private String p256dhKey;

	/**
	 * The authorization field of the Subscription.
	 */
	@NotNull
	private String auth;

	/**
	 * The last time this specific Subscription was used.
	 */
	@NotNull
	private LocalDateTime lastUsed;
}
