/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.misc;

import java.time.LocalDateTime;
import java.util.Collections;

import nl.tudelft.labracore.api.dto.AssignmentDetailsDTO;
import nl.tudelft.labracore.api.dto.Description;
import nl.tudelft.labracore.api.dto.ModuleDivisionSummaryDTO;
import nl.tudelft.labracore.api.dto.ModuleSummaryDTO;
import nl.tudelft.labracore.api.dto.StudentGroupDetailsDTO;

/**
 * Quick and dirty fix until further research is done w.r.t API interactions between services.
 */
public class DeletedObjectUtils {

	private static ModuleSummaryDTO moduleSummaryDto() {
		return new ModuleSummaryDTO()
				.id(-1L)
				.name("Deleted Module");
	}

	private static ModuleDivisionSummaryDTO moduleDivisionSummaryDTO() {
		return new ModuleDivisionSummaryDTO()
				.id(-1L);
	}

	public static AssignmentDetailsDTO assignmentDetailsDTO() {
		return new AssignmentDetailsDTO()
				.id(-1L)
				.name("Deleted Assignment")
				.description(
						new Description()
								.text("Unknown information")
								.fileDirectory("Unknown information"))
				.deadline(LocalDateTime.MIN)
				.lateSubmissionLeniency(-1L)
				.acceptLateSubmissions(false)
				.approveScript("Unknown information")
				.submissionLimit(-1)
				.allowedFileTypes(Collections.emptySet())
				.submissions(Collections.emptyList())
				.sessions(Collections.emptyList())
				.module(moduleSummaryDto());
	}

	public static StudentGroupDetailsDTO studentGroupDetailsDTO() {
		return new StudentGroupDetailsDTO()
				.id(-1L)
				.name("Deleted Student (Group)")
				.capacity(-1)
				.division(moduleDivisionSummaryDTO())
				.memberUsernames(Collections.emptyList())
				.assignmentsSubmitted(Collections.emptyMap())
				.module(moduleSummaryDto())
				.members(Collections.emptyList())
				.submissions(Collections.emptyList());
	}
}
