/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.misc;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum QueueSessionStatus {
	OPEN(
		true, "Open"
	),
	SLOT_SELECTION_CLOSED(
		false, "Slot selection not open yet"
	),
	FINISHED_SESSION(
		false, "Session finished"
	),
	INACTIVE_SESSION(
		false, "Session not started yet"
	),
	ENROLMENT_CLOSED(
		false, "Enrolment closed"
	),
	GENERIC_CLOSED(
		false, "Closed"
	);

	/**
	 * Whether the lab is currently open to enqueueing.
	 */
	private final boolean isOpenToEnqueueing;

	/**
	 * What to display on a badge reflecting the status of this lab.
	 */
	private final String badgeLabel;
}
