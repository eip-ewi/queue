/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.security;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import nl.tudelft.labracore.api.PersonControllerApi;
import nl.tudelft.labracore.lib.security.memory.InMemoryUserProvider;
import nl.tudelft.labracore.lib.security.user.Person;

@Service
@Profile("!production")
public class DevUserDetailsProvider extends InMemoryUserProvider {

	@Autowired
	private PersonControllerApi pApi;

	@Override
	@Transactional
	public Person findByUsername(String username) {
		if (username.contains("@")) {
			username = username.substring(0, username.indexOf('@'));
		}

		return pApi.getPersonByUsername(username)
				.map(person -> new ModelMapper().map(person, Person.class))
				.doOnError(e -> {
					throw new UsernameNotFoundException("Unable to find user for given username");
				})
				.block();
	}

	@Override
	public String getPasswordFor(Person person) {
		return new BCryptPasswordEncoder().encode(person.getUsername());
	}
}
