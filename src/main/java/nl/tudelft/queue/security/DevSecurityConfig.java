/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer;

import nl.tudelft.labracore.lib.security.LabradorSecurityConfig;

@Configuration
@Profile("!production")
public class DevSecurityConfig extends LabradorSecurityConfig {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		super.configure(http);

		http.authorizeHttpRequests(auth -> auth
				.requestMatchers("/",
						"/present/**",
						"/manifest.json",
						"/favicon.ico",
						"/sw.js",
						"/css/**",
						"/font/**",
						"/sass/**",
						"/img/**",
						"/js/**",
						"/webjars/**",
						"/stomp/**",
						"/lab/submit*",
						"/privacy",
						"/about",
						"/support",
						"/h2-console/**")
				.permitAll()
				.anyRequest().authenticated());
		http.csrf(csrf -> csrf.ignoringRequestMatchers("/h2-console/**"));
		http.headers(h -> h.frameOptions(HeadersConfigurer.FrameOptionsConfig::disable));
	}

}
