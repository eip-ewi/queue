/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.csv;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import lombok.Getter;
import nl.tudelft.labracore.api.dto.RoleCreateDTO;

@Getter
public class UserCsvHelper extends RoleCreateDTO {

	private String netId;

	private static final CsvSchema schema = CsvSchema.builder()
			.setSkipFirstDataRow(true)
			.addColumn("netId")
			.addColumn("role")
			.setColumnSeparator(',')
			.build();

	@JsonCreator
	public UserCsvHelper(@JsonProperty("netId") String netId, @JsonProperty("role") String role)
			throws IllegalArgumentException, InvalidCsvException {
		super();
		TypeEnum type = TypeEnum.fromValue(role.replace(" ", "").replace("\"", ""));
		if (type.equals(TypeEnum.ADMIN) || type.equals(TypeEnum.BLOCKED)) {
			throw new InvalidCsvException("Admins or blocked people cannot be added this way.");
		}
		super.type(type);
		this.netId = netId.replace(" ", "").replace("\"", "");
	}

	/**
	 * Convert csv file to a lis to of {@link UserCsvHelper} objects.
	 *
	 * @param  csv                 The file to be converted.
	 * @return                     A list of {@link UserCsvHelper} containing the information from the csv
	 *                             file including netId and new role.
	 * @throws EmptyCsvException   Thrown when csv is empty.
	 * @throws InvalidCsvException Thrown when csv cannot be parsed.
	 */
	public static List<UserCsvHelper> readCsv(MultipartFile csv) throws EmptyCsvException,
			InvalidCsvException {
		return CsvHelper.readCsv(csv, UserCsvHelper.class, schema);
	}
}
