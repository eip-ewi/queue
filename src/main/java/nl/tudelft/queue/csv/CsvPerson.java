/*
 * Queue - A Queueing system that can be used to handle labs in higher education
 * Copyright (C) 2016-2024  Delft University of Technology
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package nl.tudelft.queue.csv;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import lombok.Getter;

@Getter
public class CsvPerson {

	private static final CsvSchema schema = CsvSchema.builder().setSkipFirstDataRow(true)
			.addColumn("username").build();

	private String username;

	@JsonCreator
	public CsvPerson(@JsonProperty("username") String username) {
		this.username = username;
	}

	public static List<CsvPerson> readCsv(MultipartFile csv) throws EmptyCsvException, InvalidCsvException {
		return CsvHelper.readCsv(csv, CsvPerson.class, schema);
	}
}
