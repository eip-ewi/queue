import gitlab
import os

gl = gitlab.Gitlab(os.environ['CI_SERVER_URL'], private_token=os.environ['CI_JOB_TOKEN'])
project = gl.projects.get(os.environ['CI_PROJECT_ID'])

file = open('./build/test-results/e2eTest/TEST-e2e.RegularLabRequestWorkflowTest.xml', 'r')
descript_text = (f'Pipeline: {os.environ["CI_PIPELINE_URL"]} has failed \n '
                 f'```xml\n'
                 f'{file.read()}'
                 '\n```')

issue_details = {
    'title': f'e2e tests failed',
    'description': descript_text,
    'assignees': f'[{os.environ["GITLAB_USER_ID"]}]',
    'labels': '[type::bug, prio::critical]'
}
issue = project.issues.create(issue_details)
