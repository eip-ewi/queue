#!/bin/sh

trap "echo 'Stopping Queue...'; exit" HUP INT QUIT TERM

for file in /var/www/queue/mysql/*
do
  mysql -h database -u root queue < "$file"
done
echo "DROP DATABASE queue; CREATE DATABASE queue;" | mysql -h database -u root queue

java -cp . -jar queue.jar >> /var/log/queue.log 2>>/var/log/queue.err
tail -f /var/log/queue.log

exit 0