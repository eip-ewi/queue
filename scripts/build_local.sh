#!/bin/sh

mv src/main/resources/application.yml src/main/resources/application.disabled.yml
./gradlew clean
./gradlew assemble
mv src/main/resources/application.disabled.yml src/main/resources/application.yml
