FROM gradle:jdk21

RUN apt update && apt install mysql-client -y

EXPOSE 8081

RUN mkdir /var/www
RUN mkdir /var/www/queue

WORKDIR /var/www/queue

COPY build/libs/queue-*.jar queue.jar
COPY src/main/resources/application.docker.yml application.yml
COPY scripts/docker_entrypoint.sh docker_entrypoint

ENTRYPOINT ./docker_entrypoint