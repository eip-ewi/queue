#!/bin/sh
#
# Put this script inside the root folder of your git directory before running it.

if cat >.git/hooks/commit-msg<<'EOF'
#!/bin/sh
grep "^Signed-off-by:" "$1" || {
	echo >>"$1"
	echo "Signed-off-by: $(git config user.name) <$(git config user.email)>" >>"$1"
}
EOF
then
	chmod +x .git/hooks/commit-msg || {
		echo "Installation failed: failed to adjust permissions"
		exit 1
	}
	echo "Installed git commit hook successfully"
else
	echo "Installation failed: could not write the commit hook"
fi
