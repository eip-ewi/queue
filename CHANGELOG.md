[comment]: <> (Added = New features)
[comment]: <> (Changed = Changes in existing functionality)
[comment]: <> (Deprecated = once-stable features removed in future releases "next release")
[comment]: <> (Removed = Deprecated features removed in this release "this release")
[comment]: <> (Fixed = Bug fixes)
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
 - Can now search for people using different identifiers such as username, name, email, or student number.[@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)
 - An overview of people who match the identifier, allowing you to see their details before adding them to the edition. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)
 - Allow users to report unnapropariate behavior. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
 - A button and page to edit assignments directly in Queue. [@rgiedryte](https://gitlab.ewi.tudelft.nl/rgiedryte)
 - A support page for detailed information on how to get help for Queue. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)

### Changed

 - Make the table in the lab stats page sortable [@MrHug](https://gitlab.ewi.tudelft.nl/MrHug)

### Fixed

 - The correct email address and up-to-date information on the error pages were added. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)
 - The length of the feedback text now takes into account carriage returns. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)

## [2.3.2]

### Added
- A requests-over-time graph has been added to the session statistics page. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Enqueueing for a slot can now be restricted, such that students cannot enqueue after a time slot has started. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Given feedback can now be flagged as inappropriate. This will remove the feedback item from your feedback page and submit it to be reviewed. @cedricwilleken
- Greet users with our code of conduct once every academic year. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Course staff can now revoke requests. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)

### Changed
- Deleting a session no longer deletes the feedback in that session.
- Slotted sessions no longer have any restrictions on the exact distribution of their time slots.
  This means that slotted sessions can now span multiple days, can have breaks with no slots in between blocks of slots, and can have overlapping slots.

### Fixed
- Deleted information would stop certain pages from rendering. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- The claim request button would appear when a request was already claimed. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- CTRL + click on a request in the session view did not work. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)

## [2.3.1]

### Added 
- Use ChihuahUI implementation for markdown editors. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)

### Changed

### Fixed
-  Minor hotfixes.
-  fix dialog; this caused several pages to not render correctly, including the lab page for enqueueing. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
-  fix lab statistics page.  [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
-  fix feedback star rating. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

## [2.3.0]

### Added
- In the status page, the table with TAs and requests now also shows average rating per TA for the selected sessions.
- A lab statistics page can now be accessed from the main lab page. This is currently a work in progress. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- A table showing request per ta and time since last interaction was added in the lab statistics page. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- A few general statistics was added in the lab statistics page. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- A bar-graph detailing the assignment breakdown between questions and submissions was added in the lab statistics page. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Use ChihuahUI implementation for markdown editors. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Slotted requests show up in request table without needing to refresh the page [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Use ChihuahUI implementation for markdown editors. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Enqueueing for a slot can now be restricted after a slot starts. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Allow feedback to be flagged as inappropriate

### Changed
- Written feedback sorting now follows a reverse chronological order. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Users will see a dialog instead of a new page when promopted to unenrol from an edition. [@hpage](https://gitlab.ewi.tudelft.nl/hpage) 
- Text used to indicate the number of times a lab is rescheduled is now more intuitive. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Assistants who couldn't locate students are no longer eligible to receive feedback from those students. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Enhanced room map overview for better clarity on room map availability. Additionally, changed the ordering of rooms to be lexographical [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Ctrl-click in request table now opens new tab with request. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Creating a request for a lab that has an online mode gives the option to select the online mode or the in-person mode. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)

### Fixed
- Custom slides are now rendered correctly with markdown in a session presentation. [@hpage](https://gitlab.ewi.tudelft.nl/hpage) 
- The request history page can now be seen for participants. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)
- Teachers can now view their own feedback again. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- The feedback graph for assistants now incorporates feedback from other courses, with written feedback limited to visibility for course managers. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- The button that lets users unenrol from an edition now disabled when appropriate. [@hpage](https://gitlab.ewi.tudelft.nl/hpage) 
- Students in the same group can no longer enqueue for the same lab again. [@gvergieva](https://gitlab.ewi.tudelft.nl/gvergieva)
- The request history page can now be seen for participants. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)
- Teachers can now view their own feedback again. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- The feedback graph for assistants now incorporates feedback from other courses, with written feedback limited to visibility for course managers. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- The button that lets users unenrol from an edition now disabled when appropriate. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- The assignment count graph on the edition statistic page was fixed. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Limited capacity sessions were not shown on the home page [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- Limited capacity sessions can now have extra information. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Limited capacity sessions were not shown on the home page
- Uploading a new room map will correctly replace the old one. [@hpage](https://gitlab.ewi.tudelft.nl/hpage) 
- Minor hotfixes.
- fix dialog; this caused several pages to not render correctly, including the lab page for enqueueing. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- fix lab statistics page.  [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- fix feedback star rating. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- Feedbacks are now soft deleted correctly. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Teachers that are the only teacher for an edition now can't leave the edition. [@aturgut](https://gitlab.ewi.tudelft.nl/aturgut)
- Creating a new edition (collection) does not allow for empty title. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)

## [2.2.2]

### Added

### Changed

### Fixed
- The about page was not accessible. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- When copying a session, the rooms would not get copied over properly. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- Teachers were able to demote themselves in their edition. [@aturgut](https://gitlab.ewi.tudelft.nl/aturgut)

## [2.2.1]

### Added

- Requests in slotted labs can now be distributed to assistants in advanced. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)

### Changed
- Teachers can only see assistant feedback for courses that they manage. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Slotted requests no longer show up 15 minutes in advance on the requests page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Fixed
- Teachers can no longer see feedback of other teachers. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Screens should no longer sleep when presenting (not supported on Firefox). [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- Lab filter for shared labs now shows the correct course codes in front of assignments. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Chips are no longer cut off on smaller devices. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Presentation text was invisible on dark mode. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- Forwarded requests did not always show up or showed up for the wrong people in the requests table. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- All requests would be added to the request table regardless of language preference. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- Slotted lab page did not display requests. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

## [2.2.0]

### Added
 - Editions can now be archived by teachers. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)
 - Labs can now be 'bilingual'. This allows students to indicate they wish to do their request only in Dutch or in Dutch or English. They will then get matched with a TA that speaks a matching language. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Students now get a notification when they are in front of the queue. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Labs now have a customisable presentation. This can be accessed by everyone with the present url to be presented on the screens. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Students now get a reminder to give feedback to their TA after a request (this can be turned off). [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Requests on the lab page can now be filtered. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Changed
 - Course editions not created in Queue will now not be displayed until the teacher 'unhides' them. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Shared editions are now displayed properly as upcoming/finished/archived. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)
 - If a module already has groups, students now need to join a group before enqueueing. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Fixed
 - Session names in the edition export did not correspond to the correct sessions. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx) 
 - Completed lab info pages now load correctly. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
 - Session names in the edition export did not correspond to the correct sessions. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - File names from parameters are now filtered. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)
 - Course catalog can now be filtered again. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

 - Revoked requests could be finished (approved, rejected, etc.) by teachers. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Room maps did not work. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Archive button appears again. [@mmadara](https://gitlab.ewi.tudelft.nl/mmadara)
### Deprecated

### Removed

## [2.1.4]

### Added
 - Students are now provided with a suggestion of their most recent location within a lab when they requeue. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)

### Changed
 - Deleting a lab will now show a dialog with a warning. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)

### Fixed
 - Requests are now soft-deleted when the corresponding lab is deleted. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
 - Students can no longer enqueue twice. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Deprecated

### Removed

## [2.1.3]
### Added
 - Slotted labs and exam labs now support consecutive filling. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)

### Changed
- Breadcrumb now shows course names and editions instead of codes. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Lab creation buttons are now merged into one button. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- Labs and status page are completely disabled when there is no modules/assignments. This prevents users from accidentally creating a lab without assignments, which leads them to an non-descriptive error page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Fixed
- Slotted Labs are now offset based, a change to the start date will now move the slots accordingly. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Fixed a bug where students could 'intentionally' queue in disabled slots within slotted labs. [@hpage](https://gitlab.ewi.tudelft.nl/hpage) 
- Admin tabs no longer show up on the 'create edition' page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Deprecated

### Removed

## [2.1.2]
### Added
 - New history page that shows the handled requests in the current session. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Added course code to request details page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Changed
 - Colours used in request tables are now more colour blind friendly. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Requests page now only shows pending and forwarded requests. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Request filters are now a dialog. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - New requests now appear at the top of the requests page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Fixed
 - Request table no longer updates when not on the first page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Deprecated

### Removed
 - Refresh button on requests page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Student name in requests table on the requests page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

## [2.1.1]
### Added
 - Added upcoming courses to home page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Changed
 - Course codes are now shown next to assignment filter [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
 - Moved add edition and add edition collection buttons to home. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Fixed
 - Feedback y-axis no longer has labels on increments of 1 [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
- Request Table Pagination is fixed. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
 - Lab stats page now shows the lab dates [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Request Table Pagination is fixed. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
 - Past labs are now shown on the calendar. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Header and footer no longer stretch across the entire page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Links in the header and footer now behave like links. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Titles of pages are now more consistent. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)
 - Fixed layout bugs on the catalog page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Deprecated

### Removed
 - Removed breadcrumbs from home page. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

## [2.1.0]
### Added
- Redirect users to enrol page when they are not correctly enrolled for a lab. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- A Student Group gets created automatically when a student attempts to enqueue without one. [@lemaire](https://gitlab.ewi.tudelft.nl/Lemaire)
- Allow managers and up to pick any request [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- A new announcement system for Admins to announce maintenance and other downtime. [@lemaire](https://gitlab.ewi.tudelft.nl/Lemaire)
- A new lab constraint system to add constraints on who can place requests. [@lemaire](https://gitlab.ewi.tudelft.nl/Lemaire)
- Sort and add filters for labs on the lab listing page. [@wpolet](https://gitlab.ewi.tudelft.nl/wpolet)
- Allow teachers to upload CSV files to add participants. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add admin overview page for ongoing sessions [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add calendar for student overview [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Show users a session expired notification [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add a free text-field to labs [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add an indication on number of slots taken [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add progress indicator for exam labs [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add request type filtering to status page [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Allow teachers and TA's to reject a student in a random selection lab [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Show character counter/max characters for question text areas [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add indicator for active filters [@cedricwillekens](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Allow users to view and create new shared editions. [@cedricwillekens](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add room images for student requests [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Hybrid Labs are now supported regardless of what the direction of the lab is. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Added buttons to edit sessions for a shared lab [@hpage](https://gitlab.ewi.tudelft.nl/hpage)

### Changed
- Redirect students to their request when their are being processed when accessing the lab page. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Provide a clearer error message to users when username cannot be found [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Also allow for constraints checking to be done on individual students. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Finished course editions are now primarily ordered by the end date (DESC) and secondarily ordered by the role of the user. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Lab export now includes TA comments. [@rwbackx](https://gitlab.ewi.tudelft.nl/rwbackx)

### Fixed
- In exam lab, fix all requests getting rejected [@rbackx](https://gitlab.ewi.tudelft.nl/rbackx)
- Redirect to correct URLs when trying to delete a lab [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Thymeleaf error on students view slotted lab page [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- When changing from random to fcfs, remove old requests and correctly pick new ones [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Fix exports for editions [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Fix language of calendar [@cchen9](https://gitlab.ewi.tudelft.nl/cchen9)
- Prevent error on returning to catalog or enrol page after enrolling in a course edition [@cchen9](https://gitlab.ewi.tudelft.nl/cchen9)
- Form submission buttons on requests are disabled after they are pressed, preventing 2 requests to be sent. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Feedback is now deleted when the associated request is deleted [@hpage](https://gitlab.ewi.tudelft.nl/hpage)
- Assistants can no longer see requests belonging to other editions in a shared session [@hpage](https://gitlab.ewi.tudelft.nl/hpage)

## [1.2.0] - [2020-06-06]
### Added
- Quick rating for TAs represented as a numerical value by [@nstruharova](https://gitlab.ewi.tudelft.nl/nstruharova)
- Allow users with manager and higher access to a course to unenqueue students from the queue [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add notification asking students for feedback after they have been approved by [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Allow teachers and managers to stop students enqueuing by [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add notification for students who were not found by the TA [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add customisation for email suffix, institute name and logo [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Integrate creation of Jitsi links to labs with an online communication methods [@sjuhosova](https://gitlab.ewi.tudelft.nl/sjuhosova)
- Add minimum requirement for Questions to be 15 characters in length [@lemaire](https://gitlab.ewi.tudelft.nl/lemaire)
- Show date on pending time slot request [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Add warning messages whenever a course with groups is created, but no groups exist [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Add statistics about assistants per lab under course status. [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Add reason to forwarded request in timeline [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Add the option for teachers to specify who is allowed to handle a specific assignment [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Add colour variables file to specify colour scheme of the application [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Automatic compilation for scss files [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Client-side course creation validation [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Add copy lab feature, to copy an existing lab [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Add about page [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)

### Changed
- Unauthorized students for labs/courses now redirects to enroll pages by [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- The feedback page has been moved from course specific to a general page which does not require the user to be part of any course to be visible, by [@cedricwillekn](https://gitlab.ewi.tudelft.nl/cedricwilleken)
- Always show "Get Next" button for TAs but disable it, if queue size is 0. [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Pass signOffIntervals when editing a lab to make sure ExamLab knows if it there are sign-off intervals. [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Make exam lab checkbox when creating lab dependent on the sign off intervals [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Users can no longer visit enqueue page while already enqueued. [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Average waiting time is now calculated over the requests handled in the last hour [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Sort labs based on opening time instead of creation time [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Change all css files to scss files for better readability [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Change view course from link to button [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Redesign course creation page [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Change lab title in filter dropdown to use more readable title. [@cedricwilleken](https://gitlab.ewi.tudelft.nl/cedricwilleken)

### Fixed
- Fix the "assigned to" filter not filtering newly incoming requests through push notifications by [@lemaire](https://gitlab.ewi.tudelft.nl/lemaire)
- Fix submitting filters to redirect to the request table page such that refreshing does not rePOST by [@lemaire](https://gitlab.ewi.tudelft.nl/lemaire)
- Fix request view for TAs, permissions in Thymeleaf template were in such a way that no one except the student could see their request [@lemaire](https://gitlab.ewi.tudelft.nl/lemaire)
- Fix disappearance of map when switching options, while not changing the selected room. [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- "Clear filters" button won't appear after submission with no filters on Requests page by [@sjuhosova](https://gitlab.ewi.tudelft.nl/sjuhosova)
- In exam lab, make selected slot's z-index higher to properly enrol form. [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Make slotSelectionOpensAt field update when its value gets updated, instead of when slotsOpensAt field updates. [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Fix 400-error on empty request-reject message [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Fix get-next button disappearance whenever lab name is too long. [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Add toString method to FirstYearStudent to fix StackOverFlowError when calling firstYearStudent.toString() [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- New requests will be filtered correctly with the right assigned ta to it [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Get Next button will only show up if the lab is open. [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Improve performance of counting the number of requests current in the queue for a specific assistant and specific lab by [@lemaire](https://gitlab.ewi.tudelft.nl/lemaire)
- Fix wrong submission link by remove email suffix [@tnulle](https://gitlab.ewi.tudelft.nl/tnulle)
- Requests for shared sessions are now only sent to the appropriate assistants. [@hpage](https://gitlab.ewi.tudelft.nl/hpage)

## [1.1.0] - (2019-12-16)
### Deprecated
- Java 8 support

## [1.0.1] - (2019-11-15)
### Fixed
- Hotfix for SAML authentication

## [1.0.0] - (2019-10-24)
#### Initial public release

