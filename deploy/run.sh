# Get the relative path to this directory
MY_PATH="`dirname \"$0\"`"

read -p "Provisioning will kill the server and all running jobs. Are you sure you want to provision? [y/n]" -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    echo "Build the project"
    (cd $MY_PATH/..; gradle clean build)

    echo "Run ansible playbook"
	ansible-playbook -i $MY_PATH/hosts $MY_PATH/playbook.yml
fi

