upstream tomcat {
  server localhost:8080;
}

server {
  listen 80;
  server_name queue.martijndwars.nl;
  return 301 https://queue.martijndwars.nl$request_uri;
}

server {
  listen 443 ssl;
  listen [::]:443 ssl ipv6only=on;

  root /usr/share/nginx/html;
  index index.html index.htm;

  server_name queue.martijndwars.nl;

  ssl_certificate /etc/letsencrypt/live/queue.martijndwars.nl/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/queue.martijndwars.nl/privkey.pem;

  location / {
    proxy_pass              http://tomcat/$request_uri;
    proxy_set_header        Host $host;
    proxy_set_header        X-Real-IP $remote_addr;
    proxy_set_header        X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header        X-Forwarded-Proto $scheme;

    # WebSocket support (nginx 1.4)
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection $http_connection;
  }
}
